function delete_fun( id ){
	var reply = confirm("Do You Want to Delete ?");
	if(reply){
		$.ajax({
	        type: "POST",
	        url: HTTP_PATH+"requests/requestDelete",
	        datatype: "json",
	        async: false,
			data: { id : id },
	        success: function(data) {
	        	var res = JSON.parse(data);
	 			if(res.errors == 0){
	 				location.reload();
	 			}else{
	 				alert('Problem in deleting record !');
	 			}
	        }
	    });
	}
}

function getDetail(id){
	$.ajax({
	    type: "POST",
	    url: HTTP_PATH+"requests/requestInfoView",
	    datatype: "json",
	    async: false,
		data: { id : id },
	    success: function(data) {
	    	var res = JSON.parse(data);
				if(res.errors == 0){
					$('#viewName').text(res.data.user_fname + ' ' +res.data.user_lname);
					$('#viewSource').text(res.data.req_src_adrs);
					$('#viewDestination').text(res.data.req_des_adrs);
					$('#viewDistance').text(res.data.req_distance);
					$('#viewTime').text(res.data.req_time);
					if(res.data.req_status == 'C')
						$('#viewStatus').html('<span class="label label-primary allow-field">Create</span>');
					else if(res.data.req_status == 'A')
						$('#viewStatus').html('<span class="label label-success allow-field">Accept</span>');
					else
						$('#viewStatus').html('<span class="label label-danger allow-field">Reject</span>');

				}else{
					$('#viewError').html(res.data.msg);
				}
				$('#req-info').modal('show');
	    }
	});
}
