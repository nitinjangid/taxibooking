function delete_fun( id ){
	var reply = confirm("Do You Want to Delete ?");
	if(reply){
		$.ajax({
	        type: "POST",
	        url: HTTP_PATH+"drivers/driverDelete",
	        datatype: "json",
	        async: false,
			data: { id : id },
	        success: function(data) {
	        	var res = JSON.parse(data);
	 			if(res.errors == 0){
	 				location.reload();
	 			}else{
	 				alert('Problem in deleting record !');
	 			}
	        }
	    });
	}
}

