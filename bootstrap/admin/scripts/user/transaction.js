function getDetail(id){
	$.ajax({
	    type: "POST",
	    url: HTTP_PATH+"transaction/transactionInfoView",
	    datatype: "json",
	    async: false,
		data: { id : id },
	    success: function(data) {
	    	var res = JSON.parse(data);
				if(res.errors == 0){
					$('#viewTransactionCode').text(res.data.txn_code);
					$('#viewBookingID').text(res.data.txn_bookingID);
					$('#viewCreateTime').text(res.data.txn_create_time);
					$('#viewUpdateTime').text(res.data.txn_update_time);

					if(res.data.txn_payment_method == 'credit_card')
						$('#viewPaymentMethod').text('Credit Card');
						
					$('#viewCreditCardID').text(res.data.txn_credit_card_id);
					$('#viewCurrency').text(res.data.txn_currency);
					$('#viewSaleID').text(res.data.txn_sale_id);
					$('#viewTransactionMonth').text(res.data.txn_expire_month);
					$('#viewTransactionYear').text(res.data.txn_expire_year);
					if(res.data.txn_sale_state == 'completed')
						$('#viewSaleState').html('<span class="label label-primary allow-field">Completed</span>');
					// else
						// $('#viewSaleState').html('<span class="label label-danger allow-field">Reject</span>');

				}else{
					$('#viewError').html(res.data.msg);
				}
				$('#transaction-info').modal('show');
	    }
	});
}