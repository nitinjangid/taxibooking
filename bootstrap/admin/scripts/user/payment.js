/**
 * [getId description]
 * @param  {[type]} id  [description]
 * @param  {[type]} amt [description]
 * @return {[type]}     [description]
 */
function getId( id, amt ){
	$('#driverId').val(id);
	$('#txn_total').val(amt);
}

/**
 * [roundToTwo description]
 * @param  {[type]} num [description]
 * @return {[type]}     [description]
 */
function roundToTwo(num) {    
    return +(Math.round(num + "e+2")  + "e-2");
}

/**
 * [saveTransfer description]
 * @return {[type]} [description]
 */
function saveTransfer(){
	$('#error').html('');
	var driverId = $('#driverId').val();
	var txn_total = Number($('#txn_total').val());
	var amount = Number($('#amount').val());
	var flag = 0;

	if((amount == ' ')){
		$('#error').html('Please enter valid amount');
		flag++;
	}

	if((amount > txn_total)){
		$('#error').html('Please enter amount equal to lesser than '+roundToTwo(txn_total));
		flag++;
	}

	if(flag == 0){
		$.ajax({
	        type: "POST",
	        url: HTTP_PATH+"account/payDriver",
	        datatype: "json",
	        async: false,
			data: { acc_driverID : driverId, acc_debit : amount },
	        success: function(data) {
	        	var res = JSON.parse(data);
	 			if(res.error == 0){
	 				location.reload();
	 			}else{
	 				$('#msgs').html('<span class="text-danger">'+res.message+'</span>');
	 			}
	        }
	    });
	}

}