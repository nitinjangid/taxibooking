function delete_fun( id ){
	var reply = confirm("Do You Want to Delete ?");
	if(reply){
		$.ajax({
	        type: "POST",
	        url: HTTP_PATH+"bookings/bookingDelete",
	        datatype: "json",
	        async: false,
			data: { id : id },
	        success: function(data) {
	        	var res = JSON.parse(data);
	 			if(res.errors == 0){
	 				location.reload();
	 			}else{
	 				alert('Problem in deleting record !');
	 			}
	        }
	    });
	}
}

function getDetail(id){
	$.ajax({
	    type: "POST",
	    url: HTTP_PATH+"bookings/bookingInfoView",
	    datatype: "json",
	    async: false,
		data: { id : id },
	    success: function(data) {
	    	var res = JSON.parse(data);
				if(res.errors == 0){
					$('#viewDriver').text(res.data.driver_fname + ' ' +res.data.driver_lname);
					$('#viewSource').text(res.data.req_src_adrs);
					$('#viewDestination').text(res.data.req_des_adrs);
					$('#viewDistance').text(res.data.req_distance);
					$('#viewTime').text(res.data.req_time);
					if(res.data.booking_status == 'O')
						$('#viewStatus').html('<span class="label label-primary allow-field">Open</span>');
					else
						$('#viewStatus').html('<span class="label label-danger allow-field">Reject</span>');

				}else{
					$('#viewError').html(res.data.msg);
				}
				$('#booking-info').modal('show');
	    }
	});
}