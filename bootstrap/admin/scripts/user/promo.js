/**
 * [div_open description]
 * @param  {[type]} id [description]
 * @return {[type]}    [description]
 */
function div_open(id) {
    $(id).show("slow");
}

/**
 * [div_close description]
 * @param  {[type]} id [description]
 * @return {[type]}    [description]
 */
function div_close(id) {
    $(id).hide("slow");
}

function approvePromo(id){
	var reply = confirm("Are you sure to approve ?");
	if(reply){
		$.ajax({
	        type: "POST",
	        url: HTTP_PATH+"promo/promoApprove",
	        datatype: "json",
	        async: false,
			data: {promo_id: id},
	        success: function(data) {
	        	var res = JSON.parse(data);
	 			if(res.errors == 0){
	 				location.reload();
	 			}
	        }
	    });
	}
}

function disapprovePromo(id){
	var reply = confirm("Are you sure to disapprove ?");
	if(reply){
		$.ajax({
	        type: "POST",
	        url: HTTP_PATH+"promo/promoDisapprove",
	        datatype: "json",
	        async: false,
			data: {promo_id: id},
	        success: function(data) {
	        	var res = JSON.parse(data);
	 			if(res.errors == 0){
	 				location.reload();
	 			}
	        }
	    });
	}
}

function promoDelete(id){
	var reply = confirm("Are you sure to delete ?");
	if(reply){
		$.ajax({
	        type: "POST",
	        url: HTTP_PATH+"promo/promoDelete",
	        datatype: "json",
	        async: false,
			data: {promo_id: id},
	        success: function(data) {
	        	var res = JSON.parse(data);
	 			if(res.errors == 0){
	 				location.reload();
	 			}
	        }
	    });
	}
}

function viewPromo(id){
	$.ajax({
        type: "POST",
        url: HTTP_PATH+"promo/getPromoDetails",
        datatype: "json",
        async: false,
		data: {promo_id: id},
        success: function(data) {
        	var res = JSON.parse(data);
 			if(res.errors == 0){
 				var approve = '';
 				var deleted = '';
 				$('#v_promo_code').text(res.data.promo_code);
 				$('#v_promo_discount').text(res.data.promo_dis_per);
 				$('#v_promo_exp').text(res.data.promo_exp_date);

 				if(res.data.promo_status == 'Y'){
 					approve = '<span class="btn btn-xs green"> Approved </span>';
 				}else{
 					approve = '<span class="btn btn-xs red"> Not Approved </span>';
 				}
 				$('#v_is_approved').html(approve);

 				$('#v_created_at').text(res.data.created_at);
 				$('#v_updated_at').text(res.data.updated_at);
 				$("#promo_details").modal('show');
 			}
        }
    });	
	
}

function editPromo(id){
	$.ajax({
        type: "POST",
        url: HTTP_PATH+"promo/getPromoDetails",
        datatype: "json",
        async: false,
		data: {promo_id: id},
        success: function(data) {
        	var res = JSON.parse(data);
 			if(res.errors == 0){
 				var approve = '';
 				var deleted = '';
 				$('#promo_id').val(res.data.promo_id);
 				$('#promo_code').val(res.data.promo_code);
 				$('#promo_dis_per').val(res.data.promo_dis_per);
 				$('#promo_exp_date').val(res.data.promo_exp_date);
 				$("#add_promocode").show("slow");
 			}
        }
    });	
	
}