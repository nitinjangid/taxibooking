function allow( id ){
	var reply = confirm("Do You Want to Allow ?");
	if(reply){
		$.ajax({
	        type: "POST",
	        url: HTTP_PATH+"users/userAllow",
	        datatype: "json",
	        async: false,
			data: { id : id },
	        success: function(data) {
	        	var res = JSON.parse(data);
	 			if(res.errors == 0){
	 				location.reload();
	 			}
	        }
	    });
	}
}

function notAllow( id ){
	var reply = confirm("Do You Want to Disallow ?");
	if(reply){
		$.ajax({
	        type: "POST",
	        url: HTTP_PATH+"users/userNotAllow",
	        datatype: "json",
	        async: false,
			data: { id : id },
	        success: function(data) {
	        	var res = JSON.parse(data);
	 			if(res.errors == 0){
	 				location.reload();
	 			}
	        }
	    });
	}
}

function userDetail(id){
	$.ajax({
	    type: "POST",
	    url: HTTP_PATH+"users/userInfoView",
	    datatype: "json",
	    async: false,
		data: { id : id },
	    success: function(data) {
	    	var res = JSON.parse(data);
				if(res.errors == 0){
					if(res.data.user_avatar != '')
						$('#imagePreview').html('<div class="imagePreview pull-right"><img alt="" src="'+USER_IMAGE_PATH+res.data.user_avatar+'" id="pimg"></div>');
					else
						$('#imagePreview').html('<i>NA</i>');

					$('#viewName').text(res.data.user_fname + ' ' +res.data.user_lname);
					$('#viewEmail').text(res.data.user_email);
					$('#viewMobile').text(res.data.user_mobile);
					if(res.data.user_status == '1')
						$('#viewStatus').html('<span class="label label-success allow-field">Active</span>');
					else
						$('#viewStatus').html('<span class="label label-danger allow-field">Blocked</span>');

				}else{
					$('#viewError').html(res.data.msg);
				}
				$('#user-info').modal('show');
	    }
	});
}