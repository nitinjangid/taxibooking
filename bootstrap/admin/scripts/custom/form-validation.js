var FormValidation = function () {

    var driverAddValidation = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form1 = $('#personalinformation-form');
            var error1 = $('.alert-danger', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    driver_fname: {
                        minlength: 2,
                        required: true
                    },
                    driver_lname: {
                        minlength: 2,
                        required: true
                    },
                    driver_gender: {
                        required: true
                    },
                    driver_email: {
                        required: true,
                        email: true
                    },
                    driver_street: {
                        minlength: 2,
                        required: true
                    },
                    driver_city: {
                        minlength: 2,
                        required: true
                    },
                    driver_state: {
                        minlength: 2,
                        required: true
                    },
                    driver_mobile: {
                        required: true,
                        number: true,
                        minlength: 10,
                        maxlength: 10,
                    },
                    driver_pin: {
                        required: true,
                        number: true,
                        minlength: 6,
                        maxlength: 6,
                    },
                    driver_country: {
                        required: true,
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },


            });

    }

    var licenceUploadValidation = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form1 = $('#licenceupload-form');
            var error1 = $('.alert-danger', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    driver_licence_front: {
                        required: true
                    },
                    driver_licence_back: {
                        required: true
                    },
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

            });

    }

    var vehicleDocumentUploadValidation = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form1 = $('#vehicledocumentupload-form');
            var error1 = $('.alert-danger', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    // driver_vehicle_registration: {
                    //     required: true
                    // },
                    // driver_vehicle_insurance: {
                    //     required: true
                    // },
                    driver_vehicle_maker: {
                        required: true
                    },
                    driver_vehicle_model: {
                        required: true
                    },
                    driver_vehicle_year: {
                        required: true,
                        number: true,
                        minlength: 4,
                        maxlength: 4,
                    },
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

            });

    }

    var settingFormValidation = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form1 = $('#settings-form');
            var error1 = $('.alert-danger', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    fare_base: {
                        minlength: 2,
                        required: true
                    },
                    fare_time: {
                        required: true
                    },
                    fare_distance: {
                        required: true
                    },
                    fare_commision: {
                        required: true,
                        number: true,
                        maxlength:5,
                        minlength:1,
                        min: 1,
                        max: 100,
                    },
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },


            });

    }


    return {
        //main function to initiate the module
        init: function () {

            driverAddValidation();
            licenceUploadValidation();
            vehicleDocumentUploadValidation();
            settingFormValidation();

        }

    };

}();