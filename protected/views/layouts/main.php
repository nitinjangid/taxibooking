<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title><?php echo 'URide - '. CHtml::encode($this->pageTitle); ?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Utils::getStyleUrl() ?>plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Utils::getStyleUrl() ?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Utils::getStyleUrl() ?>plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->

	<!-- BEGIN THEME STYLES -->
	<link href="<?php echo Utils::getStyleUrl() ?>css/style-metronic.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Utils::getStyleUrl() ?>css/style.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Utils::getStyleUrl() ?>css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Utils::getStyleUrl() ?>css/plugins.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Utils::getStyleUrl() ?>css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="<?php echo Utils::getStyleUrl() ?>css/print.css" rel="stylesheet" type="text/css" media="print"/>
	<link href="<?php echo Utils::getStyleUrl() ?>css/custom.css" rel="stylesheet" type="text/css"/>
	<script src="<?php echo Utils::getStyleUrl() ?>plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
		<!-- END THEME STYLES -->
	<link rel="shortcut icon" href="favicon.ico"/>
	<script>
		var HTTP_PATH = '<?php echo Utils::getBaseUrl();?>/';
		var USER_IMAGE_PATH = '<?php echo Utils::UserImagePath();?>';
	</script>	
</head>

<body class="page-header-fixed">

	<div id="header" class="header navbar navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
	<div class="header-inner">
		<!-- BEGIN LOGO -->
		<a class="navbar-brand" style="padding: 3px;" href="<?php echo Yii::app()->createAbsoluteUrl('dashboard/index'); ?>">			
			<img style="width: 44%;" src="<?php echo Yii::app()->params['image_path'].'logo.png'; ?>" alt=""/> 
		</a>
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<img src="<?php echo Utils::getStyleUrl() ?>img/menu-toggler.png" alt=""/>
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		<ul class="nav navbar-nav pull-right">
			<!-- BEGIN USER LOGIN DROPDOWN -->
			<li class="dropdown user">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<img alt="" src="<?php echo Utils::UserImagePath() .Yii::app()->session['admin_data']['admin_image']; ?>" style="height: 30px;
    width: 30px;"/>
					<span class="username">
						 <?php echo Yii::app()->session['admin_data']['admin_name']; ?>
					</span>
					<i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu">
					<li>
						<a href="<?php echo Yii::app()->createAbsoluteUrl('dashboard/profile'); ?>">
							<i class="fa fa-user"></i> My Profile
						</a>
					</li>
					<li>
						<a href="<?php echo Yii::app()->createAbsoluteUrl('authentication/logout'); ?>">
							<i class="fa fa-key"></i> Log Out
						</a>
					</li>
				</ul>
			</li>
			<!-- END USER LOGIN DROPDOWN -->
		</ul>
		<!-- END TOP NAVIGATION MENU -->
	</div>
	<!-- END TOP NAVIGATION BAR -->
	</div><!-- header -->
	<!-- END HEADER -->
	<div class="clearfix"></div>


	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar-wrapper">
			<div class="page-sidebar navbar-collapse collapse">
				<!-- add "navbar-no-scroll" class to disable the scrolling of the sidebar menu -->
				<!-- BEGIN SIDEBAR MENU -->
				<ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
					<li class="sidebar-toggler-wrapper">
						<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
						<div class="sidebar-toggler hidden-phone">
						</div>
						<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					</li>
					<li class="sidebar-search-wrapper">
						<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
						<!-- <form class="sidebar-search" action="extra_search.html" method="POST">
							<div class="form-container">
								<div class="input-box">
									<a href="javascript:;" class="remove">
									</a>
									<input type="text" placeholder="Search..."/>
									<input type="button" class="submit" value=" "/>
								</div>
							</div>
						</form> -->
						<!-- END RESPONSIVE QUICK SEARCH FORM -->
					</li>
					<li class="start <?php echo (CHtml::encode($this->pageTitle) == 'Dashboard') ? 'active' : '' ;?>">
						<a href="<?php echo Yii::app()->createAbsoluteUrl('dashboard/index'); ?>">
							<i class="fa fa-home"></i>
							<span class="title">
								Dashboard
							</span>
							<span class="selected">
							</span>
						</a>
					</li>
					<li class="<?php echo (CHtml::encode($this->pageTitle) == 'Drivers Management') ? 'active' : '' ;?>">
						<a href="<?php echo Yii::app()->createAbsoluteUrl('drivers/drivers'); ?>">
							<i class="fa fa-truck"></i>
							<span class="title">
								Driver Management
							</span>
							<span class="selected">
							</span>
						</a>
					</li>
					<li class="<?php echo (CHtml::encode($this->pageTitle) == 'User Management') ? 'active' : '' ;?>">
						<a href="<?php echo Yii::app()->createAbsoluteUrl('users/users'); ?>">
							<i class="fa fa-users"></i>
							<span class="title">
								User Management
							</span>
							<span class="selected">
							</span>
						</a>
					</li>
					<li class="<?php echo (CHtml::encode($this->pageTitle) == 'Request Management') ? 'active' : '' ;?>">
						<a href="<?php echo Yii::app()->createAbsoluteUrl('requests/request'); ?>">
							<i class="fa fa-bell"></i>
							<span class="title">
								Request Management
							</span>
							<span class="selected">
							</span>
						</a>
					</li>
					<li class="<?php echo (CHtml::encode($this->pageTitle) == 'Booking Management') ? 'active' : '' ;?>">
						<a href="<?php echo Yii::app()->createAbsoluteUrl('bookings/booking'); ?>">
							<i class="fa fa-clock-o"></i>
							<span class="title">
								Booking Management
							</span>
							<span class="selected">
							</span>
						</a>
					</li>
					<li class="<?php echo (CHtml::encode($this->pageTitle) == 'Transaction Management') ? 'active' : '' ;?>">
						<a href="<?php echo Yii::app()->createAbsoluteUrl('transaction/transaction'); ?>">
							<i class="fa fa-dollar"></i>
							<span class="title">
								Transaction Management
							</span>
							<span class="selected">
							</span>
						</a>
					</li>
					<li class="<?php echo (CHtml::encode($this->pageTitle) == 'Payment Management') ? 'active' : '' ;?>">
						<a href="<?php echo Yii::app()->createAbsoluteUrl('payment'); ?>">
							<i class="fa fa-money"></i>
							<span class="title">
								Payment Management
							</span>
							<span class="selected">
							</span>
						</a>
					</li>
					<li class="<?php echo (CHtml::encode($this->pageTitle) == 'Promocode Management') ? 'active' : '' ;?>">
						<a href="<?php echo Yii::app()->createAbsoluteUrl('promo'); ?>">
							<i class="fa fa-money"></i>
							<span class="title">
								Promocode Management
							</span>
							<span class="selected">
							</span>
						</a>
					</li>
					<li class="<?php echo (CHtml::encode($this->pageTitle) == 'Settings') ? 'active' : '' ;?>">
						<a href="<?php echo Yii::app()->createAbsoluteUrl('settings/settings'); ?>">
							<i class="fa fa-cog"></i>
							<span class="title">
								Settings
							</span>
							<span class="selected">
							</span>
						</a>
					</li>
				</ul>
				<!-- END SIDEBAR MENU -->
			</div>
		</div>
		<!-- END SIDEBAR -->

		<!-- BEGIN PAGE CONTENT -->
		<?php echo $content; ?>
		<!-- END PAGE CONTENT -->


		</div>
		<!-- END CONTAINER -->
		<!-- BEGIN FOOTER -->
		<div class="footer">
			<div class="footer-inner">
				 2016 &copy; URide.
			</div>
			<div class="footer-tools">
				<span class="go-top">
					<i class="fa fa-angle-up"></i>
				</span>
			</div>
		</div>
		<!-- END FOOTER -->
		<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
		<!-- BEGIN CORE PLUGINS -->
		<!--[if lt IE 9]>
		<script src="assets/plugins/respond.min.js"></script>
		<script src="assets/plugins/excanvas.min.js"></script> 
		<![endif]-->
		<script src="<?php echo Utils::getStyleUrl() ?>plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
		<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
		<script src="<?php echo Utils::getStyleUrl() ?>plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
		<script src="<?php echo Utils::getStyleUrl() ?>plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="<?php echo Utils::getStyleUrl() ?>plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
		<script src="<?php echo Utils::getStyleUrl() ?>plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
		<script src="<?php echo Utils::getStyleUrl() ?>plugins/jquery.blockui.min.js" type="text/javascript"></script>
		<script src="<?php echo Utils::getStyleUrl() ?>plugins/jquery.cokie.min.js" type="text/javascript"></script>
		<script src="<?php echo Utils::getStyleUrl() ?>plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
		<script type="text/javascript" src="<?php echo Utils::getStyleUrl() ?>plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<!-- END CORE PLUGINS -->



</body>
</html>
