<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.1.1
Version: 2.0.2
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Admin Login</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="<?php echo Utils::getStyleUrl() ?>plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo Utils::getStyleUrl() ?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo Utils::getStyleUrl() ?>plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo Utils::getStyleUrl() ?>plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo Utils::getStyleUrl() ?>plugins/select2/select2-metronic.css"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link href="<?php echo Utils::getStyleUrl() ?>css/style-metronic.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo Utils::getStyleUrl() ?>css/style.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo Utils::getStyleUrl() ?>css/style-responsive.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo Utils::getStyleUrl() ?>css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo Utils::getStyleUrl() ?>css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="<?php echo Utils::getStyleUrl() ?>css/pages/login.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo Utils::getStyleUrl() ?>css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
</head>
<style>
	.forget-form{
		display: block !important;
	}
</style>
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
	<a href="javascript:void;">
		URide
		<!-- <img src="<?php echo Utils::getStyleUrl() ?>img/logo-big.png" alt=""/> -->
	</a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
	<!-- BEGIN FORGOT PASSWORD FORM -->
		<?php
			$form = $this->beginWidget('CActiveForm', array(
			    'id' => 'forgot-password-form',
			    'action' => 'forgotpasswordsubmit',
			    'enableClientValidation' => TRUE,
			    'clientOptions' => array(
			        'validateOnSubmit' => TRUE,
			        'validateOnChange' => TRUE
			    ),
			    'htmlOptions' => array(
			        'autocomplete' => 'off'
			    ),
			));
		?> 

		<?php if (Yii::app()->user->hasFlash('message')): ?>
            <div class="alert alert-<?php echo Yii::app()->user->getFlash('type'); ?>" id="successmsg">
                <?php echo Yii::app()->user->getFlash('message'); ?>
            </div>
        <?php endif; ?>

		<h3>Forget Password ?</h3>
		<p>
			 Enter your e-mail address below to reset your password.
		</p>
		<div class="form-group">
			<div class="input-icon">
				<i class="fa fa-envelope"></i>
				<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" id="email" required="required" />
			</div>
				<div style="" id="email_em" class="errorMessage text-danger"></div>
		</div>

		<div class="form-actions">
			<button type="button" id="back-btn" class="btn">
			<i class="m-icon-swapleft"></i> Back </button>
			<button type="submit" class="btn green pull-right" id="btnForgotPassword">
			Submit <i class="m-icon-swapright m-icon-white"></i>
			</button>
		</div>


	<?php $this->endWidget(); ?>
	<!-- END FORGOT PASSWORD FORM -->
	
</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright">
	 2016 &copy; URide.
</div>
<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
	<script src="assets/plugins/respond.min.js"></script>
	<script src="assets/plugins/excanvas.min.js"></script> 
	<![endif]-->
<script src="<?php echo Utils::getStyleUrl() ?>plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<?php echo Utils::getStyleUrl() ?>plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="<?php echo Utils::getStyleUrl() ?>plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo Utils::getStyleUrl() ?>plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo Utils::getStyleUrl() ?>plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo Utils::getStyleUrl() ?>plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo Utils::getStyleUrl() ?>plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo Utils::getStyleUrl() ?>plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo Utils::getStyleUrl() ?>plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo Utils::getStyleUrl() ?>plugins/select2/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo Utils::getStyleUrl() ?>scripts/core/app.js" type="text/javascript"></script>
<script src="<?php echo Utils::getStyleUrl() ?>scripts/custom/login.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
		jQuery(document).ready(function() {     
		  App.init();
		  Login.init();
		});
	</script>

<script type="text/javascript">
    // $(document).ready(function () {

    //     $("#successmsg").animate({opacity: 1.0}, 2000).fadeOut("slow");

    //     $('#LoginForm_username').blur(function () {
    //         if ($('#LoginForm_username_em_').css('display') != 'none') {
    //             $('#LoginForm_password').val('');
    //         }
    //     });

    //     $('#btnLogin').click(function () {
    //         if ($('#LoginForm_username_em_').css('display') != 'none') {
    //             $('#LoginForm_password').val('');
    //         }

    //         if ($('#recaptcha_response_field').val() == '') {
    //             $('#catcha_error').html('Please enter captcha');
    //             return false;
    //         } else {
    //             $('#catcha_error').html('');
    //         }
    //     });
    // });
    // 

    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return pattern.test(emailAddress);
    }

    $('#email').blur(function() {
        if ($('#email').val() == '') {
            $('#email_em').html('Please enter email id');
        } else {
            if (!isValidEmailAddress($('#email').val())) {
                $('#email_em').html('Please enter valid email');
            } else {
                $('#email_em').html('');
            }
        }
    });



    $('#btnForgotPassword').on('click', function(e) {
        if ($('#email').val() == '') {
            $('#email_em').html('Please enter email id');
            e.preventDefault();
        } else {
            if (!isValidEmailAddress($('#email').val())) {
                $('#email_em').html('Please enter valid email');
                e.preventDefault();
            } else {
                $('#email_em').html('');
            }
        }
    });

    $('#back-btn').on('click',function(){
    	    var url = "login";    
			$(location).attr('href',url);
    });


</script>

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>