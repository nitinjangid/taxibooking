	<link href="<?php echo Utils::getStyleUrl() ?>css/pages/profile.css" rel="stylesheet" type="text/css"/>
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					User Profile <small>user profile sample</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
												
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row profile">
				<div class="col-md-12">
					<!--BEGIN TABS-->
					<div class="tabbable tabbable-custom tabbable-full-width">
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#tab_1_3" data-toggle="tab">
									 Account
								</a>
							</li>
						</ul>
						<div class="tab-content">
							<!--tab_1_2-->
							<div class="tab-pane active" id="tab_1_3">
								<div class="row profile-account">
									<div class="col-md-3">
										<ul class="ver-inline-menu tabbable margin-bottom-10">
											<li class="active">
												<a data-toggle="tab" href="#tab_1-1">
													<i class="fa fa-cog"></i> Personal info
												</a>
												<span class="after">
												</span>
											</li>
											<li>
												<a data-toggle="tab" href="#tab_2-2">
													<i class="fa fa-picture-o"></i> Change Avatar
												</a>
											</li>
											<li>
												<a data-toggle="tab" href="#tab_3-3">
													<i class="fa fa-lock"></i> Change Password
												</a>
											</li>
										</ul>
									</div>
									<div class="col-md-9">
									<?php if (Yii::app()->user->hasFlash('message')): ?>
										<?php if(Yii::app()->user->getFlash('type') == 'success'):?>
											<div class="alert alert-success" id="successmsg">
										<?php else:?>
											<div class="alert alert-danger" id="successmsg">
										<?php endif;?>
											<button class="close" data-close="alert"></button>
											<span>
												<?php 
													echo Yii::app()->user->getFlash('message'); 
													Yii::app()->user->setFlash('message', null);
												?>
											</span>
										</div>
										<?php
											endif;
										?> 
										<div class="tab-content">
											<div id="tab_1-1" class="tab-pane active">
												<!-- <form role="form" method="post" action="<?php echo Yii::app()->createAbsoluteUrl('dashboard/updateprofile'); ?>"> -->
													<?php
				                                        $form = $this->beginWidget('CActiveForm', array(
				                                            'id' => 'personalinformation-form',
				                                            'action' => 'updateprofile',
				                                            'enableClientValidation' => TRUE,
				                                            'clientOptions' => array(
				                                                'validateOnSubmit' => TRUE,
				                                                'validateOnChange' => TRUE
				                                            ),
				                                            'htmlOptions' => array(
				                                                'enctype' => 'multipart/form-data',
				                                                'autocomplete' => 'off',
				                                                'role' => 'form'
				                                            ),
				                                            'focus' => array($model, 'admin_name'),
				                                        ));
				                                    ?>
													<div class="form-group">
														<label class="control-label">Name</label>
														<input type="text" placeholder="John" value="<?php echo $adminData['name'];?>" class="form-control" name="admin_name"/>
														<?php echo ucfirst($form->error($model, 'admin_name', array('class' => 'text-danger'))); ?>
													</div>
													<div class="form-group">
														<label class="control-label">Username</label>
														<input type="text" placeholder="Doe" value="<?php echo $adminData['username'];?>" class="form-control" name="admin_username"/>
														<?php echo ucfirst($form->error($model, 'admin_username', array('class' => 'text-danger'))); ?>
													</div>
													<div class="form-group">
														<label class="control-label">Email</label>
														<input type="text" placeholder="john.doe@mail.com" value="<?php echo $adminData['email'];?>" class="form-control" name="admin_email"/>
														<?php echo ucfirst($form->error($model, 'admin_email', array('class' => 'text-danger'))); ?>
													</div>
													<div class="margiv-top-10">
														<input type="submit" class="btn green" value="Update Profile" name="btnSaveProfile">
													</div>

												 <?php $this->endWidget(); ?>                   

											</div>
											<div id="tab_2-2" class="tab-pane">
												<?php
													$form = $this->beginWidget('CActiveForm', array(
													    'id' => 'profilepicture-form',
													    'action' => 'updateprofile',
													    'enableClientValidation' => TRUE,
													    'clientOptions' => array(
													        'validateOnSubmit' => TRUE,
													        'validateOnChange' => TRUE
													    ),
													    'htmlOptions' => array(
													        'enctype' => 'multipart/form-data',
													        'autocomplete' => 'off',
													        'role' => 'form'
													        )));
													?>

													<div class="row">
													    <div class="col-md-12">                                                
													        <h3>Profile Image</h3>
													        <hr/>
													        <?php
													        $flag = 1;
													        $path = Utils::UserImagePath_M();
													        if (!empty($model->admin_image)) {
													            $path = Utils::UserImagePath() . $model->admin_image;
													            $flag = 0;
													        }
													        ?>                                        
													        <div class="innerdiv">
													            <img id="imagePreview" style="height: 300px;width: 100%;" src="<?php echo $path; ?>" class="img-responsive img-profile"/>
													            <span id="span_close">
													                <?php if ($flag == 0) { ?>
													                    <span id="close" style="display:none" title="Click here to delete this image"><i class="fa fa-times fa-2x"></i></span>
													                <?php } ?>
													            </span>                                                
													        </div>
													        <br>                                                                                    
													        <div class="form-group">
													            <?php echo $form->fileField($model, 'admin_image'); ?>
													            <?php echo $form->error($model, 'user_image', array('class' => 'text-red')); ?>
													            <!-- <p class="help-block text-orange"><?php //echo Yii::t('lang', 'msg_images'); ?></p> -->
													        </div>                                        

													        <?php echo CHtml::submitButton('Update Profile Image', array('class' => 'btn green btn-square', 'id' => 'btnSaveProfilePicture', 'name' => 'btnSaveProfilePicture')); ?>

													    </div>
													</div>

													<?php $this->endWidget(); ?>
											</div>
											<div id="tab_3-3" class="tab-pane">
												<form action="<?php echo Yii::app()->createAbsoluteUrl('dashboard/updateprofile'); ?>" method="post">
												
												<?php
													$form = $this->beginWidget('CActiveForm', array(
													    'id' => 'changepassword-form',
													    'action' => 'updateprofile',
													    'enableClientValidation' => TRUE,
													    'clientOptions' => array(
													        'validateOnSubmit' => TRUE,
													        'validateOnChange' => TRUE
													    ),
													    'htmlOptions' => array(
													        'autocomplete' => 'off',
													        'role' => 'form'
													    ),
													    'focus' => array($model, 'user_password'),
													));
												?>

													<div class="form-group">
														<label class="control-label">Current Password</label>
														<input type="password" class="form-control" name="old_password"/>
														<?php echo ucfirst($form->error($model, 'old_password', array('class' => 'text-danger'))); ?>
													</div>
													<div class="form-group">
														<label class="control-label">New Password</label>
														<input type="password" class="form-control" name="new_password"/>
														<?php echo ucfirst($form->error($model, 'new_password', array('class' => 'text-danger'))); ?>
													</div>
													<div class="form-group">
														<label class="control-label">Re-type New Password</label>
														<input type="password" class="form-control" name="new_password_again"/>
														<?php echo ucfirst($form->error($model, 'new_password_again', array('class' => 'text-danger'))); ?>
													</div>
													<div class="margin-top-10">
														<input type="submit" class="btn green" value="Update Password" name="btnSavePassword">
													</div>

												<?php $this->endWidget(); ?>
											</div>
										</div>
									</div>
									<!--end col-md-9-->
								</div>
							</div>
							<!--end tab-pane-->
						</div>
					</div>
					<!--END TABS-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->

	<script type="text/javascript">

    $(function() {

        /*********************************************************/
        /*   User Image Block Start   */
        /*********************************************************/
        $('#Admin_admin_image').on('change', function() {
            var files = !!this.files ? this.files : [];
            if (!files.length || !window.FileReader)
                return;

            var ftype = $(this)[0].files[0].type;
            var types = ['image/jpg', 'image/jpeg', 'image/png', 'image/gif'];
            if ($.inArray(ftype, types) > 0) {
                if (/^image/.test(files[0].type)) {
                    if ($(this)[0].files[0].size > 2097152) {
                        $('#statusMsg').addClass('alert alert-danger').html('The Image Size is too Big. Max size for the image is 2MB');
                        $(this).val('');
                        $("#imagePreview").attr("src", '<?php echo!empty($model->admin_image) ? Utils::UserImagePath() . $model->admin_image : Utils::UserImagePath_M(); ?>');
                        setTimeout(function() {
                            $('#statusMsg').removeClass('alert alert-danger').html('');
                        }, 3000);
                    } else {
                        var reader = new FileReader();
                        reader.readAsDataURL(files[0]);
                        reader.onloadend = function(event) {
                            $("#imagePreview").attr("src", event.target.result);
                            $("#span_close").html('<span id="close" style="display:none" title="Click here to delete this image"><i class="fa fa-times fa-2x"></i></span>');
                        }
                    }
                } else {
                    $('#statusMsg').addClass('alert alert-danger').html('Please upload a valid Image File.');
                    $(this).val('');
                    $("#imagePreview").attr("src", '<?php echo!empty($model->admin_image) ? Utils::UserImagePath() . $model->admin_image : Utils::UserImagePath_M() ?>');
                    setTimeout(function() {
                        $('#statusMsg').removeClass('alert alert-danger').html('');
                    }, 3000);
                }
            } else {
                $('#statusMsg').addClass('alert alert-danger').html('Please upload a valid Image File.');
                $(this).val('');
                $("#imagePreview").attr("src", '<?php echo!empty($model->admin_image) ? Utils::UserImagePath() . $model->admin_image : Utils::UserImagePath_M(); ?>');
                setTimeout(function() {
                    $('#statusMsg').removeClass('alert alert-danger').html('');
                }, 3000);
            }
        });

        $("#imagePreview").mouseover(function() {
            $("#close").show();
        });
        $("#close").mouseover(function() {
            $("#close").show();
        });
        $("#span_close").mouseover(function() {
            $("#close").show();
        });
        $("#close").mouseout(function() {
            $("#close").hide();
        });
        $("#imagePreview").mouseout(function() {
            $("#close").hide();
        });

        $("#close").on("click", function() {
            var img_data = '<?php echo $model->admin_image; ?>';
            if (img_data) {
                $.post(
                        '<?php echo Yii::app()->request->baseUrl; ?>/dashboard/removeImage',
                        {'id': '<?php echo $model->admin_id; ?>'},
                function(data) {
                    if (data == 1) {
                        $('#statusMsg').addClass('alert alert-success').html('Photo deleted successfully.');
                        setTimeout(function() {
                            $('#statusMsg').removeClass('alert alert-danger').html('');
                        }, 3000);
                        //window.location.reload();
                    } else {
                        $('#statusMsg').addClass('alert alert-danger').html('System Error.');
                        setTimeout(function() {
                            $('#statusMsg').removeClass('alert alert-danger').html('');
                        }, 3000);
                    }
                });
            } else {
                $("#imagePreview").attr("src", '<?php echo Utils::UserImagePath_M() ?>');
                $('#Admin_admin_image').val('');
                $("#span_close").html("");
            }
        });

        $("#span_close").on("click", function() {
            $("#imagePreview").attr("src", '<?php echo Utils::UserImagePath_M() ?>');
            $('#Admin_admin_image').val('');
            $("#span_close").html("");
        });
        /*********************************************************/
        /*   User Image Block End   */
        /*********************************************************/
    });
</script>
<style>
    #close{
        right: 0;
        position: absolute;
        top: -2px;
        display: block;
        cursor: pointer;
        color: #d82551;
    }
    .innerdiv{
        position: relative;
        width: 280px;

        text-align: center;
        border: 1px solid #ccc;
        padding: 4px;
    }
</style>