	<?php $this->pageTitle = ucwords($title);?>
	<?php
	// echo '<pre>';print_r($model);die;

	?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo Utils::getStyleUrl() ?>plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo Utils::getStyleUrl() ?>plugins/select2/select2-metronic.css"/>
<?php 
	if(!empty($css_file))
		echo '<link href="'.Utils::getStyleUrl().'css/user/'.$css_file.'.css" rel="stylesheet" type="text/css"/>';
?>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Drivers Management <small></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('home') ?>">
								Dashboard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="javascript:;">
								<?php echo $title;?>
							</a>
						</li>
						
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="tabbable tabbable-custom boxless tabbable-reversed">
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#tab_1" data-toggle="tab">
									 Update Settings
								</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab_1">
								<div class="portlet box blue">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-reorder"></i>Settings Form
										</div>
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
										<?php
											// echo '<pre>';print_r($model);die;
	                                    	$form = $this->beginWidget('CActiveForm', array(
	                                            'id' => 'settings-form',
	                                            'action' => Yii::app()->createAbsoluteUrl('settings/updateSettings'),
	                                           
	                                            'htmlOptions' => array(
	                                                'enctype' => 'multipart/form-data',
	                                                'autocomplete' => 'off',
	                                                'role' => 'form',
	                                                'class'=>'form-horizontal form-bordered form-row-stripped',
	                                            ),
	                                        ));
	                                    ?>
	                                    	<input type="hidden" name="id" value="<?php echo $model->fare_id;?>">
											<div class="form-body">
												<div class="form-group">
													<label class="control-label col-md-3">Fare Base<sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<input type="text" placeholder="Fare Base" class="form-control" value="<?php echo $model->fare_base ?>" name="fare_base"/>
														<?php echo ucfirst($form->error($model, 'fare_base', array('class' => 'text-danger'))); ?>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Fare Time <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<input type="text" placeholder="Fare Time" class="form-control" value="<?php echo $model->fare_time ?>" name="fare_time"/>
														<span class="help-block">
															 
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Fare Distance <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<input type="text" placeholder="Fare Time" class="form-control" value="<?php echo $model->fare_distance ?>" name="fare_distance"/>
														<span class="help-block">
															 
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Commision <small>( in % )</small> <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<input type="text" placeholder="Commision" class="form-control" value="<?php echo $model->fare_commision ?>" name="fare_commision"/>
														<span class="help-block">
															 
														</span>
													</div>
												</div>
											</div>
											<div class="form-actions fluid">
												<div class="row">
													<div class="col-md-12">
														<div class="col-md-offset-3 col-md-2">
															<button type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>
														</div>

														<?php 
															if (Yii::app()->user->hasFlash('message')): 
																$type = Yii::app()->user->getFlash('type');
														?>
																<?php 
																	if($type == 'success'):
																?>
																	<div class="col-md-6 alert alert-<?php echo $type;?>">
																<?php 
																	else:
																?>
																	<div class="col-md-6 alert alert-<?php echo $type;?>">
																<?php
																	endif;
																?>
																		<span>
																			<?php 
																				echo Yii::app()->user->getFlash('message'); 
																				Yii::app()->user->setFlash('message', null);
																			?>
																		</span>
																	</div>
																<?php
																	 endif;
																?> 

													</div>
												</div>
											</div>
										<!-- </form> -->
									<?php 
										$this->endWidget();
									?>
										<!-- END FORM-->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo Utils::getStyleUrl() ?>plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo Utils::getStyleUrl() ?>plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo Utils::getStyleUrl() ?>plugins/jquery-validation/dist/additional-methods.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo Utils::getStyleUrl() ?>scripts/core/app.js"></script>
<script src="<?php echo Utils::getStyleUrl() ?>scripts/custom/form-samples.js"></script>
<script src="<?php echo Utils::getStyleUrl() ?>scripts/custom/form-validation.js"></script>
<?php 
if(!empty($js_file))
	echo '<script src="'.Utils::getStyleUrl() .'scripts/user/'.$js_file.'.js" type="text/javascript"></script>';
?>
<!-- END PAGE LEVEL SCRIPTS -->

<script>
jQuery(document).ready(function() {    

   // initiate layout and plugins
   App.init();
   FormSamples.init();
   FormValidation.init();
});

function readURL(input, id) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#'+id).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imgInp").change(function(){
    readURL(this, 'pimg');
});
$("#imgLic1").change(function(){
    readURL(this, 'lic1');
});
$("#imgLic2").change(function(){
    readURL(this, 'lic2');
});
$("#imgReg").change(function(){
    readURL(this, 'reg');
});
$("#imgIns").change(function(){
    readURL(this, 'ins');
});


</script>

