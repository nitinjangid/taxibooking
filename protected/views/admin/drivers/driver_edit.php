	<?php $this->pageTitle = ucwords($title);?>
	<?php
	// echo '<pre>';print_r($model2);die;

	?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo Utils::getStyleUrl() ?>plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo Utils::getStyleUrl() ?>plugins/select2/select2-metronic.css"/>
<?php 
	if(!empty($css_file))
		echo '<link href="'.Utils::getStyleUrl().'css/user/'.$css_file.'.css" rel="stylesheet" type="text/css"/>';
?>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Drivers Management <small></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('home') ?>">
								Dashboard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<i class="fa fa-user"></i>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('drivers/drivers') ?>">
								Drivers Management
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<i class="fa fa-user"></i>
							<a href="javascript:;">
								Driver Details
							</a>
						</li>
						
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="tabbable tabbable-custom boxless tabbable-reversed">
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#tab_1" data-toggle="tab">
									 Personal Information
								</a>
							</li>
							<li>
								<a href="#tab_2" data-toggle="tab">
									 Address
								</a>
							</li>
							<li>
								<a href="#tab_3" data-toggle="tab">
									 Licence
								</a>
							</li>
							<li>
								<a href="#tab_4" data-toggle="tab">
									 Account Details
								</a>
							</li>
							<li>
								<a href="#tab_5" data-toggle="tab">
									 Vehicle Document
								</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab_1">
								<div class="portlet box blue">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-reorder"></i>Driver Information Form
										</div>
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
										<?php
											// echo '<pre>';print_r($model);die;
	                                    	$form = $this->beginWidget('CActiveForm', array(
	                                            'id' => 'personalinformation-form',
	                                            'action' => Yii::app()->createAbsoluteUrl('drivers/updateDriver'),
	                                           
	                                            'htmlOptions' => array(
	                                                'enctype' => 'multipart/form-data',
	                                                'autocomplete' => 'off',
	                                                'role' => 'form',
	                                                'class'=>'form-horizontal form-bordered form-row-stripped',
	                                            ),
	                                            'focus' => array($model1, 'driver_fname'),
	                                        ));
	                                    ?>
	                                    	<input type="hidden" name="id" value="<?php echo $model1->driver_id;?>">
										<!-- <form action="<?php //echo Yii::app()->createAbsoluteUrl('drivers/registerDriver') ?>" class="form-horizontal form-bordered form-row-stripped" method="post" enctype="multipart/form-data"> -->
											<div class="form-body">
												<div class="form-group">
													<label class="control-label col-md-3">Image <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
													<?php $image = !empty($model1->driver_avatar) ? Utils::DriverImagePath().'driver-'.$model1->driver_id.'/avatar/'.$model1->driver_avatar : Utils::UserImagePath().'noimg/no-image.png' ;?>
													<div class="imagePreview"><img id="pimg" src="<?php echo $image; ?>" alt="" /></div>
														<input type="file" class="form-control" name="driver_avatar" id="imgInp"/>
														<span class="help-block">
															 
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">First Name <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<input type="text" placeholder="First Name" class="form-control" value="<?php echo $model1->driver_fname ?>" name="driver_fname"/>
														<?php echo ucfirst($form->error($model1, 'driver_fname', array('class' => 'text-danger'))); ?>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Last Name <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<input type="text" placeholder="Last Name" class="form-control" value="<?php echo $model1->driver_lname ?>" name="driver_lname"/>
														<span class="help-block">
															 
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Gender <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<select class="form-control" name="driver_gender">
															<option value="M" <?php echo ($model1->driver_gender == 'M' ? 'selected="selected"' : '');?>>Male</option>
															<option value="F" <?php echo ($model1->driver_gender == 'F' ? 'selected="selected"' : '');?>>Female</option>
														</select>
														<span class="help-block">
															 
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Email <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<input type="text" placeholder="Email" class="form-control" value="<?php echo strtolower($model1->driver_email) ?>" name="driver_email"/>
														<span class="help-block">
															 
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Mobile <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<input type="text" placeholder="Mobile" class="form-control" maxlength="10" value="<?php echo $model1->driver_mobile ?>" name="driver_mobile"/>
														<span class="help-block">
															 
														</span>
													</div>
												</div>
											</div>
											<div class="form-actions fluid">
												<div class="row">
													<div class="col-md-12">
														<div class="col-md-offset-3 col-md-2">
															<button type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>
														</div>

														<?php 
															if (Yii::app()->user->hasFlash('message')): 
																$type = Yii::app()->user->getFlash('type');
														?>
																<?php 
																	if($type == 'success'):
																?>
																	<div class="col-md-6 alert alert-<?php echo $type;?>">
																<?php 
																	else:
																?>
																	<div class="col-md-6 alert alert-<?php echo $type;?>">
																<?php
																	endif;
																?>
																		<span>
																			<?php 
																				echo Yii::app()->user->getFlash('message'); 
																				Yii::app()->user->setFlash('message', null);
																			?>
																		</span>
																	</div>
																<?php
																	 endif;
																?> 

													</div>
												</div>
											</div>
										<!-- </form> -->
									<?php 
										$this->endWidget();
									?>
										<!-- END FORM-->
									</div>
								</div>
							</div>

							<div class="tab-pane" id="tab_2">
								<div class="portlet box blue">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-reorder"></i>Address Form
										</div>
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
										<?php
											// echo '<pre>';print_r($model);die;
	                                    	$form = $this->beginWidget('CActiveForm', array(
	                                            'id' => 'personalinformation-form',
	                                            'action' => Yii::app()->createAbsoluteUrl('drivers/insertAddress'),
	                                           
	                                            'htmlOptions' => array(
	                                                'enctype' => 'multipart/form-data',
	                                                'autocomplete' => 'off',
	                                                'role' => 'form',
	                                                'class'=>'form-horizontal form-bordered form-row-stripped',
	                                            ),
	                                            'focus' => array($model2, 'dd_street'),
	                                        ));
	                                    ?>
	                                    	<input type="hidden" name="id" value="<?php echo $model1->driver_id;?>">
										<!-- <form action="<?php //echo Yii::app()->createAbsoluteUrl('drivers/registerDriver') ?>" class="form-horizontal form-bordered form-row-stripped" method="post" enctype="multipart/form-data"> -->
											<div class="form-body">
												<div class="form-group">
													<label class="control-label col-md-3">Street <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<input type="text" class="form-control" placeholder="Street" value="<?php echo !empty($model2->dd_street) ? $model2->dd_street :'';?>" name="dd_street">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">City <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<input type="text" class="form-control" placeholder="City" value="<?php echo !empty($model2->dd_city) ? $model2->dd_city :'';?>" name="dd_city">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">State <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<input type="text" class="form-control" placeholder="State" value="<?php echo !empty($model2->dd_state) ? $model2->dd_state :'';?>" name="dd_state">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Post Code <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<input type="text" class="form-control" placeholder="Post Code" maxlength="6" value="<?php echo !empty($model2->dd_pin) ? $model2->dd_pin :'';?>" name="dd_pin">
													</div>
												</div>
												<div class="form-group last">
													<label class="control-label col-md-3">Country <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<select class="form-control" name="dd_country">
															<option value="" selected="selected" disabled="disabled">Select Country</option>
															<?php
																if(!empty($countries)):
																	foreach ($countries as $country):
															?>
															<option value="<?php echo $country['nickname'];?>" <?php echo !empty($model2->dd_country) ? ($country['nickname'] == $model2->dd_country) ? 'selected="selected"' : '' : '' ; ?> ><?php echo $country['nickname'];?></option>
															<?php
																	endforeach;
																endif;
															?>
														</select>
													</div>
												</div>
											</div>
											<div class="form-actions fluid">
												<div class="row">
													<div class="col-md-12">
														<div class="col-md-offset-3 col-md-2">
															<button type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>
														</div>

														<?php 
															if (Yii::app()->user->hasFlash('message2')): 
																$type = Yii::app()->user->getFlash('type2');
														?>
																<?php 
																	if($type == 'success'):
																?>
																	<div class="col-md-6 alert alert-<?php echo $type;?>">
																<?php 
																	else:
																?>
																	<div class="col-md-6 alert alert-<?php echo $type;?>">
																<?php
																	endif;
																?>
																		<span>
																			<?php 
																				echo Yii::app()->user->getFlash('message2'); 
																				Yii::app()->user->setFlash('message2', null);
																			?>
																		</span>
																	</div>
																<?php
																	 endif;
																?> 

													</div>
												</div>
											</div>
										<!-- </form> -->
									<?php 
										$this->endWidget();
									?>
										<!-- END FORM-->
									</div>
								</div>
							</div>
							
							<div class="tab-pane" id="tab_3">
								<div class="portlet box blue">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-reorder"></i>Driver Information Form
										</div>
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
										<?php
											// echo '<pre>';print_r($model);die;
	                                    	$form = $this->beginWidget('CActiveForm', array(
	                                            'id' => 'licenceupload-form',
	                                            'action' => Yii::app()->createAbsoluteUrl('drivers/updateLicence'),
	                                           
	                                            'htmlOptions' => array(
	                                                'enctype' => 'multipart/form-data',
	                                                'autocomplete' => 'off',
	                                                'role' => 'form',
	                                                'class'=>'form-horizontal form-bordered form-row-stripped',
	                                            ),
	                                            'focus' => array($model2, 'dd_licence_front'),
	                                        ));
	                                    ?>
	                                    <input type="hidden" name="id" value="<?php echo $model1->driver_id;?>">
										<!-- <form action="<?php //echo Yii::app()->createAbsoluteUrl('drivers/registerDriver') ?>" class="form-horizontal form-bordered form-row-stripped" method="post" enctype="multipart/form-data"> -->
											<div class="form-body">
												<div class="form-group">
													<label class="control-label col-md-3">Licence Number <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<input type="text" class="form-control" placeholder="Licence Number" value="<?php echo !empty($model2->dd_licence_no) ? $model2->dd_licence_no : '' ;?>" name="driver_licence_no">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Licence (front) <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
													<?php $image = !empty($model2->dd_licence_front) ? Utils::DriverImagePath().'driver-'.$model1->driver_id.'/licence/'.$model2->dd_licence_front : Utils::UserImagePath().'noimg/no-image.png' ;?>
													<div class="imagePreviewLic"><img id="lic1" src="<?php echo $image; ?>" alt="" /></div>
														<input type="file" class="form-control" name="dd_licence_front" id="imgLic1"/>
														<span class="help-block">
															 
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Licence (back) <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
													<?php $image = !empty($model2->dd_licence_back) ? Utils::DriverImagePath().'driver-'.$model1->driver_id.'/licence/'.$model2->dd_licence_back : Utils::UserImagePath().'noimg/no-image.png' ;?>
													<div class="imagePreviewLic"><img id="lic2" src="<?php echo $image; ?>" alt="" /></div>
														<input type="file" class="form-control" name="dd_licence_back" id="imgLic2"/>
														<span class="help-block">
															 
														</span>
													</div>
												</div>
												
											</div>
											<div class="form-actions fluid">
												<div class="row">
													<div class="col-md-12">
														<div class="col-md-offset-3 col-md-2">
															<button type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>
														</div>
														<?php 
															if (Yii::app()->user->hasFlash('message3')): 
																$type = Yii::app()->user->getFlash('type3');
														?>
																<?php 
																	if($type == 'success'):
																?>
																	<div class="col-md-6 alert alert-<?php echo $type;?>">
																<?php 
																	else:
																?>
																	<div class="col-md-6 alert alert-<?php echo $type;?>">
																<?php
																	endif;
																?>
																		<span>
																			<?php 
																				echo Yii::app()->user->getFlash('message3'); 
																				Yii::app()->user->setFlash('message3', null);
																			?>
																		</span>
																	</div>
																<?php
																	 endif;
																?> 

														</div>
													</div>
												</div>
											<!-- </form> -->
										<?php 
											$this->endWidget();
										?>
											<!-- END FORM-->
										</div>
									</div>
							</div>

                           <div class="tab-pane" id="tab_4">
								<div class="portlet box blue">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-reorder"></i>Driver Account Details
										</div>
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
										<?php
											// echo '<pre>';print_r($model);die;
	                                    	$form = $this->beginWidget('CActiveForm', array(
	                                            'id' => 'driveraccountdetails-form',
	                                            'action' => Yii::app()->createAbsoluteUrl('drivers/updateAccountDetails'),
	                                           
	                                            'htmlOptions' => array(
	                                                'enctype' => 'multipart/form-data',
	                                                'autocomplete' => 'off',
	                                                'role' => 'form',
	                                                'class'=>'form-horizontal form-bordered form-row-stripped',
	                                            ),
	                                            'focus' => array($model2, 'dd_vehicle_registration'),
	                                        ));
	                                    ?>
	                                    <input type="hidden" name="id" value="<?php echo $model1->driver_id;?>">
										<!-- <form action="<?php //echo Yii::app()->createAbsoluteUrl('drivers/registerDriver') ?>" class="form-horizontal form-bordered form-row-stripped" method="post" enctype="multipart/form-data"> -->
											<div class="form-body">
												
												<div class="form-group">
													<label class="control-label col-md-3">Bank Name<sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<input type="text" class="form-control" placeholder="Bank Name" value="<?php echo !empty($model2->dd_bankName) ? $model2->dd_bankName : '' ;?>" name="dd_bankName">
													</div>
												</div>

												<div class="form-group">
													<label class="control-label col-md-3">Account Number<sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<input type="text" class="form-control" placeholder="Account Number" value="<?php echo !empty($model2->dd_accountNumber) ? $model2->dd_accountNumber : '' ;?>" name="dd_accountNumber">
													</div>
												</div>
												
											</div>
											<div class="form-actions fluid">
												<div class="row">
													<div class="col-md-12">
														<div class="col-md-offset-3 col-md-2">
															<button type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>
														</div>
														<?php 
															if (Yii::app()->user->hasFlash('message4')): 
																$type = Yii::app()->user->getFlash('type4');
														?>
																<?php 
																	if($type == 'success'):
																?>
																	<div class="col-md-6 alert alert-<?php echo $type;?>">
																<?php 
																	else:
																?>
																	<div class="col-md-6 alert alert-<?php echo $type;?>">
																<?php
																	endif;
																?>
																		<span>
																			<?php 
																				echo Yii::app()->user->getFlash('message4'); 
																				Yii::app()->user->setFlash('message4', null);
																			?>
																		</span>
																	</div>
																<?php
																	 endif;
																?> 

														</div>
													</div>
												</div>
											<!-- </form> -->
										<?php 
											$this->endWidget();
										?>
											<!-- END FORM-->
										</div>
									</div>
							</div>
							<div class="tab-pane" id="tab_5">
								<div class="portlet box blue">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-reorder"></i>Driver Information Form
										</div>
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
										<?php
											// echo '<pre>';print_r($model);die;
	                                    	$form = $this->beginWidget('CActiveForm', array(
	                                            'id' => 'vehicledocumentupload-form',
	                                            'action' => Yii::app()->createAbsoluteUrl('drivers/updateVehicleDocument'),
	                                           
	                                            'htmlOptions' => array(
	                                                'enctype' => 'multipart/form-data',
	                                                'autocomplete' => 'off',
	                                                'role' => 'form',
	                                                'class'=>'form-horizontal form-bordered form-row-stripped',
	                                            ),
	                                            'focus' => array($model2, 'dd_vehicle_registration'),
	                                        ));
	                                    ?>
	                                    <input type="hidden" name="id" value="<?php echo $model1->driver_id;?>">
										<!-- <form action="<?php //echo Yii::app()->createAbsoluteUrl('drivers/registerDriver') ?>" class="form-horizontal form-bordered form-row-stripped" method="post" enctype="multipart/form-data"> -->
											<div class="form-body">
												<div class="form-group">
													<label class="control-label col-md-3">Vehicle Registration <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
													<?php $image = !empty($model2->dd_vehicle_registration) ? Utils::DriverImagePath().'driver-'.$model1->driver_id.'/vehicle/'.$model2->dd_vehicle_registration : Utils::UserImagePath().'noimg/no-image.png' ;?>
													<div class="imagePreviewVeh"><img id="reg" src="<?php echo $image ?>" alt="" /></div>
														<input type="file" class="form-control" name="dd_vehicle_registration" id="imgReg"/>
														<span class="help-block">
															 
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Vehicle Insurance <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
													<?php $image = !empty($model2->dd_vehicle_insurance) ? Utils::DriverImagePath().'driver-'.$model1->driver_id.'/vehicle/'.$model2->dd_vehicle_insurance : Utils::UserImagePath().'noimg/no-image.png' ;?>
													<div class="imagePreviewVeh"><img id="ins" src="<?php echo $image ?>" alt="" /></div>
														<input type="file" class="form-control" name="dd_vehicle_insurance" id="imgIns"/>
														<span class="help-block">
															 
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Vehicle Maker <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<input type="text" class="form-control" placeholder="Vehicle Maker" value="<?php echo !empty($model2->dd_vehicle_make) ? $model2->dd_vehicle_make : '' ;?>" name="dd_vehicle_make">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Vehicle Model <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<input type="text" class="form-control" placeholder="Vehicle Model" value="<?php echo !empty($model2->dd_vehicle_model) ? $model2->dd_vehicle_model : '' ;?>" name="dd_vehicle_model">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Vehicle Year <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<input type="text" class="form-control" placeholder="Vehicle Year" minlength="4" maxlength="4" value="<?php echo !empty($model2->dd_vehicle_year) ? $model2->dd_vehicle_year : '' ;?>" name="dd_vehicle_year">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Vehicle Seats <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<input type="text" class="form-control" placeholder="Vehicle Seats" maxlength="3" value="<?php echo !empty($model2->dd_vehicle_year) ? $model2->dd_vehicle_seat : '' ;?>" name="dd_vehicle_seat">
													</div>
												</div>
												
											</div>
											<div class="form-actions fluid">
												<div class="row">
													<div class="col-md-12">
														<div class="col-md-offset-3 col-md-2">
															<button type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>
														</div>
														<?php 
															if (Yii::app()->user->hasFlash('message5')): 
																$type = Yii::app()->user->getFlash('type5');
														?>
																<?php 
																	if($type == 'success'):
																?>
																	<div class="col-md-6 alert alert-<?php echo $type;?>">
																<?php 
																	else:
																?>
																	<div class="col-md-6 alert alert-<?php echo $type;?>">
																<?php
																	endif;
																?>
																		<span>
																			<?php 
																				echo Yii::app()->user->getFlash('message5'); 
																				Yii::app()->user->setFlash('message5', null);
																			?>
																		</span>
																	</div>
																<?php
																	 endif;
																?> 

														</div>
													</div>
												</div>
											<!-- </form> -->
										<?php 
											$this->endWidget();
										?>
											<!-- END FORM-->
										</div>
									</div>
								</div>								
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo Utils::getStyleUrl() ?>plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo Utils::getStyleUrl() ?>plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo Utils::getStyleUrl() ?>plugins/jquery-validation/dist/additional-methods.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo Utils::getStyleUrl() ?>scripts/core/app.js"></script>
<script src="<?php echo Utils::getStyleUrl() ?>scripts/custom/form-samples.js"></script>
<script src="<?php echo Utils::getStyleUrl() ?>scripts/custom/form-validation.js"></script>
<?php 
if(!empty($js_file))
	echo '<script src="'.Utils::getStyleUrl() .'scripts/user/'.$js_file.'.js" type="text/javascript"></script>';
?>
<!-- END PAGE LEVEL SCRIPTS -->

<script>
jQuery(document).ready(function() {    

   // initiate layout and plugins
   App.init();
   FormSamples.init();
   FormValidation.init();
});

function readURL(input, id) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#'+id).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imgInp").change(function(){
    readURL(this, 'pimg');
});
$("#imgLic1").change(function(){
    readURL(this, 'lic1');
});
$("#imgLic2").change(function(){
    readURL(this, 'lic2');
});
$("#imgReg").change(function(){
    readURL(this, 'reg');
});
$("#imgIns").change(function(){
    readURL(this, 'ins');
});


</script>

