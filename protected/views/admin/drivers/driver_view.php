	<?php $this->pageTitle = ucwords($title);?>
	<?php
	// echo '<pre>';print_r($driverData);die;

	?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo Utils::getStyleUrl() ?>plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo Utils::getStyleUrl() ?>plugins/select2/select2-metronic.css"/>
<?php 
	if(!empty($css_file))
		echo '<link href="'.Utils::getStyleUrl().'css/user/'.$css_file.'.css" rel="stylesheet" type="text/css"/>';
?>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Drivers Management <small></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('home') ?>">
								Dashboard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<i class="fa fa-user"></i>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('drivers') ?>">
								Drivers Management
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<i class="fa fa-user"></i>
							<a href="javascript:;">
								Driver Details
							</a>
						</li>
						
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
							<div class="tab-content">
								<div class="tab-pane active" id="tab_1">
									<div class="portlet box blue">
										<div class="portlet-title">
											<div class="caption">
												<i class="fa fa-reorder"></i>Driver Information
											</div>
										</div>
										<div class="portlet-body form">
											<!-- BEGIN FORM-->
											<form class="form-horizontal" role="form">
												<div class="form-body">
													<div class="margin-bottom-20 col-md-12"> 
														<h2>Driver Info - <?php echo ucwords($model->driver_fname .' '.$model->driver_lname);?> </h2>
													</div>
													

													<!-- Person info -->
													<h3 class="form-section">Person Info</h3>
													<div class="row">
														<div class="col-md-6 margin-left-40">
															<div class="row">
																<div class="col-md-6">
																	<div class="form-group margin-bottom-5">
																		<label class="control-label col-md-3"><strong>Name:</strong></label>
																		<div class="col-md-9">
																			<p class="form-control-static">
																				<?php
																				if(!empty($model->driver_fname)):
																				  	echo ucwords($model->driver_fname .' '.$model->driver_lname);
																				else:
																					echo '<i>NA</i>';
																				endif;
																				?>
																			</p>
																		</div>
																	</div>
																</div>
															</div>
															<!--/row-->
															<div class="row">
																<div class="col-md-6">
																	<div class="form-group margin-bottom-5">
																		<label class="control-label col-md-3"><strong>E-Mail:</strong></label>
																		<div class="col-md-9">
																			<p class="form-control-static">
																				<?php 
																				if(!empty($model->driver_email)):
																				 	echo strtolower($model->driver_email);
																				else:
																					echo '<i>NA</i>';
																				endif;
																				?>
																			</p>
																		</div>
																	</div>
																</div>
															</div>
															<!--/row-->
															<div class="row">
																<div class="col-md-6">
																	<div class="form-group margin-bottom-5">
																		<label class="control-label col-md-3"><strong>Mobile:</strong></label>
																		<div class="col-md-9">
																			<p class="form-control-static">
																				<?php 
																				if(!empty($model->driver_mobile)): 
																				 	echo $model->driver_mobile;
																				else:
																					echo '<i>NA</i>';
																				endif;
																				?>
																			</p>
																		</div>
																	</div>
																</div>
															</div>
															<!--/row-->
															<div class="row">
																<div class="col-md-6">
																	<div class="form-group margin-bottom-5">
																		<label class="control-label col-md-3"><strong>Address:</strong></label>
																		<div class="col-md-9">
																			<p class="form-control-static">
																				 <?php
																				 if(!empty($model2->dd_street)):
																				 	echo ucfirst($model2->dd_street).',<br>'.ucfirst($model2->dd_city).' ('. $model2->dd_pin .') ,<br>'.ucfirst($model2->dd_state).',<br>'.strtoupper($model2->dd_country);
																				 else:
																				 	echo '<i>NA</i>';
																				 endif;
																				?>
																			</p>
																		</div>
																	</div>
																</div>
															</div>
															<!--/row-->
														</div>
														<!-- / col-md-6 -->

														<div class="col-md-5">
															<?php $image = !empty($model->driver_avatar) ? Utils::DriverImagePath().'driver-'.$model->driver_id.'/avatar/'.$model->driver_avatar : Utils::UserImagePath().'noimg/1389952697.png' ;?>
															<div class="imagePreview pull-right">
																<img id="pimg" src="<?php echo $image; ?>" alt="" />
															</div>
														</div>
														<!-- / col-md-6 -->
													</div>
													<!--/row-->
													
													<!-- Licence ifnfo -->
													<h3 class="form-section">Licence Info</h3>

													<div class="row">
														<div class="col-md-6">
															<div class="form-group margin-bottom-5">
																<label class="control-label col-md-3"><strong>Number:</strong></label>
																<div class="col-md-9">
																	<p class="form-control-static">
																		<?php 
																		if(!empty($model2->dd_licence_no)):
																			echo ucfirst($model2->dd_licence_no);
																		else:
																			echo '<i>NA</i>';
																		endif;
																		?>
																	</p>
																</div>
															</div>
														</div>
													</div>
													<!--/row-->

													<div class="row">
														<div class="col-md-6">
															<div class="form-group margin-bottom-5">
																<label class="control-label col-md-3"><strong>Front:</strong></label>
																<div class="col-md-9">
																		 <?php $licenceFront = !empty($model2->dd_licence_front) ? Utils::DriverImagePath().'driver-'.$model->driver_id.'/licence/'.$model2->dd_licence_front : Utils::UserImagePath().'noimg/no-image.png' ;?>
																			<div class="imagePreviewLic"><img id="lic1" src="<?php echo $licenceFront; ?>" alt="" /></div>
																</div>
															</div>
														</div>

														<div class="col-md-6">
															<div class="form-group margin-bottom-5">
																<label class="control-label col-md-3"><strong>Back:</strong></label>
																<div class="col-md-9">
																	<?php $licenceBack = !empty($model2->dd_licence_back) ? Utils::DriverImagePath().'driver-'.$model->driver_id.'/licence/'.$model2->dd_licence_back : Utils::UserImagePath().'noimg/no-image.png' ;?>
																	<div class="imagePreviewLic"><img id="lic2" src="<?php echo $licenceBack; ?>" alt="" /></div>
																</div>
															</div>
														</div>
													</div>
													<!--/row-->

													<!-- Vehcle ifnfo -->
													<h3 class="form-section">Vehicle Info</h3>

													<div class="row">
														<div class="col-md-6">
															<div class="form-group margin-bottom-5">
																<label class="control-label col-md-3"><strong>Maker:</strong></label>
																<div class="col-md-9">
																	<p class="form-control-static">
																		<?php 
																		if(!empty($model2->dd_vehicle_make)):
																			echo ucfirst($model2->dd_vehicle_make);
																		else:
																			echo '<i>NA</i>';
																		endif;
																		?>
																	</p>
																</div>
															</div>
														</div>
													</div>
													<!--/row-->
													<div class="row">
														<div class="col-md-6">
															<div class="form-group margin-bottom-5">
																<label class="control-label col-md-3"><strong>Model:</strong></label>
																<div class="col-md-9">
																	<p class="form-control-static">
																		<?php 
																		if(!empty($model2->dd_vehicle_model)):
																			echo $model2->dd_vehicle_model;
																		else:
																			echo '<i>NA</i>';
																		endif;
																		?> 
																	</p>
																</div>
															</div>
														</div>
													</div>
													<!--/row-->
													<div class="row">
														<div class="col-md-6">
															<div class="form-group margin-bottom-5">
																<label class="control-label col-md-3"><strong>Year:</strong></label>
																<div class="col-md-9">
																	<p class="form-control-static">
																		<?php 
																		if(!empty($model2->dd_vehicle_year)):
																			echo $model2->dd_vehicle_year;
																		else:
																			echo '<i>NA</i>';
																		endif;
																		?> 
																	</p>
																</div>
															</div>
														</div>
													</div>
													<!--/row-->

													<div class="row">
														<div class="col-md-6">
															<div class="form-group margin-bottom-5">
																<label class="control-label col-md-3"><strong>Registration:</strong></label>
																<div class="col-md-9">
																	<?php $registration = !empty($model2->dd_vehicle_registration) ? Utils::DriverImagePath().'driver-'.$model->driver_id.'/vehicle/'.$model2->dd_vehicle_registration : Utils::UserImagePath().'noimg/no-image.png' ;?>
																	<div class="imagePreviewVeh"><img id="reg" src="<?php echo $registration ?>" alt="" /></div>
																</div>
															</div>
														</div>

														<div class="col-md-6">
															<div class="form-group margin-bottom-5">
																<label class="control-label col-md-3"><strong>Insurance:</strong></label>
																<div class="col-md-9">
																	<?php $insurance = !empty($model2->dd_vehicle_insurance) ? Utils::DriverImagePath().'driver-'.$model->driver_id.'/vehicle/'.$model2->dd_vehicle_insurance : Utils::UserImagePath().'noimg/no-image.png' ;?>
																	<div class="imagePreviewVeh"><img id="ins" src="<?php echo $insurance ?>" alt="" /></div>
																</div>
															</div>
														</div>
													</div>
													<!--/row-->

												</div>
												<!-- /form-body -->

												<div class="form-actions fluid">
													<div class="row">
														<div class="col-md-6">
															<div class="col-md-offset-3 col-md-9">
																<a href="<?php echo Utils::getBaseUrl().'/drivers/driverInfoEdit/'.$model->driver_id ;?>" class="btn green edit-info"><i class="fa fa-pencil"></i> Edit</a>
															</div>
														</div>
														<div class="col-md-6">
														</div>
													</div>
												</div>
											</form>
											<!-- END FORM-->
										</div>
									</div>
								</div>								
							</div>
						</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo Utils::getStyleUrl() ?>plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo Utils::getStyleUrl() ?>plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo Utils::getStyleUrl() ?>plugins/jquery-validation/dist/additional-methods.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo Utils::getStyleUrl() ?>scripts/core/app.js"></script>
<script src="<?php echo Utils::getStyleUrl() ?>scripts/custom/form-samples.js"></script>
<script src="<?php echo Utils::getStyleUrl() ?>scripts/custom/form-validation.js"></script>
<?php 
if(!empty($js_file))
	echo '<script src="'.Utils::getStyleUrl() .'scripts/user/'.$js_file.'.js" type="text/javascript"></script>';
?>
<!-- END PAGE LEVEL SCRIPTS -->

<script>
jQuery(document).ready(function() {    

   // initiate layout and plugins
   App.init();
   FormSamples.init();
   FormValidation.init();
});

function readURL(input, id) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#'+id).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imgInp").change(function(){
    readURL(this, 'pimg');
});
$("#imgLic1").change(function(){
    readURL(this, 'lic1');
});
$("#imgLic2").change(function(){
    readURL(this, 'lic2');
});
$("#imgReg").change(function(){
    readURL(this, 'reg');
});
$("#imgIns").change(function(){
    readURL(this, 'ins');
});


</script>

