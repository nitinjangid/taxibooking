	<?php $this->pageTitle = ucwords($title);?>

<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo Utils::getStyleUrl() ?>plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo Utils::getStyleUrl() ?>plugins/select2/select2-metronic.css"/>
<?php 
	if(!empty($css_file))
		echo '<link href="'.Utils::getStyleUrl().'css/user/'.$css_file.'.css" rel="stylesheet" type="text/css"/>';
?>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Drivers Management <small></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('home') ?>">
								Dashboard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<i class="fa fa-user"></i>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('drivers/drivers') ?>">
								Drivers Management
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="javascript:;">
								Register Driver
							</a>
						</li>
						
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="tabbable tabbable-custom boxless tabbable-reversed">
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#tab_1" data-toggle="tab">
									 Personal Information
								</a>
							</li>
							<!-- <li>
								<a href="#tab_2" data-toggle="tab">
									 Address
								</a>
							</li>
							<li>
								<a href="#tab_3" data-toggle="tab">
									 Licence
								</a>
							</li>
							<li>
								<a href="#tab_4" data-toggle="tab">
									 Vehicle Document
								</a>
							</li> -->
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab_1">
								<div class="portlet box blue">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-reorder"></i>Driver Information Form
										</div>
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
										<?php
	                                    	$form = $this->beginWidget('CActiveForm', array(
	                                            'id' => 'personalinformation-form',
	                                            'action' => 'registerDriver',
	                                           
	                                            'htmlOptions' => array(
	                                                'enctype' => 'multipart/form-data',
	                                                'autocomplete' => 'off',
	                                                'role' => 'form',
	                                                'class'=>'form-horizontal form-bordered form-row-stripped',
	                                            ),
	                                            'focus' => array($model, 'driver_fname'),
	                                        ));
	                                    ?>
										<!-- <form action="<?php //echo Yii::app()->createAbsoluteUrl('drivers/registerDriver') ?>" class="form-horizontal form-bordered form-row-stripped" method="post" enctype="multipart/form-data"> -->
											<div class="form-body">
												<div class="form-group">
													<label class="control-label col-md-3">Image <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
													<div class="imagePreview"><img id="pimg" src="<?php echo Utils::UserImagePath().'noimg/no-image.png' ?>" alt="" /></div>
														<input type="file" class="form-control" name="driver_avatar" id="imgInp"/>
														<span class="help-block">
															 
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">First Name <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<input type="text" placeholder="First Name" class="form-control" name="driver_fname"/>
														<?php echo ucfirst($form->error($model, 'driver_fname', array('class' => 'text-danger'))); ?>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Last Name <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<input type="text" placeholder="Last Name" class="form-control" name="driver_lname"/>
														<span class="help-block">
															 
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Gender <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<select class="form-control" name="driver_gender">
															<option value="M">Male</option>
															<option value="F">Female</option>
														</select>
														<span class="help-block">
															 
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Email <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<input type="text" placeholder="Email" class="form-control" name="driver_email"/>
														<span class="help-block">
															 
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Mobile <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<input type="text" placeholder="Mobile" class="form-control" maxlength="10" name="driver_mobile"/>
														<span class="help-block">
															 
														</span>
													</div>
												</div>
											</div>
											<div class="form-actions fluid">
												<div class="row">
													<div class="col-md-12">
														<div class="col-md-offset-3 col-md-2">
															<button type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>
														</div>

														<?php 
															if (Yii::app()->user->hasFlash('message')): 
																$type = Yii::app()->user->getFlash('type');
														?>
																<?php 
																	if($type == 'success'):
																?>
																	<div class="col-md-6 alert alert-<?php echo $type;?>">
																<?php 
																	else:
																?>
																	<div class="col-md-6 alert alert-<?php echo $type;?>">
																<?php
																	endif;
																?>
																		<span>
																			<?php 
																				echo Yii::app()->user->getFlash('message'); 
																				Yii::app()->user->setFlash('message', null);
																			?>
																		</span>
																	</div>
																<?php
																	 endif;
																?> 

													</div>
												</div>
											</div>
										<!-- </form> -->
									<?php 
										$this->endWidget();
									?>
										<!-- END FORM-->
									</div>
								</div>
							</div>

							<div class="tab-pane" id="tab_2">
								<div class="portlet box blue">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-reorder"></i>Address Form
										</div>
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
										<?php
	                                    	$form = $this->beginWidget('CActiveForm', array(
	                                            'id' => 'personalinformation-form',
	                                            'action' => 'insertAddress',
	                                           
	                                            'htmlOptions' => array(
	                                                'enctype' => 'multipart/form-data',
	                                                'autocomplete' => 'off',
	                                                'role' => 'form',
	                                                'class'=>'form-horizontal form-bordered form-row-stripped',
	                                            ),
	                                            'focus' => array($model, 'driver_fname'),
	                                        ));
	                                    ?>
										<!-- <form action="<?php //echo Yii::app()->createAbsoluteUrl('drivers/registerDriver') ?>" class="form-horizontal form-bordered form-row-stripped" method="post" enctype="multipart/form-data"> -->
											<div class="form-body">
												<div class="form-group">
													<label class="control-label col-md-3">Street <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<input type="text" class="form-control" placeholder="Street" name="dd_street">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">City <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<input type="text" class="form-control" placeholder="City" name="dd_city">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">State <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<input type="text" class="form-control" placeholder="State" name="dd_state">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Post Code <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<input type="text" class="form-control" placeholder="Post Code" maxlength="6" name="dd_pin">
													</div>
												</div>
												<div class="form-group last">
													<label class="control-label col-md-3">Country <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<select class="form-control" name="dd_country">
															<option value="" selected="selected" disabled="disabled">Select Country</option>
															<?php
																if(!empty($countries)):
																	foreach ($countries as $country):
															?>
															<option value="<?php echo $country['nickname'];?>"><?php echo $country['nickname'];?></option>
															<?php
																	endforeach;
																endif;
															?>
														</select>
													</div>
												</div>
											</div>
											<div class="form-actions fluid">
												<div class="row">
													<div class="col-md-12">
														<div class="col-md-offset-3 col-md-2">
															<button type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>
														</div>

														<?php 
															if (Yii::app()->user->hasFlash('message2')): 
																$type = Yii::app()->user->getFlash('type2');
														?>
																<?php 
																	if($type == 'success'):
																?>
																	<div class="col-md-6 alert alert-<?php echo $type;?>">
																<?php 
																	else:
																?>
																	<div class="col-md-6 alert alert-<?php echo $type;?>">
																<?php
																	endif;
																?>
																		<span>
																			<?php 
																				echo Yii::app()->user->getFlash('message2'); 
																				Yii::app()->user->setFlash('message2', null);
																			?>
																		</span>
																	</div>
																<?php
																	 endif;
																?> 

													</div>
												</div>
											</div>
										<!-- </form> -->
									<?php 
										$this->endWidget();
									?>
										<!-- END FORM-->
									</div>
								</div>
							</div>

							<div class="tab-pane" id="tab_3">
								<div class="portlet box blue">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-reorder"></i>Driver Information Form
										</div>
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
										<?php
											// echo '<pre>';print_r($model);die;
	                                    	$form = $this->beginWidget('CActiveForm', array(
	                                            'id' => 'licenceupload-form',
	                                            'action' => 'uploadLicence',
	                                           
	                                            'htmlOptions' => array(
	                                                'enctype' => 'multipart/form-data',
	                                                'autocomplete' => 'off',
	                                                'role' => 'form',
	                                                'class'=>'form-horizontal form-bordered form-row-stripped',
	                                            ),
	                                            'focus' => array($model, 'driver_licence_front'),
	                                        ));
	                                    ?>
										<!-- <form action="<?php //echo Yii::app()->createAbsoluteUrl('drivers/registerDriver') ?>" class="form-horizontal form-bordered form-row-stripped" method="post" enctype="multipart/form-data"> -->
											<div class="form-body">
												<div class="form-group">
													<label class="control-label col-md-3">Licence Number <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<input type="text" class="form-control" placeholder="Licence Number" name="driver_licence_no">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Licence (front) <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
													<div class="imagePreviewLic"><img id="lic1" src="<?php echo Utils::UserImagePath().'noimg/no-image.png' ?>" alt="" /></div>
														<input type="file" class="form-control" name="driver_licence_front" id="imgLic1"/>
														<span class="help-block">
															 
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Licence (back) <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
													<div class="imagePreviewLic"><img id="lic2" src="<?php echo Utils::UserImagePath().'noimg/no-image.png' ?>" alt="" /></div>
														<input type="file" class="form-control" name="driver_licence_back" id="imgLic2"/>
														<span class="help-block">
															 
														</span>
													</div>
												</div>
												
											</div>
											<div class="form-actions fluid">
												<div class="row">
													<div class="col-md-12">
														<div class="col-md-offset-3 col-md-2">
															<button type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>
														</div>
														<?php 
															if (Yii::app()->user->hasFlash('message3')): 
																$type = Yii::app()->user->getFlash('type3');
														?>
																<?php 
																	if($type == 'success'):
																?>
																	<div class="col-md-6 alert alert-<?php echo $type;?>">
																<?php 
																	else:
																?>
																	<div class="col-md-6 alert alert-<?php echo $type;?>">
																<?php
																	endif;
																?>
																		<span>
																			<?php 
																				echo Yii::app()->user->getFlash('message3'); 
																				Yii::app()->user->setFlash('message3', null);
																			?>
																		</span>
																	</div>
																<?php
																	 endif;
																?> 

														</div>
													</div>
												</div>
											<!-- </form> -->
										<?php 
											$this->endWidget();
										?>
											<!-- END FORM-->
										</div>
									</div>
							</div>

							<div class="tab-pane" id="tab_4">
								<div class="portlet box blue">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-reorder"></i>Driver Information Form
										</div>
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
										<?php
											// echo '<pre>';print_r($model);die;
	                                    	$form = $this->beginWidget('CActiveForm', array(
	                                            'id' => 'vehicledocumentupload-form',
	                                            'action' => 'uploadVehicleDocument',
	                                           
	                                            'htmlOptions' => array(
	                                                'enctype' => 'multipart/form-data',
	                                                'autocomplete' => 'off',
	                                                'role' => 'form',
	                                                'class'=>'form-horizontal form-bordered form-row-stripped',
	                                            ),
	                                            'focus' => array($model, 'driver_licence_front'),
	                                        ));
	                                    ?>
										<!-- <form action="<?php //echo Yii::app()->createAbsoluteUrl('drivers/registerDriver') ?>" class="form-horizontal form-bordered form-row-stripped" method="post" enctype="multipart/form-data"> -->
											<div class="form-body">
												<div class="form-group">
													<label class="control-label col-md-3">Vehicle Registration <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
													<div class="imagePreviewVeh"><img id="reg" src="<?php echo Utils::UserImagePath().'noimg/no-image.png' ?>" alt="" /></div>
														<input type="file" class="form-control" name="driver_vehicle_registration" id="imgReg"/>
														<span class="help-block">
															 
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Vehicle Insurance <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
													<div class="imagePreviewVeh"><img id="ins" src="<?php echo Utils::UserImagePath().'noimg/no-image.png' ?>" alt="" /></div>
														<input type="file" class="form-control" name="driver_vehicle_insurance" id="imgIns"/>
														<span class="help-block">
															 
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Vehicle Maker <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<input type="text" class="form-control" placeholder="Vehicle Maker" name="driver_vehicle_maker">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Vehicle Model <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<input type="text" class="form-control" placeholder="Vehicle Model" name="driver_vehicle_model">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Vehicle Year <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<input type="text" class="form-control" placeholder="Vehicle Year" minlength="4" maxlength="4" name="driver_vehicle_year">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Vehicle Seats <sup class="text-danger">*</sup></label>
													<div class="col-md-9">
														<input type="text" class="form-control" placeholder="Vehicle Seats" maxlength="3" name="dd_vehicle_seat">
													</div>
												</div>
											</div>
											<div class="form-actions fluid">
												<div class="row">
													<div class="col-md-12">
														<div class="col-md-offset-3 col-md-2">
															<button type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>
														</div>
														<?php 
															if (Yii::app()->user->hasFlash('message4')): 
																$type = Yii::app()->user->getFlash('type4');
														?>
																<?php 
																	if($type == 'success'):
																?>
																	<div class="col-md-6 alert alert-<?php echo $type;?>">
																<?php 
																	else:
																?>
																	<div class="col-md-6 alert alert-<?php echo $type;?>">
																<?php
																	endif;
																?>
																		<span>
																			<?php 
																				echo Yii::app()->user->getFlash('message4'); 
																				Yii::app()->user->setFlash('message4', null);
																			?>
																		</span>
																	</div>
																<?php
																	 endif;
																?> 

														</div>
													</div>
												</div>
											<!-- </form> -->
										<?php 
											$this->endWidget();
										?>
											<!-- END FORM-->
										</div>
									</div>
							</div>								
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo Utils::getStyleUrl() ?>plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo Utils::getStyleUrl() ?>plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo Utils::getStyleUrl() ?>plugins/jquery-validation/dist/additional-methods.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo Utils::getStyleUrl() ?>scripts/core/app.js"></script>
<script src="<?php echo Utils::getStyleUrl() ?>scripts/custom/form-samples.js"></script>
<script src="<?php echo Utils::getStyleUrl() ?>scripts/custom/form-validation.js"></script>
<?php 
if(!empty($js_file))
	echo '<script src="'.Utils::getStyleUrl() .'scripts/user/'.$js_file.'.js" type="text/javascript"></script>';
?>
<!-- END PAGE LEVEL SCRIPTS -->

<script>
jQuery(document).ready(function() {    
   // initiate layout and plugins
   App.init();
   FormSamples.init();
   FormValidation.init();
});

function readURL(input, id) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#'+id).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imgInp").change(function(){
    readURL(this, 'pimg');
});
$("#imgLic1").change(function(){
    readURL(this, 'lic1');
});
$("#imgLic2").change(function(){
    readURL(this, 'lic2');
});
$("#imgReg").change(function(){
    readURL(this, 'reg');
});
$("#imgIns").change(function(){
    readURL(this, 'ins');
});


</script>

