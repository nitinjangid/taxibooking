<?php $this->pageTitle = 'Error'; ?>
<link href="<?php echo Utils::getStyleUrl() ?>css/pages/error.css" rel="stylesheet" type="text/css"/>

	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12 page-404">
					<div class="number">
						<?php echo $message['code']; ?>         
					</div>
					<div class="details">
						<h3>Oops! You're lost.</h3>
						<p>
							 <!-- We can not find the page you're looking for.<br/> -->
							 <?php echo $message['message']; ?> <br/>                     
							<a href="<?php echo Yii::app()->createAbsoluteUrl('home');?>">
								 Return home
							</a>
						</p>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
		<!-- BEGIN CONTENT -->
	</div>
	<!-- END CONTAINER -->
<script src="<?php echo Utils::getStyleUrl() ?>scripts/core/app.js"></script>
<script>
	jQuery(document).ready(function() {    
		App.init();
	});
</script>