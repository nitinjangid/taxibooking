	<?php $this->pageTitle = ucwords($title); ?>

	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="<?php echo Utils::getStyleUrl() ?>plugins/select2/select2.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo Utils::getStyleUrl() ?>plugins/select2/select2-metronic.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo Utils::getStyleUrl() ?>plugins/data-tables/DT_bootstrap.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo Utils::getStyleUrl() ?>plugins/bootstrap-datepicker/css/datepicker.css"/>

	<?php 
	if(!empty($css_file))
		echo '<link href="'.Utils::getStyleUrl().'css/user/'.$css_file.'.css" rel="stylesheet" type="text/css"/>';
	?>
	<!-- END PAGE LEVEL STYLES -->

	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">

		<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="promo_details" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Promo Code</h4>
                        </div>
                        <div class="modal-body">
                            <table id="example-table" class="data-table-init-class table table-striped table-bordered table-hover table-green">
                                <tr>
                                    <th>Promo Code</th>
                                    <td id="v_promo_code"></td>
                                </tr>
                                <tr>
                                    <th>Discount <small>(%)</small></th>
                                    <td id="v_promo_discount"></td>
                                </tr>
                                <tr>
                                    <th>Expiry Date</th>
                                    <td id="v_promo_exp"></td>
                                </tr>
                                <tr>
                                    <th>Is Approved </th>
                                    <td id="v_is_approved"></td>
                                </tr>
                                <tr>
                                    <th>Created At</th>
                                    <td id="v_created_at"></td>
                                </tr>
                                <tr>
                                    <th>Updated At </th>
                                    <td id="v_updated_at"></td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?php echo $title;?>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('home') ?>">
								Dashboard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<i class="fa fa-user"></i>
							<a href="javascript:void(0)">
								<?php echo $title;?>
							</a>
						</li>						
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet">
						<div class="portlet-title">
							 <button id="add_new" onclick="div_open('#add_promocode');" class="btn green" style="margin-bottom:5px;">
                                    Add New <i class="fa fa-plus"></i>
                                </button>
						</div>

						<div  id="prefix_1242110685767">	
							
						</div>
						<?php
                        	$form = $this->beginWidget('CActiveForm', array(
                                'id' => 'add-form',
                                'action' => 'promoAdd',
                               
                                'htmlOptions' => array(
                                    'enctype' => 'multipart/form-data',
                                    'autocomplete' => 'off',
                                    'role' => 'form',
                                    'class'=>'add-form',

                                ),

                            ));
                        ?>
						<div class="row" id="add_promocode" style="display: none;" >
                            <div class="col-md-12">
                                <div class="portlet box grey">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-reorder"></i><span id="title">Add Promocode</span> 
                                        </div>
                                    </div>
                                    <div class="portlet-body form">
                                    
                                        <!-- <form role="form" id="add-form" class="add-form" action="" method="post" enctype="multipart/form-data"> -->
                                            <div class="form-body">
                                                <div class="row">
                                                    <input type="hidden" name="promo_id" id="promo_id">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label" for="promo_code">Promo Code</label>
                                                            <input type="text" class="form-control" id="promo_code" name="promo_code" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label" for="promo_dis_per">Discount <small>(%)</small></label>
                                                            <select name="promo_dis_per" id="promo_dis_per" class="form-control">
                                                            	<option value="">select discount</option>
                                                            	<?php for ($i= 0; $i <= 500; $i+=5) { 
                                                            	?>
                                                            	<option value="<?php echo $i;?>"><?php echo $i ;?></option>
                                                            	<?php

                                                            	} ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label" for="promo_exp_date">Expiry Date</label>
                                                            <input type="text" class="form-control date-picker1" data-date-format="yyyy-mm-dd" id="promo_exp_date" name="promo_exp_date" value="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
										
                                        <!-- </form> -->
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn blue">Submit</button>
                                <button type="button" class="btn default" onclick="div_close('#add_promocode');">Cancel</button>
                            </div>
                        </div>
                        <?php 
						$this->endWidget();
						?>



						<div class="portlet-body">
							<div class="table-container">
								<table class="table table-striped table-bordered table-hover categories" id="datatable_ajax">
									<thead>
										<tr role="row" class="heading">
											<th width="5%">
												 S. NO. 
											</th>
											<th width="15%">
												 Promocode
											</th>
											<th width="15%">
												 Discount <small>(%)</small>
											</th>
											<th width="15%">
												 Expiry Date
											</th>
											<th width="15%">
												 Status
											</th>
											<th width="15%">
												 Action
											</th>
										</tr>
										<tr>
											<td>
												
											</td>
											<td>
												<input type="text" class="form-control form-filter input-sm" name="promo_code">
											</td>
											<td>
												<select name="promo_dis_per" class="form-control form-filter input-sm" id="">
													<option value="">select discount</option>
		                                            	<?php for ($i= 0; $i <= 500; $i+=5) { 
		                                            	?>
		                                            	<option value="<?php echo $i;?>"><?php echo $i ;?></option>
		                                            	<?php

		                                            	} ?>
												</select>
											</td>
											<td>
												<input type="text" class="form-control date date-picker1 form-filter input-sm" data-date-format="yyyy-mm-dd" readonly="" name="promo_exp_date" placeholder="From">
											</td>
											<td>
											</td>
											<td>
												<button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
												<button class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Reset</button>
											</td>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
					<!-- End: life time stats -->
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo Utils::getStyleUrl() ?>plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo Utils::getStyleUrl() ?>plugins/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo Utils::getStyleUrl() ?>plugins/data-tables/DT_bootstrap.js"></script>

<!-- END PAGE LEVEL PLUGINS -->

<script src="<?php echo Utils::getStyleUrl() ?>plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?php echo Utils::getStyleUrl() ?>plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo Utils::getStyleUrl() ?>plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo Utils::getStyleUrl() ?>scripts/core/app.js"></script>
<script src="<?php echo Utils::getStyleUrl() ?>scripts/core/datatable.js"></script>
<script src="<?php echo Utils::getStyleUrl() ?>scripts/custom/table-ajax.js"></script>

<?php 
if(!empty($js_file))
	echo '<script src="'.Utils::getStyleUrl() .'scripts/user/'.$js_file.'.js" type="text/javascript"></script>';
?>
<!-- END PAGE LEVEL SCRIPTS -->

<script>
        jQuery(document).ready(function() {    
        	var path = '<?php echo Yii::app()->createAbsoluteUrl('promo/showlist') ?>';
        	App.init();
        	TableAjax.init(path);

        	$('.date-picker1').datepicker({
                rtl: App.isRTL(),
                autoclose: true
            });
        });
    </script>
