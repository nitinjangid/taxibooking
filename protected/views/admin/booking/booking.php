	<?php $this->pageTitle = ucwords($title); ?>

	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="<?php echo Utils::getStyleUrl() ?>plugins/select2/select2.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo Utils::getStyleUrl() ?>plugins/select2/select2-metronic.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo Utils::getStyleUrl() ?>plugins/data-tables/DT_bootstrap.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo Utils::getStyleUrl() ?>plugins/bootstrap-datepicker/css/datepicker.css"/>
	<!-- <link href="<?php echo Utils::getStyleUrl() ?>plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/> -->
	<!-- <link href="<?php echo Utils::getStyleUrl() ?>plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/> -->

	<?php 
	if(!empty($css_file))
		echo '<link href="'.Utils::getStyleUrl().'css/user/'.$css_file.'.css" rel="stylesheet" type="text/css"/>';
	?>
	<!-- END PAGE LEVEL STYLES -->

	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?php echo $title;?>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('home') ?>">
								Dashboard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<i class="fa fa-user"></i>
							<a href="javascript:void(0)">
								<?php echo $title;?>
							</a>
						</li>						
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class=""></i><?php echo $title;?>
							</div>
						</div>
						<div  id="prefix_1242110685767">	
						</div>
						<div class="portlet-body">
							<div class="table-container">
								<table class="table table-striped table-bordered table-hover categories" id="datatable_ajax">
									<thead>
										<tr role="row" class="heading">
											<th width="5%">
												 S. NO. 
											</th>
											<th width="15%">
												 Driver
											</th>
											<th width="15%">
												 Source Address
											</th>
											<th width="15%">
												 Destination Address
											</th>
											<th width="15%">
												 Distance
											</th>
											<th width="15%">
												 Time
											</th>
											<th width="15%">
												 Status
											</th>
											<th width="15%">
												 Action
											</th>
										</tr>
										<tr>
											<td>
												
											</td>
											<td>
												<input type="text" class="form-control form-filter input-sm" name="user">
											</td>
											<td>
												<input type="text" class="form-control form-filter input-sm" name="sAdd">
											</td>
											<td>
												<input type="text" class="form-control form-filter input-sm" name="dAdd">
											</td>
											<td>
												<input type="text" class="form-control form-filter input-sm" name="distance">
											</td>
											<td>
												<input type="text" class="form-control form-filter input-sm" name="time">
											</td>
											<td>
												<select name="status" class="form-control form-filter" id="">
													<option value="">Select ...</option>
													<option value="O">Open</option>
													<option value="R">Reject</option>
												</select>
											</td>
											<td>
												<button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
												<button class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Reset</button>
											</td>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>


								<div class="modal fade" id="booking-info" tabindex="-1" role="dialog" aria-hidden="true">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
												<h4 class="modal-title">Booking Info</h4>
											</div>
											<div id="viewError"></div>
											<div class="modal-body">
												<table class="table table-striped">
													<tr>
														<td><strong>Driver</strong></td>
														<td id="viewDriver"></td>
													</tr>
													<tr>
														<td><strong>Source Address</strong></td>
														<td id="viewSource"></td>
													</tr>
													<tr>
														<td><strong>Destination Address</strong></td>
														<td id="viewDestination"></td>
													</tr>
													<tr>
														<td><strong>Distance</strong></td>
														<td id="viewDistance"></td>
													</tr>
													<tr>
														<td><strong>Time</strong></td>
														<td id="viewTime"></td>
													</tr>
													<tr>
														<td><strong>Status</strong></td>
														<td id="viewStatus"></td>
													</tr>
												</table>
											</div>
											<div class="modal-footer">
												<button data-dismiss="modal" class="btn default" type="button">Close</button>
											</div>
										</div>
										<!-- /.modal-content -->
									</div>
									<!-- /.modal-dialog -->
								</div>


							</div>
						</div>
					</div>
				</div>
					<!-- End: life time stats -->
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo Utils::getStyleUrl() ?>plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo Utils::getStyleUrl() ?>plugins/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo Utils::getStyleUrl() ?>plugins/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" src="<?php echo Utils::getStyleUrl() ?>plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<script src="<?php echo Utils::getStyleUrl() ?>plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?php echo Utils::getStyleUrl() ?>plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo Utils::getStyleUrl() ?>plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo Utils::getStyleUrl() ?>scripts/core/app.js"></script>
<script src="<?php echo Utils::getStyleUrl() ?>scripts/core/datatable.js"></script>
<script src="<?php echo Utils::getStyleUrl() ?>scripts/custom/table-ajax.js"></script>

<?php 
if(!empty($js_file))
	echo '<script src="'.Utils::getStyleUrl() .'scripts/user/'.$js_file.'.js" type="text/javascript"></script>';
?>
<!-- END PAGE LEVEL SCRIPTS -->

<script>
        jQuery(document).ready(function() {    
        	var path = '<?php echo Yii::app()->createAbsoluteUrl('bookings/showlist') ?>';
        	App.init();
        	TableAjax.init(path);
        });
    </script>
