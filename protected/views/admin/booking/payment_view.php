	<?php $this->pageTitle = ucwords($title);?>
	<?php
	// echo '<pre>';print_r($driverData);die;

	?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo Utils::getStyleUrl() ?>plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo Utils::getStyleUrl() ?>plugins/select2/select2-metronic.css"/>
<?php 
	if(!empty($css_file))
		echo '<link href="'.Utils::getStyleUrl().'css/user/'.$css_file.'.css" rel="stylesheet" type="text/css"/>';
?>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Payment Management <small></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('home') ?>">
								Dashboard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<i class="fa fa-user"></i>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('payment') ?>">
								Payment Management
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<i class="fa fa-circle"></i>
							<a href="javascript:;">
								Payment Details
							</a>
						</li>
						
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
							<div class="tab-content">
								<div class="tab-pane active" id="<tab_1></tab_1>">
									<div class="col-md-9">
										<div class="portlet box blue">
											<div class="portlet-title">
												<div class="caption">
													<i class="fa fa-reorder"></i>Payment Information
												</div>
											</div>
											<div class="portlet-body form">
												<!-- <div class="col-md-9"> -->
													<table class="table table-striped">
														<?php if(!empty($payment)): ?>
															<tr>
																<td><strong>Driver</strong></td>
																<td><?php echo ucwords($payment[0]['driver_fname'].' '.$payment[0]['driver_lname']);?></td>
															</tr>
															<tr>
																<td><strong>Email</strong></td>
																<td><?php echo $payment[0]['driver_email'];?></td>
															</tr>
															<tr>
																<td><strong>Phone</strong></td>
																<td><?php echo ucwords($payment[0]['driver_mobile']);?></td>
															</tr>
															<tr>
																<td><strong>Total</strong></td>
																<td><?php echo $payment[0]['txn_total'] != '' ? ucwords($payment[0]['txn_total']) : '<i>NA</i>' ;?></td>
															</tr>
															<tr>
																<td><strong>Method</strong></td>
																<td><?php echo $payment[0]['txn_payment_method'] != '' ? $payment[0]['txn_payment_method'] : '<i>NA</i>';?></td>
															</tr>
															<tr>
																<td><strong>Transaction Time</strong></td>
																<td><?php echo $payment[0]['txn_create_time'] != '' ? $payment[0]['txn_create_time'] : '<i>NA</i>' ;?></td>
															</tr>
															<tr>
																<td><strong>Transaction Expire</strong></td>
																<td><?php echo $payment[0]['txn_expire_month'] != '' ?$payment[0]['txn_expire_month'].','.$payment[0]['txn_expire_year'] : '<i>NA</i>';?></td>
															</tr>
															<tr>
																<td><strong>Status</strong></td>
																<td><?php echo $payment[0]['booking_status'] == "R" ? '<span class="label label-danger allow-field">Rejected</span>' : ($bookings[0]['booking_status'] == "O" ? '<span class="label label-success allow-field">O</span>' : ($bookings[0]['booking_status'] == "C" ? '<span class="label label-warning allow-field">Closed</span>' : '') )
																	?>
																</td>
															</tr>
														<?php else: ?>
															<tr>
																<td colspan="2"> <i> No reocrds </i> </td>
															</tr>
														<?php endif; ?>
													</table>
												<!-- </div> -->
											</div>
										</div>
									</div>
								</div>								
							</div>
						</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo Utils::getStyleUrl() ?>plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo Utils::getStyleUrl() ?>plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo Utils::getStyleUrl() ?>plugins/jquery-validation/dist/additional-methods.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo Utils::getStyleUrl() ?>scripts/core/app.js"></script>
<script src="<?php echo Utils::getStyleUrl() ?>scripts/custom/form-samples.js"></script>
<script src="<?php echo Utils::getStyleUrl() ?>scripts/custom/form-validation.js"></script>
<?php 
if(!empty($js_file))
	echo '<script src="'.Utils::getStyleUrl() .'scripts/user/'.$js_file.'.js" type="text/javascript"></script>';
?>
<!-- END PAGE LEVEL SCRIPTS -->

<script>
jQuery(document).ready(function() {    

   // initiate layout and plugins
   App.init();
   FormSamples.init();
   FormValidation.init();
});
</script>

