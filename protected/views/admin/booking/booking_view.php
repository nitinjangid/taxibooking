	<?php $this->pageTitle = ucwords($title);?>
	<?php
	// echo '<pre>';print_r($driverData);die;

	?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo Utils::getStyleUrl() ?>plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo Utils::getStyleUrl() ?>plugins/select2/select2-metronic.css"/>
<?php 
	if(!empty($css_file))
		echo '<link href="'.Utils::getStyleUrl().'css/user/'.$css_file.'.css" rel="stylesheet" type="text/css"/>';
?>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Booking Management <small></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('home') ?>">
								Dashboard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<i class="fa fa-user"></i>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('bookings') ?>">
								Booking Management
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<i class="fa fa-circle"></i>
							<a href="javascript:;">
								Booking Details
							</a>
						</li>
						
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
							<div class="tab-content">
								<div class="tab-pane active" id="tab_1">
									<div class="col-md-9">
										<div class="portlet box blue">
											<div class="portlet-title">
												<div class="caption">
													<i class="fa fa-reorder"></i>Booking Information
												</div>
											</div>
											<div class="portlet-body form">
												<!-- <div class="col-md-9"> -->
													<table class="table table-striped">
														<?php if(!empty($bookings)): ?>
															<tr>
																<td><strong>Driver</strong></td>
																<td><?php echo ucwords($bookings[0]['driver_fname'].' '.$bookings[0]['driver_lname']);?></td>
															</tr>
															<tr>
																<td><strong>Source Address</strong></td>
																<td><?php echo ucwords($bookings[0]['req_src_adrs']);?></td>
															</tr>
															<tr>
																<td><strong>Destination Address</strong></td>
																<td><?php echo ucwords($bookings[0]['req_des_adrs']);?></td>
															</tr>
															<tr>
																<td><strong>Distance</strong></td>
																<td><?php echo $bookings[0]['req_distance'];?></td>
															</tr>
															<tr>
																<td><strong>Time</strong></td>
																<td><?php echo $bookings[0]['req_time'];?></td>
															</tr>
															<tr>
																<td><strong>Status</strong></td>
																<td><?php echo $bookings[0]['booking_status'] == "R" ? '<span class="label label-danger allow-field">Rejected</span>' : ($bookings[0]['booking_status'] == "O" ? '<span class="label label-success allow-field">O</span>' : ($bookings[0]['booking_status'] == "C" ? '<span class="label label-warning allow-field">Closed</span>' : '') )
																?></td>
															</tr>
														<?php else: ?>
															<tr>
																<td colspan="2"> <i> No reocrds </i> </td>
															</tr>
														<?php endif; ?>
													</table>
												<!-- </div> -->
											</div>
										</div>
									</div>
								</div>								
							</div>
						</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo Utils::getStyleUrl() ?>plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo Utils::getStyleUrl() ?>plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo Utils::getStyleUrl() ?>plugins/jquery-validation/dist/additional-methods.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo Utils::getStyleUrl() ?>scripts/core/app.js"></script>
<script src="<?php echo Utils::getStyleUrl() ?>scripts/custom/form-samples.js"></script>
<script src="<?php echo Utils::getStyleUrl() ?>scripts/custom/form-validation.js"></script>
<?php 
if(!empty($js_file))
	echo '<script src="'.Utils::getStyleUrl() .'scripts/user/'.$js_file.'.js" type="text/javascript"></script>';
?>
<!-- END PAGE LEVEL SCRIPTS -->

<script>
jQuery(document).ready(function() {    

   // initiate layout and plugins
   App.init();
   FormSamples.init();
   FormValidation.init();
});
</script>

