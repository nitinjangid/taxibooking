	<?php $this->pageTitle = ucwords($title); ?>

	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="<?php echo Utils::getStyleUrl() ?>plugins/select2/select2.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo Utils::getStyleUrl() ?>plugins/select2/select2-metronic.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo Utils::getStyleUrl() ?>plugins/data-tables/DT_bootstrap.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo Utils::getStyleUrl() ?>plugins/bootstrap-datepicker/css/datepicker.css"/>
	<!-- <link href="<?php echo Utils::getStyleUrl() ?>plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/> -->
	<!-- <link href="<?php echo Utils::getStyleUrl() ?>plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/> -->
	

	<style>
		.modal.fade.in {
		    margin: auto;
		    top: 0 !important;
		    width: 50%;
		}
	</style>
	<?php 
	if(!empty($css_file))
		echo '<link href="'.Utils::getStyleUrl().'css/user/'.$css_file.'.css" rel="stylesheet" type="text/css"/>';
	?>
	<!-- END PAGE LEVEL STYLES -->



	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?php echo $title;?>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('home') ?>">
								Dashboard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<i class="fa fa-money"></i>
							<a href="javascript:void(0)">
								<?php echo $title;?>
							</a>
						</li>						
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class=""></i><?php echo $title;?>
							</div>
						</div>
						<div  id="prefix_1242110685767">	
						</div>
						<div class="portlet-body">
							<div class="table-container">
								<table class="table table-striped table-bordered table-hover categories" id="datatable_ajax">
									<thead>
										<tr role="row" class="heading">
											<th width="5%">
												 S. NO. 
											</th>
											<th width="15%">
												 Driver
											</th>
											<th width="15%">
												 Email
											</th>
											<th width="15%">
												 Phone
											</th>
											<th width="15%">
												 Total
											</th>
											<th width="15%">
												 Action
											</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>

								<div class="modal fade" id="basic" tabindex="-1" role="dialog" aria-hidden="true">
									<div class="modal-dialog modal-full">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
												<h4 class="modal-title">Pay Driver</h4>
											</div>
											<div class="modal-body">
												 <form id="pay_driver">
													<input type="hidden" name="transfer_driverID" id="driverId">
													<input type="hidden" name="txn_total" id="txn_total">
													  <div class="form-group">
													    <label for="amount">Pay Amount</label>
													    <input type="text" class="form-control" id="amount" placeholder="Amount">
													  	<span class="error text-danger" id="error"></span>
													  </div>
													</form>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn default" data-dismiss="modal">Close</button>
												<button type="submit" class="btn green" onclick="saveTransfer()" >Pay</button>
											</div>
										</div>
										<!-- /.modal-content -->
									</div>
									<!-- /.modal-dialog -->
								</div>

							</div>
						</div>
					</div>
				</div>
					<!-- End: life time stats -->
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo Utils::getStyleUrl() ?>plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo Utils::getStyleUrl() ?>plugins/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo Utils::getStyleUrl() ?>plugins/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" src="<?php echo Utils::getStyleUrl() ?>plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<script src="<?php echo Utils::getStyleUrl() ?>plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?php echo Utils::getStyleUrl() ?>plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo Utils::getStyleUrl() ?>plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo Utils::getStyleUrl() ?>scripts/core/app.js"></script>
<script src="<?php echo Utils::getStyleUrl() ?>scripts/core/datatable.js"></script>
<script src="<?php echo Utils::getStyleUrl() ?>scripts/custom/table-ajax.js"></script>

<?php 
if(!empty($js_file))
	echo '<script src="'.Utils::getStyleUrl() .'scripts/user/'.$js_file.'.js" type="text/javascript"></script>';
?>
<!-- END PAGE LEVEL SCRIPTS -->

<script>
        jQuery(document).ready(function() {    
        	var path = '<?php echo Yii::app()->createAbsoluteUrl('bookings/showPaymentlist') ?>';
        	App.init();
        	TableAjax.init(path);
        });



</script>
