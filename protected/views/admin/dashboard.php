	<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
	<link href="<?php echo Utils::getStyleUrl() ?>plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Utils::getStyleUrl() ?>plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Utils::getStyleUrl() ?>plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Utils::getStyleUrl() ?>plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Utils::getStyleUrl() ?>plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css"/>
	<!-- END PAGE LEVEL PLUGIN STYLES -->

	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Dashboard <small>statistics and more</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="index.html">
								Home
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								Dashboard
							</a>
						</li>
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								<i class="fa fa-angle-down"></i>
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN DASHBOARD STATS -->
				
			<!-- END DASHBOARD STATS -->
		</div>
	</div>
	<!-- END CONTENT -->

			<!-- BEGIN PAGE LEVEL PLUGINS -->
		<script src="<?php echo Utils::getStyleUrl() ?>plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
		<script src="<?php echo Utils::getStyleUrl() ?>plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
		<script src="<?php echo Utils::getStyleUrl() ?>plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
		<script src="<?php echo Utils::getStyleUrl() ?>plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
		<script src="<?php echo Utils::getStyleUrl() ?>plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
		<script src="<?php echo Utils::getStyleUrl() ?>plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
		<script src="<?php echo Utils::getStyleUrl() ?>plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
		<script src="<?php echo Utils::getStyleUrl() ?>plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
		<script src="<?php echo Utils::getStyleUrl() ?>plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
		<script src="<?php echo Utils::getStyleUrl() ?>plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
		<script src="<?php echo Utils::getStyleUrl() ?>plugins/jquery.pulsate.min.js" type="text/javascript"></script>
		<script src="<?php echo Utils::getStyleUrl() ?>plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
		<script src="<?php echo Utils::getStyleUrl() ?>plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
		<script src="<?php echo Utils::getStyleUrl() ?>plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script>
		<!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
		<script src="<?php echo Utils::getStyleUrl() ?>plugins/fullcalendar/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
		<script src="<?php echo Utils::getStyleUrl() ?>plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>
		<script src="<?php echo Utils::getStyleUrl() ?>plugins/jquery.sparkline.min.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL PLUGINS -->
		<!-- BEGIN PAGE LEVEL SCRIPTS -->
		<script src="<?php echo Utils::getStyleUrl() ?>scripts/core/app.js" type="text/javascript"></script>
		<script src="<?php echo Utils::getStyleUrl() ?>scripts/custom/index.js" type="text/javascript"></script>
		<script src="<?php echo Utils::getStyleUrl() ?>scripts/custom/tasks.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL SCRIPTS -->
		<script>
		jQuery(document).ready(function() {    
		   App.init(); // initlayout and core plugins
		   Index.init();
		   Index.initJQVMAP(); // init index page's custom scripts
		   Index.initCalendar(); // init index page's custom scripts
		   Index.initCharts(); // init index page's custom scripts
		   Index.initChat();
		   Index.initMiniCharts();
		   Index.initDashboardDaterange();
		   Index.initIntro();
		   Tasks.initDashboardWidget();
		});
		</script>
		<!-- END JAVASCRIPTS -->