<?php
//print_r($_SERVER);
   if($_SERVER['HTTP_HOST']=='localhost') {
      $base_url = 'http://localhost' . Yii::app()->request->baseUrl;
   } else {
      $base_url = 'http://'.$_SERVER['HTTP_HOST'].Yii::app()->request->baseUrl;
   }
   ?>
<!DOCTYPE html>
<html>
   <head>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
   </head>
   <body>
      <div class="container">
         <h2>WebServices For U-Ride App</h2>
         <div class="panel-group" id="accordion">
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><strong class="col-md-3">Registration</strong>  <?php echo $base_url; ?>/webservice/register</a>
                  </h4>
               </div>
               <div id="collapse1" class="panel-collapse collapse">
                  <div class="panel-body">
                     <form class="form-horizontal" role="form" method="post" action="<?php echo $base_url; ?>/webservice/register" enctype="multipart/form-data">
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="email">first_name:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="first_name"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="pwd">last_name:</label>
                           <div class="col-sm-10"> 
                              <input type="text" class="form-control" name="last_name" >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="pwd">email:</label>
                           <div class="col-sm-10"> 
                              <input type="text" class="form-control" name="email"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="mobile">mobile:</label>
                           <div class="col-sm-10"> 
                              <input type="text" class="form-control" name="mobile"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="pwd">password:</label>
                           <div class="col-sm-10"> 
                              <input type="text" class="form-control" name="password" >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="pwd">udid:</label>
                           <div class="col-sm-10"> 
                              <input type="text" class="form-control" name="udid"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="platform">platform:</label>
                           <div class="col-sm-10"> 
                              <input type="text" class="form-control" name="platform"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="avatar">avatar:</label>
                           <div class="col-sm-10">
                              <input type="file" class="form-control" name="avatar"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-sm-offset-2 col-sm-10">
                              <button type="submit" class="btn btn-default">Submit</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"><strong class="col-md-3">Login</strong><?php echo $base_url; ?>/webservice/login</a>
                  </h4>
               </div>
               <div id="collapse2" class="panel-collapse collapse">
                  <div class="panel-body">
                     <form class="form-horizontal" role="form" method="post" action="<?php echo $base_url; ?>/webservice/login" enctype="multipart/form-data">
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="email">email:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="email"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="pwd">password:</label>
                           <div class="col-sm-10"> 
                              <input type="text" class="form-control" name="password" >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="pwd">udid:</label>
                           <div class="col-sm-10"> 
                              <input type="text" class="form-control" name="udid" >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="platform">platform:</label>
                           <div class="col-sm-10"> 
                              <input type="text" class="form-control" name="platform" >
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-sm-offset-2 col-sm-10">
                              <button type="submit" class="btn btn-default">Submit</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse3"><strong class="col-md-3">Forgot Password</strong><?php echo $base_url; ?>/webservice/forgotpassword</a>
                  </h4>
               </div>
               <div id="collapse3" class="panel-collapse collapse">
                  <div class="panel-body">
                     <form class="form-horizontal" role="form" method="post" action="<?php echo $base_url; ?>/webservice/forgotpassword" enctype="multipart/form-data">
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="email">email:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="email"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-sm-offset-2 col-sm-10">
                              <button type="submit" class="btn btn-default">Submit</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse4"><strong class="col-md-3">Show Profile</strong><?php echo $base_url; ?>/webservice/showprofile</a>
                  </h4>
               </div>
               <div id="collapse4" class="panel-collapse collapse">
                  <div class="panel-body">
                     <form class="form-horizontal" role="form" method="post" action="<?php echo $base_url; ?>/webservice/showprofile" enctype="multipart/form-data">
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="user_id">user_id:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="user_id"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-sm-offset-2 col-sm-10">
                              <button type="submit" class="btn btn-default">Submit</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse5"><strong class="col-md-3">Edit Profile</strong><?php echo $base_url; ?>/webservice/editprofile</a>
                  </h4>
               </div>
               <div id="collapse5" class="panel-collapse collapse">
                  <div class="panel-body">
                     <form class="form-horizontal" role="form" method="post" action="<?php echo $base_url; ?>/webservice/editprofile" enctype="multipart/form-data">
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="user_id">user_id:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="user_id"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="first_name">first_name:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="first_name"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="last_name">last_name:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="last_name"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="email">email:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="email"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="mobile">mobile:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="mobile"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="avatar">avatar:</label>
                           <div class="col-sm-10">
                              <input type="file" class="form-control" name="avatar"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="user_id">platform:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="platform"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-sm-offset-2 col-sm-10">
                              <button type="submit" class="btn btn-default">Submit</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse6"><strong class="col-md-3">Reset Password</strong><?php echo $base_url; ?>/webservice/resetpassword</a>
                  </h4>
               </div>
               <div id="collapse6" class="panel-collapse collapse">
                  <div class="panel-body">
                     <form class="form-horizontal" role="form" method="post" action="<?php echo $base_url; ?>/webservice/resetpassword" enctype="multipart/form-data">
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="user_id">user_id:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="user_id"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="old_password">old_password:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="old_password"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="new_password">new_password:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="new_password"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-sm-offset-2 col-sm-10">
                              <button type="submit" class="btn btn-default">Submit</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse7"><strong class="col-md-3">FB Check</strong><?php echo $base_url; ?>/webservice/fbcheck</a>
                  </h4>
               </div>
               <div id="collapse7" class="panel-collapse collapse">
                  <div class="panel-body">
                     <form class="form-horizontal" role="form" method="post" action="<?php echo $base_url; ?>/webservice/fbcheck" enctype="multipart/form-data">
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="fb_id">fb_id:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="fb_id"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="user_id">platform:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="platform"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="udid">udid:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="udid"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-sm-offset-2 col-sm-10">
                              <button type="submit" class="btn btn-default">Submit</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse8"><strong class="col-md-3">FB Connect</strong><?php echo $base_url; ?>/webservice/fbconnect</a>
                  </h4>
               </div>
               <div id="collapse8" class="panel-collapse collapse">
                  <div class="panel-body">
                     <form class="form-horizontal" role="form" method="post" action="<?php echo $base_url; ?>/webservice/fbconnect" enctype="multipart/form-data">
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="fb_id">fb_id:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="fb_id"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="email">first_name:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="first_name"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="pwd">last_name:</label>
                           <div class="col-sm-10"> 
                              <input type="text" class="form-control" name="last_name" >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="pwd">email:</label>
                           <div class="col-sm-10"> 
                              <input type="text" class="form-control" name="email"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="mobile">mobile:</label>
                           <div class="col-sm-10"> 
                              <input type="text" class="form-control" name="mobile"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="pwd">password:</label>
                           <div class="col-sm-10"> 
                              <input type="text" class="form-control" name="password" >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="pwd">udid:</label>
                           <div class="col-sm-10"> 
                              <input type="text" class="form-control" name="udid"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="platform">platform:</label>
                           <div class="col-sm-10"> 
                              <input type="text" class="form-control" name="platform"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="avatar">avatar:</label>
                           <div class="col-sm-10">
                              <input type="file" class="form-control" name="avatar"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-sm-offset-2 col-sm-10">
                              <button type="submit" class="btn btn-default">Submit</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse9"><strong class="col-md-3">Generate Secret</strong><?php echo $base_url; ?>/webservice/generatesecret</a>
                  </h4>
               </div>
               <div id="collapse9" class="panel-collapse collapse">
                  <div class="panel-body">
                     <form class="form-horizontal" role="form" method="post" action="<?php echo $base_url; ?>/webservice/generatesecret" enctype="multipart/form-data">
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="email">email:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="email"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="platform">platform:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="platform"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-sm-offset-2 col-sm-10">
                              <button type="submit" class="btn btn-default">Submit</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse10"><strong class="col-md-3">Validate Secret</strong><?php echo $base_url; ?>/webservice/validatesecret</a>
                  </h4>
               </div>
               <div id="collapse10" class="panel-collapse collapse">
                  <div class="panel-body">
                     <form class="form-horizontal" role="form" method="post" action="<?php echo $base_url; ?>/webservice/validatesecret" enctype="multipart/form-data">
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="secret">secret:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="secret"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="secret">mobile_no:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="mobile_no"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-sm-offset-2 col-sm-10">
                              <button type="submit" class="btn btn-default">Submit</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse11"><strong class="col-md-3">Fare Calculation</strong><?php echo $base_url; ?>/webservice/farecalculation</a>
                  </h4>
               </div>
               <div id="collapse11" class="panel-collapse collapse">
                  <div class="panel-body">
                     <form class="form-horizontal" role="form" method="post" action="<?php echo $base_url; ?>/webservice/farecalculation" enctype="multipart/form-data">
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="time">time:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="time"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="distance">distance:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="distance"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="platform">platform:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="platform"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-sm-offset-2 col-sm-10">
                              <button type="submit" class="btn btn-default">Submit</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse12"><strong class="col-md-3">Get Nearest Driver</strong><?php echo $base_url; ?>/webservice/getnearestdriver</a>
                  </h4>
               </div>
               <div id="collapse12" class="panel-collapse collapse">
                  <div class="panel-body">
                     <form class="form-horizontal" role="form" method="post" action="<?php echo $base_url; ?>/webservice/getnearestdriver" enctype="multipart/form-data">
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="latitude">latitude:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="latitude"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="longitude">longitude:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="longitude"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="platform">platform:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="platform"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-sm-offset-2 col-sm-10">
                              <button type="submit" class="btn btn-default">Submit</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse13"><strong class="col-md-3">Driver Login</strong><?php echo $base_url; ?>/webservice/driverlogin</a>
                  </h4>
               </div>
               <div id="collapse13" class="panel-collapse collapse">
                  <div class="panel-body">
                     <form class="form-horizontal" role="form" method="post" action="<?php echo $base_url; ?>/webservice/driverlogin" enctype="multipart/form-data">
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="email">email:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="email"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="pwd">password:</label>
                           <div class="col-sm-10"> 
                              <input type="text" class="form-control" name="password" >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="pwd">udid:</label>
                           <div class="col-sm-10"> 
                              <input type="text" class="form-control" name="udid" >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="platform">platform:</label>
                           <div class="col-sm-10"> 
                              <input type="text" class="form-control" name="platform" >
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-sm-offset-2 col-sm-10">
                              <button type="submit" class="btn btn-default">Submit</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse14"><strong class="col-md-3">Driver Forgot Password</strong><?php echo $base_url; ?>/webservice/driverforgotpassword</a>
                  </h4>
               </div>
               <div id="collapse14" class="panel-collapse collapse">
                  <div class="panel-body">
                     <form class="form-horizontal" role="form" method="post" action="<?php echo $base_url; ?>/webservice/driverforgotpassword" enctype="multipart/form-data">
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="email">email:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="email"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-sm-offset-2 col-sm-10">
                              <button type="submit" class="btn btn-default">Submit</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse15"><strong class="col-md-3">Show Driver Profile</strong><?php echo $base_url; ?>/webservice/drivershowprofile</a>
                  </h4>
               </div>
               <div id="collapse15" class="panel-collapse collapse">
                  <div class="panel-body">
                     <form class="form-horizontal" role="form" method="post" action="<?php echo $base_url; ?>/webservice/drivershowprofile" enctype="multipart/form-data">
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="driver_id">driver_id:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="driver_id"  >
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-sm-offset-2 col-sm-10">
                              <button type="submit" class="btn btn-default">Submit</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         
               <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse16"><strong class="col-md-3">Add Card information</strong><?php echo $base_url; ?>/webservice/addcard</a>
                  </h4>
               </div>
               <div id="collapse16" class="panel-collapse collapse">
                  <div class="panel-body">
                     <form class="form-horizontal" role="form" method="post" action="<?php echo $base_url; ?>/webservice/addcard" enctype="multipart/form-data">
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="data">data:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="data" placeholder='{
  "create_time" : "2016-09-23T10:39:09Z", 
  "expire_month" : "3",
  "expire_year" : "2017", 
  "first_name" : "text Card", 
  "id" : "CARD-7HB0913066314063MK7SQMTI", 
  "number" : "xxxxxxxxxxxx1881", 
  "state" : "ok", 
  "type" : "visa", 
  "update_time" : "2016-09-23T10:39:09Z", 
  "valid_until" : "2019-09-23T00:00:00Z" 
  
}'  >
                           </div>
                        
                        
                        
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="user_id">user_id:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="user_id"   >
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-sm-offset-2 col-sm-10">
                              <button type="submit" class="btn btn-default">Submit</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         
         
         
               <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse17"><strong class="col-md-3">Get Card information</strong><?php echo $base_url; ?>/webservice/getcard</a>
                  </h4>
               </div>
               <div id="collapse17" class="panel-collapse collapse">
                  <div class="panel-body">
                     <form class="form-horizontal" role="form" method="post" action="<?php echo $base_url; ?>/webservice/getcard" enctype="multipart/form-data">
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="user_id">user_id:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="user_id"   >
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-sm-offset-2 col-sm-10">
                              <button type="submit" class="btn btn-default">Submit</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         
         
         
         
               
               
               <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse18"><strong class="col-md-3">Delete Card</strong><?php echo $base_url; ?>/webservice/deletecard</a>
                  </h4>
               </div>
               <div id="collapse18" class="panel-collapse collapse">
                  <div class="panel-body">
                     <form class="form-horizontal" role="form" method="post" action="<?php echo $base_url; ?>/webservice/deletecard" enctype="multipart/form-data">
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="user_id">user_id:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="user_id"   >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="card_id">card_id:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="card_id"   >
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-sm-offset-2 col-sm-10">
                              <button type="submit" class="btn btn-default">Submit</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         
         
         
         <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse19"><strong class="col-md-3">Set Driver Mode</strong><?php echo $base_url; ?>/webservice/setdrivermode</a>
                  </h4>
               </div>
               <div id="collapse19" class="panel-collapse collapse">
                  <div class="panel-body">
                     <form class="form-horizontal" role="form" method="post" action="<?php echo $base_url; ?>/webservice/setdrivermode" enctype="multipart/form-data">
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="driver_id">driver_id:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="driver_id"   >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="mode">mode:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="mode" placeholder="1 = online or 0 = offline"   >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="platform">platform:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="platform"    >
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-sm-offset-2 col-sm-10">
                              <button type="submit" class="btn btn-default">Submit</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         
         
         
         
         <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse20"><strong class="col-md-3">Get Booking Detail</strong><?php echo $base_url; ?>/webservice/getbookingdetail</a>
                  </h4>
               </div>
               <div id="collapse20" class="panel-collapse collapse">
                  <div class="panel-body">
                     <form class="form-horizontal" role="form" method="post" action="<?php echo $base_url; ?>/webservice/getbookingdetail" enctype="multipart/form-data">
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="booking_id">booking_id:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="booking_id"   >
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-sm-offset-2 col-sm-10">
                              <button type="submit" class="btn btn-default">Submit</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         
         
         
         
               
         
        
        
         <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse21"><strong class="col-md-3">Get Booking History</strong><?php echo $base_url; ?>/webservice/getbookinghistory</a>
                  </h4>
               </div>
               <div id="collapse21" class="panel-collapse collapse">
                  <div class="panel-body">
                     <form class="form-horizontal" role="form" method="post" action="<?php echo $base_url; ?>/webservice/getbookinghistory" enctype="multipart/form-data">
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="type">type:</label>
                           <div class="col-sm-10">
                              <select name="type" class="form-control">
                                 <option value="user">user</option>
                                 <option value="driver">driver</option>
                              </select>
                           </div>
                        </div>
                        
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="id">id:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="id"   >
                           </div>
                        </div>
                        
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="current_page">current_page:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="current_page"   >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="limit">limit:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="limit"   >
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-sm-offset-2 col-sm-10">
                              <button type="submit" class="btn btn-default">Submit</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         
         
         
         <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse22"><strong class="col-md-3">Add Booking Image</strong><?php echo $base_url; ?>/webservice/addbookingimage</a>
                  </h4>
               </div>
               <div id="collapse22" class="panel-collapse collapse">
                  <div class="panel-body">
                     <form class="form-horizontal" role="form" method="post" action="<?php echo $base_url; ?>/webservice/addbookingimage" enctype="multipart/form-data">
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="booking_id">booking_id:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="booking_id"   >
                           </div>
                        </div>
                        
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="avatar">avatar:</label>
                           <div class="col-sm-10">
                              <input type="file" class="form-control" name="avatar"   >
                           </div>
                        </div>
                        
                        <div class="form-group">
                           <div class="col-sm-offset-2 col-sm-10">
                              <button type="submit" class="btn btn-default">Submit</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         
         
         
         
               
         
        
        
         
               
         
        
        
        
        
        
        
         
         <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse23"><strong class="col-md-3">Add Rating</strong><?php echo $base_url; ?>/webservice/addrating</a>
                  </h4>
               </div>
               <div id="collapse23" class="panel-collapse collapse">
                  <div class="panel-body">
                     <form class="form-horizontal" role="form" method="post" action="<?php echo $base_url; ?>/webservice/addrating" enctype="multipart/form-data">
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="booking_id">booking_id:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="booking_id"   >
                           </div>
                        </div>
                        
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="rate">rate:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="rate"   >
                           </div>
                        </div>
                        
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="comment">comment:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="comment"   >
                           </div>
                        </div>
                        
                        
                        <div class="form-group">
                           <div class="col-sm-offset-2 col-sm-10">
                              <button type="submit" class="btn btn-default">Submit</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         
         
         
         
               
         <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse24"><strong class="col-md-3">Get account history</strong><?php echo $base_url; ?>/webservice/getaccounthistory</a>
                  </h4>
               </div>
               <div id="collapse24" class="panel-collapse collapse">
                  <div class="panel-body">
                     <form class="form-horizontal" role="form" method="post" action="<?php echo $base_url; ?>/webservice/getaccounthistory" enctype="multipart/form-data">
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="driver_id">driver_id:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="driver_id"   >
                           </div>
                        </div>
                        
                       
                        <div class="form-group">
                           <div class="col-sm-offset-2 col-sm-10">
                              <button type="submit" class="btn btn-default">Submit</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         
         
         
         
               
         
        
        
         
               
          <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse25"><strong class="col-md-3">User Log out</strong><?php echo $base_url; ?>/webservice/userlogout</a>
                  </h4>
               </div>
               <div id="collapse25" class="panel-collapse collapse">
                  <div class="panel-body">
                     <form class="form-horizontal" role="form" method="post" action="<?php echo $base_url; ?>/webservice/userlogout" enctype="multipart/form-data">
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="user_id">user_id:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="user_id"   >
                           </div>
                        </div>
                        
                       
                        <div class="form-group">
                           <div class="col-sm-offset-2 col-sm-10">
                              <button type="submit" class="btn btn-default">Submit</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
        
        
        
        
        
        
         
        
        
         
               
         
        
        
        
        
        
         
         
          <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse26"><strong class="col-md-3">Driver Log out</strong><?php echo $base_url; ?>/webservice/driverlogout</a>
                  </h4>
               </div>
               <div id="collapse26" class="panel-collapse collapse">
                  <div class="panel-body">
                     <form class="form-horizontal" role="form" method="post" action="<?php echo $base_url; ?>/webservice/driverlogout" enctype="multipart/form-data">
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="driver_id">driver_id:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="driver_id"   >
                           </div>
                        </div>
                        
                       
                        <div class="form-group">
                           <div class="col-sm-offset-2 col-sm-10">
                              <button type="submit" class="btn btn-default">Submit</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
        
        
        
        
        
        
         
        
        
         
               
         
        
        
        
        
        
         
         
         
         
          <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse27"><strong class="col-md-3">Get Latest Booking</strong><?php echo $base_url; ?>/webservice/getlatestbooking</a>
                  </h4>
               </div>
               <div id="collapse27" class="panel-collapse collapse">
                  <div class="panel-body">
                     <form class="form-horizontal" role="form" method="post" action="<?php echo $base_url; ?>/webservice/getlatestbooking" enctype="multipart/form-data">
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="type">type:</label>
                           <div class="col-sm-10">
                              <select name="type" class="form-control">
                                 <option value="user">user</option>
                                 <option value="driver">driver</option>
                              </select>
                              <!--<input type="text" class="form-control" name="type"   >-->
                           </div>
                        </div>
                        
                        <div class="form-group">
                           <label class="control-label col-sm-2" for="id">id:</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" name="id"   >
                           </div>
                        </div>
                       
                        <div class="form-group">
                           <div class="col-sm-offset-2 col-sm-10">
                              <button type="submit" class="btn btn-default">Submit</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
        
        
        
        
        
        
         
        
        
         
               
         
        
        
        
        
        
         
         
         
         
         
         </div>
      </div>
      <script>
         $(function () {
            $('.panel-collapse').on('shown.bs.collapse', function (e) {
            var $panel = $(this).closest('.panel');
            $('html,body').animate({
                scrollTop: $panel.offset().top
            }, 500); 
         }); 
         });
              
      </script>
   </body>
</html>