<?php

/**
 * This is the model class for table "{{driver_detail}}".
 *
 * The followings are the available columns in table '{{driver_detail}}':
 * @property integer $dd_id
 * @property integer $dd_driverID
 * @property string $dd_street
 * @property string $dd_city
 * @property string $dd_state
 * @property string $dd_country
 * @property string $dd_pin
 * @property string $dd_licence_no
 * @property string $dd_licence_front
 * @property string $dd_licence_back
 * @property string $dd_vehicle_no
 * @property string $dd_vehicle_year
 * @property string $dd_vehicle_make
 * @property string $dd_vehicle_model
 * @property string $dd_vehicle_registration
 * @property string $dd_vehicle_insurance
 * @property string $dd_created
 * @property string $dd_updated
 */
class DriverDetail extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{driver_detail}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dd_driverID, dd_street, dd_city, dd_state, dd_country, dd_pin, dd_licence_no, dd_licence_front, dd_licence_back, dd_vehicle_no, dd_vehicle_year, dd_vehicle_make, dd_vehicle_model, dd_vehicle_registration, dd_vehicle_insurance, dd_created, dd_updated', 'required'),
			array('dd_driverID', 'numerical', 'integerOnly'=>true),
			array('dd_street', 'length', 'max'=>150),
			array('dd_city, dd_state, dd_country', 'length', 'max'=>15),
			array('dd_pin', 'length', 'max'=>10),
			array('dd_licence_no, dd_licence_front, dd_licence_back, dd_vehicle_no, dd_vehicle_year, dd_vehicle_make, dd_vehicle_model, dd_vehicle_registration, dd_vehicle_insurance', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('dd_id, dd_driverID, dd_street, dd_city, dd_state, dd_country, dd_pin, dd_licence_no, dd_licence_front, dd_licence_back, dd_vehicle_no, dd_vehicle_year, dd_vehicle_make, dd_vehicle_model, dd_vehicle_registration, dd_vehicle_insurance, dd_created, dd_updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'dd_id' => 'Dd',
			'dd_driverID' => 'Dd Driver',
			'dd_street' => 'Dd Street',
			'dd_city' => 'Dd City',
			'dd_state' => 'Dd State',
			'dd_country' => 'Dd Country',
			'dd_pin' => 'Dd Pin',
			'dd_licence_no' => 'Dd Licence No',
			'dd_licence_front' => 'Dd Licence Front',
			'dd_licence_back' => 'Dd Licence Back',
			'dd_vehicle_no' => 'Dd Vehicle No',
			'dd_vehicle_year' => 'Dd Vehicle Year',
			'dd_vehicle_make' => 'Dd Vehicle Make',
			'dd_vehicle_model' => 'Dd Vehicle Model',
			'dd_vehicle_registration' => 'Dd Vehicle Registration',
			'dd_vehicle_insurance' => 'Dd Vehicle Insurance',
			'dd_created' => 'Dd Created',
			'dd_updated' => 'Dd Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('dd_id',$this->dd_id);
		$criteria->compare('dd_driverID',$this->dd_driverID);
		$criteria->compare('dd_street',$this->dd_street,true);
		$criteria->compare('dd_city',$this->dd_city,true);
		$criteria->compare('dd_state',$this->dd_state,true);
		$criteria->compare('dd_country',$this->dd_country,true);
		$criteria->compare('dd_pin',$this->dd_pin,true);
		$criteria->compare('dd_licence_no',$this->dd_licence_no,true);
		$criteria->compare('dd_licence_front',$this->dd_licence_front,true);
		$criteria->compare('dd_licence_back',$this->dd_licence_back,true);
		$criteria->compare('dd_vehicle_no',$this->dd_vehicle_no,true);
		$criteria->compare('dd_vehicle_year',$this->dd_vehicle_year,true);
		$criteria->compare('dd_vehicle_make',$this->dd_vehicle_make,true);
		$criteria->compare('dd_vehicle_model',$this->dd_vehicle_model,true);
		$criteria->compare('dd_vehicle_registration',$this->dd_vehicle_registration,true);
		$criteria->compare('dd_vehicle_insurance',$this->dd_vehicle_insurance,true);
		$criteria->compare('dd_created',$this->dd_created,true);
		$criteria->compare('dd_updated',$this->dd_updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DriverDetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
