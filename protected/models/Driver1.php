<?php

/**
 * This is the model class for table "{{driver}}".
 *
 * The followings are the available columns in table '{{driver}}':
 * @property integer $driver_id
 * @property string $driver_fname
 * @property string $driver_lname
 * @property string $driver_gender
 * @property string $driver_email
 * @property string $driver_mobile
 * @property string $driver_password
 * @property integer $driver_mode
 * @property string $driver_avatar
 * @property string $driver_lat
 * @property string $driver_long
 * @property string $driver_platform
 * @property string $driver_udid
 * @property string $driver_created
 * @property string $driver_updated
 * @property integer $driver_status
 */
class Driver extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{driver}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('driver_lname, driver_gender, driver_mobile, driver_password, driver_avatar, driver_lat, driver_long, driver_platform, driver_udid, driver_created, driver_updated', 'required'),
			array('driver_mode, driver_status', 'numerical', 'integerOnly'=>true),
			array('driver_fname, driver_lname, driver_avatar', 'length', 'max'=>30),
			array('driver_gender', 'length', 'max'=>1),
			array('driver_email', 'length', 'max'=>50),
			array('driver_mobile, driver_lat, driver_long', 'length', 'max'=>10),
			array('driver_password', 'length', 'max'=>100),
			array('driver_platform', 'length', 'max'=>150),
			array('driver_udid', 'length', 'max'=>200),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('driver_id, driver_fname, driver_lname, driver_gender, driver_email, driver_mobile, driver_password, driver_mode, driver_avatar, driver_lat, driver_long, driver_platform, driver_udid, driver_created, driver_updated, driver_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'driver_id' => 'Driver',
			'driver_fname' => 'Driver Fname',
			'driver_lname' => 'Driver Lname',
			'driver_gender' => 'Driver Gender',
			'driver_email' => 'Driver Email',
			'driver_mobile' => 'Driver Mobile',
			'driver_password' => 'Driver Password',
			'driver_mode' => 'Driver Mode',
			'driver_avatar' => 'Driver Avatar',
			'driver_lat' => 'Driver Lat',
			'driver_long' => 'Driver Long',
			'driver_platform' => 'Driver Platform',
			'driver_udid' => 'Driver Udid',
			'driver_created' => 'Driver Created',
			'driver_updated' => 'Driver Updated',
			'driver_status' => 'Driver Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('driver_id',$this->driver_id);
		$criteria->compare('driver_fname',$this->driver_fname,true);
		$criteria->compare('driver_lname',$this->driver_lname,true);
		$criteria->compare('driver_gender',$this->driver_gender,true);
		$criteria->compare('driver_email',$this->driver_email,true);
		$criteria->compare('driver_mobile',$this->driver_mobile,true);
		$criteria->compare('driver_password',$this->driver_password,true);
		$criteria->compare('driver_mode',$this->driver_mode);
		$criteria->compare('driver_avatar',$this->driver_avatar,true);
		$criteria->compare('driver_lat',$this->driver_lat,true);
		$criteria->compare('driver_long',$this->driver_long,true);
		$criteria->compare('driver_platform',$this->driver_platform,true);
		$criteria->compare('driver_udid',$this->driver_udid,true);
		$criteria->compare('driver_created',$this->driver_created,true);
		$criteria->compare('driver_updated',$this->driver_updated,true);
		$criteria->compare('driver_status',$this->driver_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Driver the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
