<?php

/**
 * This is the model class for table "{{fare}}".
 *
 * The followings are the available columns in table '{{fare}}':
 * @property integer $fare_id
 * @property string $fare_base
 * @property string $fare_time
 * @property string $fare_distance
 * @property string $fare_created
 * @property string $fare_updated
 * @property integer $fare_status
 */
class Fare extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{fare}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fare_base, fare_time, fare_distance, fare_created, fare_updated', 'required'),
			array('fare_status', 'numerical', 'integerOnly'=>true),
			array('fare_base, fare_time, fare_distance', 'length', 'max'=>10),
			array('fare_commision', 'numerical', 'allowEmpty' => false, 'integerOnly' => false, 'min' => 0, 'max' => 100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('fare_id, fare_base, fare_time, fare_distance, fare_created, fare_updated, fare_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'fare_id' => 'Fare',
			'fare_base' => 'Fare Base',
			'fare_time' => 'Fare Time',
			'fare_distance' => 'Fare Distance',
			'fare_created' => 'Fare Created',
			'fare_updated' => 'Fare Updated',
			'fare_status' => 'Fare Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('fare_id',$this->fare_id);
		$criteria->compare('fare_base',$this->fare_base,true);
		$criteria->compare('fare_time',$this->fare_time,true);
		$criteria->compare('fare_distance',$this->fare_distance,true);
		$criteria->compare('fare_created',$this->fare_created,true);
		$criteria->compare('fare_updated',$this->fare_updated,true);
		$criteria->compare('fare_status',$this->fare_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Fare the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
