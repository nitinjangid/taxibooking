<?php

/**
 * This is the model class for table "{{card}}".
 *
 * The followings are the available columns in table '{{card}}':
 * @property integer $card_id
 * @property string $card_token
 * @property integer $card_userID
 * @property string $car_create_time
 * @property string $car_expire_month
 * @property string $car_expire_year
 * @property string $card_first_name
 * @property string $card_number
 * @property string $card_state
 * @property string $card_type
 * @property string $card_update_time
 * @property string $card_valid_until
 * @property string $card_created
 * @property integer $card_status
 */
class Card extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{card}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('card_token, card_userID, car_create_time, car_expire_month, car_expire_year, card_first_name, card_number, card_state, card_type, card_update_time, card_valid_until', 'required'),
			array('card_userID, card_status', 'numerical', 'integerOnly'=>true),
			array('card_token', 'length', 'max'=>200),
			array('car_create_time, card_update_time, card_valid_until', 'length', 'max'=>25),
			array('car_expire_month', 'length', 'max'=>3),
			array('car_expire_year', 'length', 'max'=>5),
			array('card_first_name', 'length', 'max'=>30),
			array('card_number', 'length', 'max'=>20),
			array('card_state, card_type', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('card_id, card_token, card_userID, car_create_time, car_expire_month, car_expire_year, card_first_name, card_number, card_state, card_type, card_update_time, card_valid_until, card_created, card_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'card_id' => 'Card',
			'card_token' => 'Card Token',
			'card_userID' => 'Card User',
			'car_create_time' => 'Car Create Time',
			'car_expire_month' => 'Car Expire Month',
			'car_expire_year' => 'Car Expire Year',
			'card_first_name' => 'Card First Name',
			'card_number' => 'Card Number',
			'card_state' => 'Card State',
			'card_type' => 'Card Type',
			'card_update_time' => 'Card Update Time',
			'card_valid_until' => 'Card Valid Until',
			'card_created' => 'Card Created',
			'card_status' => 'Card Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('card_id',$this->card_id);
		$criteria->compare('card_token',$this->card_token,true);
		$criteria->compare('card_userID',$this->card_userID);
		$criteria->compare('car_create_time',$this->car_create_time,true);
		$criteria->compare('car_expire_month',$this->car_expire_month,true);
		$criteria->compare('car_expire_year',$this->car_expire_year,true);
		$criteria->compare('card_first_name',$this->card_first_name,true);
		$criteria->compare('card_number',$this->card_number,true);
		$criteria->compare('card_state',$this->card_state,true);
		$criteria->compare('card_type',$this->card_type,true);
		$criteria->compare('card_update_time',$this->card_update_time,true);
		$criteria->compare('card_valid_until',$this->card_valid_until,true);
		$criteria->compare('card_created',$this->card_created,true);
		$criteria->compare('card_status',$this->card_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Card the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
