<?php

/**
 * This is the model class for table "{{rating}}".
 *
 * The followings are the available columns in table '{{rating}}':
 * @property integer $rating_id
 * @property integer $rating_bookingID
 * @property string $rating_rate
 * @property string $rating_comment
 * @property string $rating_created
 * @property integer $rating_status
 */
class Rating extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{rating}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('rating_bookingID, rating_rate, rating_comment', 'required'),
			array('rating_bookingID, rating_status', 'numerical', 'integerOnly'=>true),
			array('rating_rate', 'length', 'max'=>5),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('rating_id, rating_bookingID, rating_rate, rating_comment, rating_created, rating_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'rating_id' => 'Rating',
			'rating_bookingID' => 'Rating Booking',
			'rating_rate' => 'Rating Rate',
			'rating_comment' => 'Rating Comment',
			'rating_created' => 'Rating Created',
			'rating_status' => 'Rating Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('rating_id',$this->rating_id);
		$criteria->compare('rating_bookingID',$this->rating_bookingID);
		$criteria->compare('rating_rate',$this->rating_rate,true);
		$criteria->compare('rating_comment',$this->rating_comment,true);
		$criteria->compare('rating_created',$this->rating_created,true);
		$criteria->compare('rating_status',$this->rating_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Rating the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
