<?php

/**
 * This is the model class for table "{{user}}".
 *
 * The followings are the available columns in table '{{user}}':
 * @property integer $user_id
 * @property string $user_fname
 * @property string $user_lname
 * @property string $user_email
 * @property string $user_avatar
 * @property string $user_mobile
 * @property string $user_pwd
 * @property string $user_udid
 * @property string $user_platform
 * @property string $user_created
 * @property string $user_updated
 * @property integer $user_status
 */
class User extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{user}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_fname, user_lname, user_email, user_mobile,user_udid, user_platform', 'required'),
			array('user_status', 'numerical', 'integerOnly'=>true),
			array('user_fname, user_lname', 'length', 'max'=>50),
			array('user_email', 'length', 'max'=>150),
			array('user_udid', 'length', 'max'=>200),
			array('user_avatar, user_mobile, user_platform', 'length', 'max'=>50),
			array('user_email','unique'),
			array('user_mobile','numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('user_id, user_fname, user_lname, user_email, user_avatar, user_mobile, user_pwd, user_udid, user_platform, user_created, user_updated, user_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'User',
			'user_fname' => 'User Fname',
			'user_lname' => 'User Lname',
			'user_email' => 'User Email',
			'user_avatar' => 'User Avatar',
			'user_mobile' => 'User Mobile',
			'user_pwd' => 'User Pwd',
			'user_udid' => 'User Udid',
			'user_platform' => 'User Platform',
			'user_created' => 'User Created',
			'user_updated' => 'User Updated',
			'user_status' => 'User Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('user_fname',$this->user_fname,true);
		$criteria->compare('user_lname',$this->user_lname,true);
		$criteria->compare('user_email',$this->user_email,true);
		$criteria->compare('user_avatar',$this->user_avatar,true);
		$criteria->compare('user_mobile',$this->user_mobile,true);
		$criteria->compare('user_pwd',$this->user_pwd,true);
		$criteria->compare('user_udid',$this->user_udid,true);
		$criteria->compare('user_platform',$this->user_platform,true);
		$criteria->compare('user_created',$this->user_created,true);
		$criteria->compare('user_updated',$this->user_updated,true);
		$criteria->compare('user_status',$this->user_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
