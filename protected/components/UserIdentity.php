<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	// public function authenticate()
	// {
	// 	$users=array(
	// 		// username => password
	// 		'demo'=>'demo',
	// 		'admin'=>'admin',
	// 	);
	// 	if(!isset($users[$this->username]))
	// 		$this->errorCode=self::ERROR_USERNAME_INVALID;
	// 	elseif($users[$this->username]!==$this->password)
	// 		$this->errorCode=self::ERROR_PASSWORD_INVALID;
	// 	else
	// 		$this->errorCode=self::ERROR_NONE;
	// 	return !$this->errorCode;
	// }

    private $_userData = [];
    
    public function getUserData() {
        return $this->_userData;
    }

	public function authenticate()
	{
        $utils = new Utils;
        $user = Admin::model()->findAllByAttributes(array('admin_email' => strtolower($this->username)));
        if (empty($user)) {
            $this->errorCode = self::ERROR_UNKNOWN_IDENTITY;
        } elseif ($utils->passwordDecrypt($user[0]->admin_password) != ($this->password)) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else {
            $this->_userData['admin_id'] = $user[0]->admin_id;
            $this->_userData['admin_name'] = $user[0]->admin_name;
            $this->_userData['admin_role'] = $user[0]->admin_role;
            $this->_userData['admin_image'] = $user[0]->admin_image;
            $role = $user[0]->admin_role == 1 ? 'admin' : '';
            Yii::app()->user->name = $role;
            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
	}
}