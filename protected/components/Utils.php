<?php

class Utils {
    /*
     * Random Password Functions
     */

    public static function getRandomPassword($length = 8) {

        $characters = '123456789abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ';

        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }

        return $randomString;
    }

    /*
     * Base Url
     */

    public static function getBaseUrl() {
        return Yii::app()->baseUrl;
    }

    /*
     * Base Path
     */

    public static function getBasePath() {
        return Yii::app()->basePath;
    }

    /*
     * Get Style Url
     */

    public static function getStyleUrl() {
        return Yii::app()->baseUrl . '/bootstrap/admin/';
    }

    /*
     * Password Encryption and Decryption Functions
     */

    private function Encryption_Key() {
        $string = 'd0a7e7997b6d5fcd55f4b5c32611b87cd923e88837b63bf2941ef819dc8ca282';
        return $string;
    }

    private function mc_encrypt($encrypt, $key) {
        $encrypt = serialize($encrypt);
        $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC), MCRYPT_DEV_URANDOM);
        $key = pack('H*', $key);
        $mac = hash_hmac('sha256', $encrypt, substr(bin2hex($key), -32));
        $passcrypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $encrypt . $mac, MCRYPT_MODE_CBC, $iv);
        $encoded = base64_encode($passcrypt) . '|' . base64_encode($iv);
        return $encoded;
    }

    private function mc_decrypt($decrypt, $key) {
        $decrypt = explode('|', $decrypt . '|');
        $decoded = base64_decode($decrypt[0]);
        $iv = base64_decode($decrypt[1]);
        if (strlen($iv) !== mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC)) {
            return false;
        }
        $key = pack('H*', $key);
        $decrypted = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $decoded, MCRYPT_MODE_CBC, $iv));
        $mac = substr($decrypted, -64);
        $decrypted = substr($decrypted, 0, -64);
        $calcmac = hash_hmac('sha256', $decrypted, substr(bin2hex($key), -32));
        if ($calcmac !== $mac) {
            return false;
        }
        $decrypted = unserialize($decrypted);
        return $decrypted;
    }

    function passwordEncrypt($password) {
        return $this->mc_encrypt($password, $this->Encryption_Key());
    }

    function passwordDecrypt($password) {
        return $this->mc_decrypt($password, $this->Encryption_Key());
    }

    /*
     * Product Images 
     */

    public static function ProductImageTempBasePath() {
        $temp_path = Yii::app()->basePath . "/../bootstrap/uploads/product/temp/";
        return $temp_path;
    }

    public static function ProductImageBasePath() {
        $product_image_path = Yii::app()->basePath . "/../bootstrap/uploads/product/";
        return $product_image_path;
    }

    public static function ProductImageThumbnailBasePath() {
        $product_image_path = Yii::app()->basePath . "/../bootstrap/uploads/product/thumbs/";
        return $product_image_path;
    }

    public static function ProductImagePath() {
        $product_image_path = Yii::app()->baseUrl . "/bootstrap/uploads/product/";
        return $product_image_path;
    }

    public static function ProductImageThumbnailPath() {
        $product_image_path = Yii::app()->baseUrl . "/bootstrap/uploads/product/thumbs/";
        return $product_image_path;
    }

    /*
     * User Images 
     */

    public static function UserImagePath() {
        $user_image_path = Yii::app()->request->baseUrl . "/upload/user/";
        return $user_image_path;
    }

    public static function UserThumbnailImagePath() {
        $user_image_path = Yii::app()->request->baseUrl . "/bootstrap/uploads/user/thumbs/";
        return $user_image_path;
    }

    public static function UserImageBasePath() {
        $user_image_path = Yii::app()->basePath . "/../upload/user/";
        return $user_image_path;
    }

    public static function UserThumbnailImageBasePath() {
        $user_image_path = Yii::app()->basePath . "/../bootstrap/uploads/user/thumbs/";
        return $user_image_path;
    }

    public static function UserImagePath_M() {
        $user_m = Utils::getStyleUrl() . '/img/avatar_m.png';
        return $user_m;
    }

    public static function UserImagePath_F() {
        $user_f = Utils::getStyleUrl() . '/img/avatar_f.png';
        return $user_f;
    }

    /*
     * No Vehicle Image
     */

    public static function getNoImageAvailable() {
        return Yii::app()->baseUrl . '/bootstrap/dashboard/img/default.jpeg';
    }

    /*
     * Vehicle Image
     */

    public static function getVehicleImage() {
        return Yii::app()->baseUrl . '/bootstrap/uploads/vehicle/';
    }

    public static function getVehicleImageBasePath() {
        $path = Yii::app()->basePath . "/../bootstrap/uploads/vehicle/";
        return $path;
    }

    /*
     * Vehicle Image Thumbnail
     */

    public static function getVehicleImageThumb() {
        return Yii::app()->baseUrl . '/bootstrap/uploads/vehicle/thumbs/';
    }

    public static function getVehicleImageThumbBasePath() {
        $path = Yii::app()->basePath . "/../bootstrap/uploads/vehicle/thumbs/";
        return $path;
    }

    /*
     * User Image
     */

    public static function getUserImage() {
        return Yii::app()->baseUrl . '/bootstrap/uploads/user/';
    }

    public static function getUserImageBasePath() {
        $path = Yii::app()->basePath . "/../bootstrap/uploads/user/";
        return $path;
    }

    /*
     * User Image Thumbnail
     */

    public static function getUserImageThumb() {
        return Yii::app()->baseUrl . '/bootstrap/uploads/user/thumbs/';
    }

    public static function getUserImageThumbBasePath() {
        $path = Yii::app()->basePath . "/../bootstrap/uploads/user/thumbs/";
        return $path;
    }

    public function replace($array, $str) {
        foreach ($array as $key => $value) {
            $str = str_replace("$" . $key, $value, $str);
        }
        return $str;
    }

    public function Send($to, $to_name, $subject, $message) {
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->IsSMTP();
        $mail->Host = 'smtp.gmail.com:465';
        $mail->SMTPSecure = "ssl";
        $mail->SMTPAuth = true;
        $mail->Username = 'nitin.j@cisinlabs.com';
        $mail->Password = 'cgw3Amce';
        //$mail->SetFrom(Yii::app()->params['adminEmail'], Yii::app()->name);
        $mail->SetFrom('info@uride.com', 'U-Ride');
        $mail->Subject = $subject;
        $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
        $mail->MsgHTML($message);
        $mail->AddAddress($to, $to_name);

         if ($mail->Send()) {
             return true;
         } else {
             return false;
         }
        return true;
    }

    /*
     * Promotion File Path
     */

    public static function getPromotionFilePath() {
        return Yii::app()->baseUrl . '/bootstrap/uploads/promotion/';
    }

    public static function getPromotionFileBasePath() {
        $path = Yii::app()->basePath . "/../bootstrap/uploads/promotion/";
        return $path;
    }

    /*
     * Quota Banner Image Path
     */

    public static function getQuotaImagePath() {
        return Yii::app()->baseUrl . '/bootstrap/uploads/quota/';
    }

    public static function getQuotaImageBasePath() {
        $path = Yii::app()->basePath . "/../bootstrap/uploads/quota/";
        return $path;
    }
    
    public static function getConfigValue($name){
        $config = Config::model()->findByAttributes(array('config_name' => $name));
        if(!empty($config)){
            return $config['config_value'];
        }
        return FALSE;
    }

    public static function getColorClass($color_code) {
        $color_class = 'yellow-row';
        switch ($color_code) {
            case 1: $color_class = 'yellow-row';
                break;
            case 2: $color_class = 'green-row';
                break;
            case 3: $color_class = 'white-row';
                break;
            default :$color_class = 'yellow-row';
                break;
        }
        return $color_class;
    }

    public static function getRejectColorClass() {
        $color_class = 'red-row';
        return $color_class;
    }

    public static function getUserType($num) {
        $name = 'General Card';
        switch ($num) {
            case 1:
                $name = 'VISA Infinite Card';
                break;
            case 2:
                $name = 'General Card';
                break;
            default :
                $name = 'General Card';
                break;
        }
        return $name;
    }

    public static function getUserPlatform($num) {
        $name = 'Android';
        switch ($num) {
            case 1:
                $name = 'Android';
                break;
            case 2:
                $name = 'iPhone';
                break;
            default :
                $name = 'Android';
                break;
        }
        return $name;
    }

    //Booking ID($booking_id): Booking ID of the Record
    //Service ID($service_id): 1 - Hourly On Hire, 2 - Adhoc Point to Point
    //Message Type: 0 - Reject, 1 - Order

    public static function SendNotification($booking_id, $service_id, $message_type, $message, $user_id) {

        $token_1 = array();
        $token_2 = array();

        $devices = Devices::model()->findAllByAttributes(array('device_userID' => $user_id));
        if (!empty($devices)) {
            foreach ($devices as $d) {
                if ($d->device_type == 1) {
                    $token_1[] = $d->device_token;
                } else {
                    $token_2[] = $d->device_token;
                }
            }
        }

        if ($message_type == 0) {
            $message = array(
                'service_id' => $service_id,
                'service_type' => FALSE,
                'service_message' => $message
            );
        } else {
            $message = array(
                'service_id' => $service_id,
                'service_type' => TRUE,
                'service_message' => $message
            );
        }
        $message = json_encode($message);

        //Android            
        if (count($token_1) > 0) {
            Utils::SendNotificationAndroid($token_1, $message);
        }

        //iPhone
        if (count($token_2) > 0) {
            foreach ($token_2 as $token) {
                Utils::SendNotificationIphone($token, $message);
            }
        }
    }

    
    
    public static function SendNotificationAndroid($token, $message) {
        $GOOGLE_API_KEY = 'AIzaSyD_elCI0g8kt2vmfIxdkn-9ajnECIedL8E';

        // Set POST variables        
        $url = 'https://android.googleapis.com/gcm/send';
        $msg = array(
            'message' => $message,
            'title' => 'Parklane Limousine',
            'subtitle' => '',
            'tickerText' => '',
            'vibrate' => 1,
            'sound' => 1
        );

        //print_r($registatoin_ids);die;
        $fields = array(
            'registration_ids' => $token,
            'data' => $msg
        );

        $headers = array(
            'Authorization: key=' . $GOOGLE_API_KEY,
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);
        $t = 1;
        if ($result === FALSE) {
            $t = 0;
            die('Curl failed: ' . curl_error($ch));
        }

        // Close connection
        curl_close($ch);
        //exit;
        return $t;
    }

    public static function SendNotificationIphone($deviceToken, $message) {
        $path = $_SERVER['DOCUMENT_ROOT'] . '/bootstrap/ProductionCertificates.pem';
        $passphrase = '';

        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', $path);
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

        // Open a connection to the APNS server
        $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
        if (!$fp) {
            exit("Failed to connect: $err $errstr" . PHP_EOL);
        }
        //echo 'Connected to APNS' . PHP_EOL;
        // Create the payload body
        $body['aps'] = array(
            'alert' => 'You have new notification.',
            'sound' => 'default'
        );
        $body['message'] = $message;

//        print_r($body['aps']);
//        die;

        // Encode the payload as JSON
        $payload = json_encode($body);

        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));

        if (!$result)
            return 1;
        else
            return 0;

// Close the connection to the server
        fclose($fp);
    }

    public static function SendNotificationIphone1($deviceToken, $message) {
        $path = $_SERVER['DOCUMENT_ROOT'] . '/bootstrap/DevelopmentCertificates.pem';

// Put your private key's passphrase here:
        $passphrase = '123';

        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', $path);
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

        // Open a connection to the APNS server
        $fp = stream_socket_client(
                'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);

        //echo 'Connected to APNS' . PHP_EOL;
        // Create the payload body
        $body['aps'] = array(
            'alert' =>
            $message,
            'sound' => 'default'
        );

        // Encode the payload as JSON
        $payload = json_encode($body);

        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', trim($deviceToken)) . pack('n', strlen($payload)) . $payload;

        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));

        if (!$result)
            return 0; //echo 'Message not delivered' . PHP_EOL;
        else
            return 1; //echo 'Message successfully delivered' . PHP_EOL;

        fclose($fp);
    }
    
    
    public static function DriverImageBasePath() {
        $user_image_path = Yii::app()->basePath . "/../upload/drivers/";
        return $user_image_path;
    }    
    public static function DriverImagePath() {
        $user_image_path = Yii::app()->request->baseUrl . "/upload/drivers/";
        return $user_image_path;
    }    


    function random_password( $length = 8 ) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
        $password = substr( str_shuffle( $chars ), 0, $length );
        return $password;
    }
}
