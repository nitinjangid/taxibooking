<?php

class SettingsController extends Controller {

    public $layout = '//layouts/column1';
    public $data_array = [];

    protected function beforeAction($event) {
        if (!isset(Yii::app()->session['admin_data'])):
            $this->redirect(Yii::app()->createAbsoluteUrl('authentication/login'));
        endif;
        return TRUE;
    }


    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        if (isset(Yii::app()->session['admin_data']) && !empty(Yii::app()->session['admin_data'])):
            $this->render('//admin/dashboard');
        else:
            $this->redirect(Yii::app()->createAbsoluteUrl('authentication/login'));
        endif;
    }
   
     /**
      * [actionDriverInfoEdit Action to edit driver information]
      * @param  [type] $id [driver id]
      * @return [type]     [description]
      * @developer : Amit K Sharma
      */
    public function actionSettings() {
        $data_array['css_file']     = 'settings';
        $data_array['title']        = 'Settings';
        $data_array['sub_title']    = '';
        $data_array['js_file']      = 'settings';
        $data_array['model']     = Fare::model()->findAll()[0];
        $this->render('/admin/settings/settings', $data_array);
    }


    public function actionUpdateSettings() {
        $post_data =  $_POST;
        $settings = Fare::model()->findByPk($_POST['id']);
        // creating the instance of CDbCriteria
        if(!empty($post_data)):
            if($settings->validate()):
                // assigning all the attribute
                $settings->fare_base   = $post_data['fare_base'];
                $settings->fare_time     = $post_data['fare_time'];
                $settings->fare_distance    = $post_data['fare_distance'];
                $settings->fare_commision    = $post_data['fare_commision'];

                // saving the driver Address info to the database
                if($settings->save()):
                    Yii::app()->user->setFlash('type', 'success');
                    Yii::app()->user->setFlash('message', 'Updated Successfully.');
                else:
                    Yii::app()->user->setFlash('type', 'danger');
                    Yii::app()->user->setFlash('message', 'Some Errors are found!');
                endif;
            else:
                    Yii::app()->user->setFlash('type', 'danger');
                    Yii::app()->user->setFlash('message', 'No Data has been received!');
            endif;
        endif;

        $this->redirect(Yii::app()->createAbsoluteUrl('settings#tab_1'));
    }

} // end class
