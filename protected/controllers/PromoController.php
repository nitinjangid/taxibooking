<?php

class PromoController extends Controller {

    public $layout = '//layouts/column1';
    public $data_array = [];

    protected function beforeAction($event) {
        if (!isset(Yii::app()->session['admin_data'])):
            $this->redirect(Yii::app()->createAbsoluteUrl('authentication/login'));
        endif;
        return TRUE;
    }


    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        if (isset(Yii::app()->session['admin_data']) && !empty(Yii::app()->session['admin_data'])):
            $this->render('//admin/dashboard');
        else:
            $this->redirect(Yii::app()->createAbsoluteUrl('authentication/login'));
        endif;
    }

    /**
     * [actionProfile Used to render the profile view]
     * @return [type] [description]
     * @developer : Amit K Sharma
     */
    public function actionPromo() {
        $data_array['css_file']     = 'promo';
        $data_array['title']        = 'Promocode Management';
        $data_array['sub_title']    = '';
        $data_array['js_file']      = 'promo';
        $data_array['model']        = new Driver;
        $this->render('/admin/promo/promo', $data_array);
    }


    /**
     * [actionShowlist Show the list of drivers]
     * @return [type] [description]
     * @developer : Amit K Sharma
     */
    public function actionShowlist(){

        $model = Promo::model()->findAll();
        $iTotalRecords  = count($model);
        $iDisplayLength = intval($_REQUEST['iDisplayLength']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart  = intval($_REQUEST['iDisplayStart']);
        $sEcho = intval($_REQUEST['sEcho']);
        
        $records           = array();
        $records["aaData"] = array();

        $end               = $iDisplayStart + $iDisplayLength;
        $end               = $end > $iTotalRecords ? $iTotalRecords : $end;

        $joins             = array();

        $condition = '';
        $code = $dis = $exp = '';

        // creating the instance of CDbCriteria
        $criteria = new CDbCriteria;

        if(!empty($_POST['promo_code'])){
            $code = $_POST['promo_code'];
            $criteria->compare('promo_code',$code,true);
        }
      
        if(!empty($_POST['promo_dis_per'])){
            $dis = $_POST['promo_dis_per'];
            // $criteria->compare('promo_dis_per',$dis,true);
            $criteria->condition = "promo_dis_per = $dis";
        }

        if(!empty($_POST['promo_exp_date'])){
            $exp = $_POST['promo_exp_date'];
            $criteria->compare('promo_exp_date',$exp,true);
            // $criteria->condition = "promo_exp_date = $exp";
        }

        // setting the limit and offset using CDbCriteria class instance
        $criteria->limit = $iDisplayLength;
        $criteria->offset = $iDisplayStart;
        $data              = Promo::model()->findAll($criteria);
        
        if(is_array($data)){
            $i=1;
            foreach($data as $row){
                $records["aaData"][] = array(
                                             $i,
                                             $row['promo_code'],
                                             $row['promo_dis_per'],
                                             $row['promo_exp_date'],
                                             $row['promo_status'] == 'Y'? '<span class="btn default btn-xs green" onClick="disapprovePromo('.$row['promo_id'].')"> Approved </span>' : '<span class="btn default btn-xs red" onClick="approvePromo('.$row['promo_id'].')"> Not-approved </span>',
                                             '<span class="btn default btn-xs blue" onClick="viewPromo('.$row['promo_id'].')"> <i class="fa fa-eye"></i> View </span>
                                             <span class="btn default btn-xs green" onclick="editPromo('.$row['promo_id'].')"> <i class="fa fa-edit"></i> Edit </span>
                                             <span class="btn default btn-xs" onclick="promoDelete('.$row['promo_id'].')"> <i class="fa fa-trash"></i> Delete </span>'
                                             );
            $i++;}
        }else{
            $records["aaData"][] = array('<i>No records</i>', null, null,null,null,null);
        }
      
        $records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;
        echo json_encode($records);
    }

    /**
     * [actionRegisterDriver Action to register a driver]
     * @return [type] [description]
     * @developer : Amit K Sharma
     */
    public function actionPromoAdd() {
        $post_data =  $_POST;

        if(!empty($post_data)):
            $promo = new Promo;
            $promo->attributes = $post_data;
            if($promo->validate()):

                if(empty($post_data['promo_id'])){

                    // assigning all the attribute
                    $promo->promo_code    = ucwords($post_data['promo_code']);
                    $promo->promo_dis_per    = ucwords($post_data['promo_dis_per']);
                    $promo->promo_exp_date   = $post_data['promo_exp_date'];

                    if($promo->save()):
                        Yii::app()->user->setFlash('type', 'success');
                        Yii::app()->user->setFlash('message', 'Promo Code Added Successfully.');
                    else:
                        Yii::app()->user->setFlash('type', 'danger');
                        Yii::app()->user->setFlash('message', 'Some Errors are found!');
                    endif;
                }else{
                    $promo = Promo::model()->findByPk($post_data['promo_id']);
                    $promo->promo_code     = $post_data['promo_code'];
                    $promo->promo_dis_per  = $post_data['promo_dis_per'];
                    $promo->promo_exp_date = $post_data['promo_exp_date'];

                    if($promo->save()):
                        Yii::app()->user->setFlash('type', 'success');
                        Yii::app()->user->setFlash('message', 'Promo Code Updated Successfully.');
                    else:
                        Yii::app()->user->setFlash('type', 'danger');
                        Yii::app()->user->setFlash('message', 'Some Errors are found!');
                    endif;
                }
            else:
                    Yii::app()->user->setFlash('type', 'danger');
                    Yii::app()->user->setFlash('message', 'No Data has been received!');
            endif;
        endif;

        $this->redirect(Yii::app()->createAbsoluteUrl('promo'));
    }

    public function actionGetPromoDetails(){
        $post_data =  $_POST;
        $promo = Promo::model()->findByPk($post_data['promo_id']);
        $promoData['promo_id'] = $promo->promo_id;
        $promoData['promo_code'] = $promo->promo_code;
        $promoData['promo_dis_per'] = $promo->promo_dis_per;
        $promoData['promo_exp_date'] = $promo->promo_exp_date;
        $promoData['promo_status'] = $promo->promo_status;
        $promoData['created_at'] = $promo->created_at;
        $promoData['updated_at'] = $promo->updated_at;
        if(!empty($promo)){
            echo json_encode(['errors' => 0, 'data' => $promoData]);
        }else{
            echo json_encode(['errors' => 1]);
        }
    }


    public function actionPromoApprove(){
         $post_data =  $_POST;
         $promo = Promo::model()->findByPk($post_data['promo_id']);
         $promo->promo_status   = 'Y';
         if($promo->save()):
            echo json_encode(['errors' => 0]);
         else:
            echo json_encode(['errors' => 1]);
         endif;

    }

    public function actionPromoDisapprove(){
         $post_data =  $_POST;
         $promo = Promo::model()->findByPk($post_data['promo_id']);
         $promo->promo_status   = 'N';
         if($promo->save()):
            echo json_encode(['errors' => 0]);
         else:
            echo json_encode(['errors' => 1]);
         endif;

    }    


     /**
      * [actionDriverDelete Action to delete the Driver information from the database]
      * @return [type] [description]
      * @developer : Amit K Sharma
      */
     function actionPromoDelete(){
        if(Promo::model()->findByPk($_POST['promo_id'])->delete()):
            echo json_encode(array('errors' => 0));
        else:
            echo json_encode(array('errors' => 1));
        endif;
     }

   

} // end class
