<?php

class BookingsController extends Controller {

    public $layout = '//layouts/column1';
    public $data_array = [];

    protected function beforeAction($event) {
        if (!isset(Yii::app()->session['admin_data'])):
            $this->redirect(Yii::app()->createAbsoluteUrl('authentication/login'));
        endif;
        return TRUE;
    }


    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        if (isset(Yii::app()->session['admin_data']) && !empty(Yii::app()->session['admin_data'])):
            $this->render('//admin/dashboard');
        else:
            $this->redirect(Yii::app()->createAbsoluteUrl('authentication/login'));
        endif;
    }

    /**
     * [actionProfile Used to render the profile view]
     * @return [type] [description]
     * @developer : Amit K Sharma
     */
    public function actionBooking() {
        $data_array['css_file']     = 'booking';
        $data_array['title']        = 'Booking Management';
        $data_array['sub_title']    = '';
        $data_array['js_file']      = 'booking';
        $this->render('/admin/booking/booking', $data_array);
    }


    /**
     * [actionShowlist Show the list of drivers]
     * @return [type] [description]
     * @developer : Amit K Sharma
     */
    public function actionShowlist(){
        $sql = 'SELECT ur_booking.*,ur_request.req_src_adrs, ur_request.req_des_adrs, ur_request.req_distance, ur_request.req_time,ur_driver.driver_fname, ur_driver.driver_lname
                FROM ur_booking 
                INNER JOIN ur_request 
                ON ur_request.req_id = ur_booking.booking_requestID 
                INNER JOIN ur_driver 
                ON ur_driver.driver_id = ur_booking.booking_driverID';
                
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $model = $command->queryAll();
        
        $iTotalRecords  = count($model);
        $iDisplayLength = intval($_REQUEST['iDisplayLength']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart  = intval($_REQUEST['iDisplayStart']);
        $sEcho = intval($_REQUEST['sEcho']);
        
        $records           = array();
        $records["aaData"] = array();

        $end               = $iDisplayStart + $iDisplayLength;
        $end               = $end > $iTotalRecords ? $iTotalRecords : $end;

        $joins             = array();

        $condition = '';
        $user = $sAdd = $dAdd = $dist = $time = $status = '';
        $where = '';

        if(!empty($_POST['user'])){
            $user = $_POST['user'];
            // $criteria->compare('user_fname',$user,true);
            $where .= 'WHERE CONCAT(driver_fname, driver_lname) LIKE '. "'%$user%'" ;
        }
      
        if(!empty($_POST['sAdd'])){
            $sAdd = $_POST['sAdd'];
            // $criteria->compare('req_src_adrs',$sAdd,true);
            $where .= 'WHERE req_src_adrs LIKE '."'%$sAdd%'" ;
        }

        if(!empty($_POST['dAdd'])){
            $dAdd = $_POST['dAdd'];
            // $criteria->compare('req_des_adrs',$dAdd,true);
            $where .= 'WHERE req_des_adrs LIKE '."'%$dAdd%'" ;
        }

        if(!empty($_POST['distance'])){
            $dist = $_POST['distance'];
            // $criteria->compare('req_distance',$dist,true);
            $where .= 'WHERE req_distance = '.$dist ;
        }

        if(!empty($_POST['time'])){
            $time = $_POST['time'];
            // $criteria->compare('req_distance',$dist,true);
            $where .= 'WHERE req_time = '.$time ;
        }

        if(!empty($_POST['status'])){
            $status = $_POST['status'];
            // $criteria->compare('req_status',$status,true);
            $where .= 'WHERE booking_status ='."'$status'" ;
        }   


        $column = $_POST["iSortCol_0"];
        $sort_by = 'DESC';

        if(!empty($_POST["sAction"])):
          if($_POST["sAction"] == 'filter')
            $column = 0;
        endif;

        switch($column){
                case 1:
                    $order_by = 'ORDER BY driver_fname';
                    $sort_by = $_POST["sSortDir_0"];
                    break;
                case 2:
                    $order_by = 'ORDER BY req_src_adrs';
                    $sort_by = $_POST["sSortDir_0"];
                    break;
                case 3:
                    $order_by = 'ORDER BY req_des_adrs';
                    $sort_by = $_POST["sSortDir_0"];
                    break;
                case 4:
                    $order_by = 'ORDER BY req_distance';
                    $sort_by = $_POST["sSortDir_0"];
                    break;
                case 5:
                    $order_by = 'ORDER BY req_time';
                    $sort_by = $_POST["sSortDir_0"];
                    break;
                case 6:
                    $order_by = 'ORDER BY booking_status';
                    $sort_by = $_POST["sSortDir_0"];
                    break;
                default:
                    $order_by = 'ORDER BY booking_id';
        }

        // creating the instance of CDbCriteria
        $criteria = new CDbCriteria;

        $sql = 'SELECT ur_booking.*,ur_request.req_src_adrs, ur_request.req_des_adrs, ur_request.req_distance, ur_request.req_time,ur_driver.driver_fname, ur_driver.driver_lname
                FROM ur_booking 
                INNER JOIN ur_request 
                ON ur_request.req_id = ur_booking.booking_requestID 
                INNER JOIN ur_driver 
                ON ur_driver.driver_id = ur_booking.booking_driverID
                '.$where.' '.$order_by.' '.$sort_by.'
                LIMIT '.$iDisplayLength.' OFFSET '.$iDisplayStart;
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $data = $command->queryAll();

        // $data             = Request::model()->findAll($criteria);
        // echo '<pre>';print_r($data);die;
        if(is_array($data)){
            $i=1;
            foreach($data as $row){
                $records["aaData"][] = array(
                                             $i,
                                             $row['driver_fname'] != '' ? ucwords($row['driver_fname'].' '.$row['driver_lname']) : '<i>NA</i>' ,
                                             $row['req_src_adrs'] != '' ? ucwords(wordwrap($row['req_src_adrs'],20,"<br>\n")) : '<i>NA</i>' ,
                                             $row['req_des_adrs'] != '' ? ucwords(wordwrap($row['req_des_adrs'],20,"<br>\n")) : '<i>NA</i>' ,
                                             $row['req_distance'] != '' ? $row['req_distance'] : '<i>NA</i>',
                                             $row['req_time'] != '' ? $row['req_time'] : '<i>NA</i>',
                                             $row['booking_status'] == "R" ? '<span class="label label-danger allow-field">Rejected</span>' : ($row['booking_status'] == "O" ? '<span class="label label-primary allow-field">Open</span>' : ($row['booking_status'] == "C" ? '<span class="label label-warning allow-field">Closed</span>' : '') ),
                                              '<a href="javascript:;" class="btn default btn-xs blue" data-toggle="modal" onClick="getDetail('.$row['booking_id'].')"><i class="fa fa-eye"></i> view</a>
                                              <a href="javascript:void(0);" onClick="delete_fun('.$row['booking_id'].');" class="btn default btn-xs black "><i class="fa fa-trash-o"></i> Delete</a>'
                                             );
            $i++;}
        }else{
            $records["aaData"][] = array('<i>No records</i>', null, null,null,null,null,null,null);
        }
      
        $records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;
        echo json_encode($records);
    }

     /**
      * [actionDriverDelete Action to delete the Driver information from the database]
      * @return [type] [description]
      * @developer : Amit K Sharma
      */
     function actionBookingDelete(){
        if(Booking::model()->findByPk($_POST['id'])->delete()):
            echo json_encode(array('errors' => 0));
        else:
            echo json_encode(array('errors' => 1));
        endif;
     }

    /**
     * [actionDriverInfoView Action to show the driver details on view event]
     * @param  [type] $id [Driver ID]
     * @return [type]     [description]
     * @developer : Amit K Sharma
     */
    public function actionBookingInfoView() {
        $id = $_POST['id'];
        $sql = 'SELECT ur_booking.*,ur_request.req_src_adrs, ur_request.req_des_adrs, ur_request.req_distance, ur_request.req_time,ur_driver.driver_fname, ur_driver.driver_lname
                FROM ur_booking 
                INNER JOIN ur_request 
                ON ur_request.req_id = ur_booking.booking_requestID 
                INNER JOIN ur_driver 
                ON ur_driver.driver_id = ur_booking.booking_driverID WHERE ur_booking.booking_id ='.$id;
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $bookings = $command->queryAll();
        if(!empty($bookings)):
          echo json_encode(['errors' => 0, 'data' => $bookings[0]]);
        else:
          echo json_encode(['errors' => 0, 'msg' => 'No data found!']);
        endif;
    }    

    public function actionPayment() {
        $data_array['css_file']     = 'payment';
        $data_array['title']        = 'Payment Management';
        $data_array['sub_title']    = '';
        $data_array['js_file']      = 'payment';
        $this->render('/admin/booking/payment', $data_array);
    } 

    public function actionShowPaymentlist(){
        $sql = 'SELECT ur_driver.driver_fname,ur_driver.driver_lname,ur_driver.driver_email, ur_driver.driver_mobile, SUM(ur_account.acc_credit) - SUM(ur_account.acc_debit) acc_total
            FROM ur_account
            INNER JOIN ur_driver ON ur_driver.driver_id = ur_account.acc_driverID GROUP BY acc_driverID';
        // echo '<pre>';print_r($sql);die;        
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $model = $command->queryAll();
        
        $iTotalRecords  = count($model);
        $iDisplayLength = intval($_REQUEST['iDisplayLength']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart  = intval($_REQUEST['iDisplayStart']);
        $sEcho = intval($_REQUEST['sEcho']);
        
        $records           = array();
        $records["aaData"] = array();

        $end               = $iDisplayStart + $iDisplayLength;
        $end               = $end > $iTotalRecords ? $iTotalRecords : $end;

        $joins             = array();

        $condition = '';
        $name = $email = $mobile = '';

        // creating the instance of CDbCriteria
        $criteria = new CDbCriteria;

      $sql ='SELECT ur_driver.driver_id, ur_driver.driver_fname,ur_driver.driver_lname,ur_driver.driver_email, ur_driver.driver_mobile, SUM(ur_account.acc_credit) - SUM(ur_account.acc_debit) acc_total
            FROM ur_account
            INNER JOIN ur_driver ON ur_driver.driver_id = ur_account.acc_driverID GROUP BY acc_driverID
           LIMIT '.$iDisplayLength.' OFFSET '.$iDisplayStart;
                
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $data = $command->queryAll();

        if(is_array($data)){
            $i=1; $ac = '';
            foreach($data as $row){
              if($row['acc_total'] > 0 && $row['acc_total']!=' '){
                $ac = '<a class="btn default btn-xs green" data-toggle="modal" href="#basic" onclick="getId('.$row['driver_id'].','.$row['acc_total'].')"><i class="fa fa-money"></i> Pay</a>';
              }

                $records["aaData"][] = array(
                                             $i,
                                             $row['driver_fname'] != '' ? ucwords($row['driver_fname'].' '.$row['driver_lname']) : '<i>NA</i>' ,
                                             $row['driver_email'] != '' ? $row['driver_email'] : '<i>NA</i>' ,
                                             $row['driver_mobile'] != '' ? $row['driver_mobile'] : '<i>NA</i>' ,
                                             $row['acc_total'] != '' ? round($row['acc_total'], 3) : '<i>NA</i>' ,
                                              '<a href="'.Yii::app()->createAbsoluteUrl('paymentDetails').'/'.$row['driver_id'].'" class="btn default btn-xs blue "><i class="fa fa-eye"></i> view</a> '.$ac.''
                                             );
            $i++;}
        }else{
            $records["aaData"][] = array('<i>No records</i>', null, null,null,null,null);
        }
      
        $records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;
        echo json_encode($records);
    }

    public function actionPaymentInfoView( $id ) {
        $data_array['css_file']  = 'payment';
        $data_array['title']     = 'Payment Management';
        $data_array['sub_title'] = '';
        $data_array['js_file']   = 'payment';
        $data_array['id']        = $id;
        $this->render('/admin/booking/payment_detail', $data_array);
    } 

    public function actionShowPaymentDetail( $id ){
        $sql = 'SELECT ur_booking . * , ur_transaction.txn_code, ur_transaction.txn_status FROM ur_booking INNER JOIN ur_transaction ON ur_booking.booking_id = ur_transaction.txn_bookingID INNER JOIN ur_driver ON ur_booking.booking_driverID = ur_driver.driver_id WHERE driver_id ='.$id .' AND booking_paid = "P"';

        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $model = $command->queryAll();
        
        $iTotalRecords  = count($model);
        $iDisplayLength = intval($_REQUEST['iDisplayLength']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart  = intval($_REQUEST['iDisplayStart']);
        $sEcho = intval($_REQUEST['sEcho']);
        
        $records           = array();
        $records["aaData"] = array();

        $end               = $iDisplayStart + $iDisplayLength;
        $end               = $end > $iTotalRecords ? $iTotalRecords : $end;

        $joins             = array();

        $sql ='SELECT ur_booking . * , ur_transaction.txn_code, ur_transaction.txn_status, ur_transaction.txn_total FROM ur_booking INNER JOIN ur_transaction ON ur_booking.booking_id = ur_transaction.txn_bookingID INNER JOIN ur_driver ON ur_booking.booking_driverID = ur_driver.driver_id WHERE driver_id ='.$id. ' AND booking_paid = "P" LIMIT '.$iDisplayLength.' OFFSET '.$iDisplayStart;
                
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $data = $command->queryAll();

        // $data             = Request::model()->findAll($criteria);
        // echo '<pre>';print_r($data);die;
        if(is_array($data)){
            $i=1;
            foreach($data as $row){
                $records["aaData"][] = array(
                                             $i,
                                             $row['booking_id'],
                                             $row['txn_code'],
                                             $row['txn_total'],
                                             );
            $i++;}
        }else{
            $records["aaData"][] = array('<i>No records</i>', null, null,null);
        }
      
        $records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;
        echo json_encode($records);
    }

} // end class
