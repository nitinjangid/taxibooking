<?php

class AccountController extends Controller {

    public $layout = '//layouts/column1';
    public $data_array = [];

    protected function beforeAction($event) {
        if (!isset(Yii::app()->session['admin_data'])):
            $this->redirect(Yii::app()->createAbsoluteUrl('authentication/login'));
        endif;
        return TRUE;
    }


    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        if (isset(Yii::app()->session['admin_data']) && !empty(Yii::app()->session['admin_data'])):
            $this->render('//admin/dashboard');
        else:
            $this->redirect(Yii::app()->createAbsoluteUrl('authentication/login'));
        endif;
    }

    public function actionPayDriver(){
        $post_data =  $_POST;
        if(!empty($post_data)):
// INSERT INTO ur_account (acc_driverID,acc_debit) VALUES ($post_data['acc_driverID'],$post_data['acc_debit'])
            $sql ="INSERT INTO ur_account (acc_driverID , acc_debit) VALUES (".$post_data['acc_driverID'].", ".$post_data['acc_debit'].")";
            $connection = Yii::app()->db;
            $command = $connection->createCommand($sql);
            $transfer = $command->execute();

            if($transfer):
                echo json_encode(['error'=> '0', 'message'=> 'Payment Added']);
            else:
                echo json_encode(['error'=> '1', 'message'=> 'Error in payment add']);
            endif;
            
        endif;
    }
} // end class
