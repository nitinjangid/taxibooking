<?php

class TransactionController extends Controller {

    public $layout = '//layouts/column1';
    public $data_array = [];

    protected function beforeAction($event) {
        if (!isset(Yii::app()->session['admin_data'])):
            $this->redirect(Yii::app()->createAbsoluteUrl('authentication/login'));
        endif;
        return TRUE;
    }


    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        if (isset(Yii::app()->session['admin_data']) && !empty(Yii::app()->session['admin_data'])):
            $this->render('//admin/dashboard');
        else:
            $this->redirect(Yii::app()->createAbsoluteUrl('authentication/login'));
        endif;
    }

    /**
     * [actionProfile Used to render the profile view]
     * @return [type] [description]
     * @developer : Amit K Sharma
     */
    public function actionTransaction() {
        $data_array['css_file']     = 'transaction';
        $data_array['title']        = 'Transaction Management';
        $data_array['sub_title']    = '';
        $data_array['js_file']      = 'transaction';
        $this->render('/admin/transaction/transaction', $data_array);
    }


    /**
     * [actionShowlist Show the list of drivers]
     * @return [type] [description]
     * @developer : Amit K Sharma
     */
    public function actionShowlist(){

        $model = Transaction::model()->findAll();
        $iTotalRecords  = count($model);
        $iDisplayLength = intval($_REQUEST['iDisplayLength']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart  = intval($_REQUEST['iDisplayStart']);
        $sEcho = intval($_REQUEST['sEcho']);
        
        $records           = array();
        $records["aaData"] = array();

        $end               = $iDisplayStart + $iDisplayLength;
        $end               = $end > $iTotalRecords ? $iTotalRecords : $end;

        $joins             = array();

        $condition = '';
        $name = $email = $mobile = '';

        // creating the instance of CDbCriteria
        $criteria = new CDbCriteria;

        // if(!empty($_POST['driver_name'])){
        //     $name = $_POST['driver_name'];
        //     $criteria->compare('driver_fname',$name,true);
        // }
      
        // if(!empty($_POST['driver_email'])){
        //     $email = $_POST['driver_email'];
        //     $criteria->compare('driver_email',$email,true);
        // }

        // if(!empty($_POST['driver_mobile'])){
        //     $mobile = $_POST['driver_mobile'];
        //     $criteria->condition = "driver_mobile = $mobile";
        // }


        // setting the limit and offset using CDbCriteria class instance
        $criteria->limit = $iDisplayLength;
        $criteria->offset = $iDisplayStart;

        $data             = Transaction::model()->findAll($criteria);
        if(is_array($data)){
            $i=1;
            foreach($data as $row){
                $records["aaData"][] = array(
                                             $i,
                                             $row['txn_code'],
                                             $row['txn_bookingID'],
                                             $row['txn_create_time'],
                                             ucwords(str_replace('_', ' ', $row['txn_payment_method'])),
                                             '<span class="btn default btn-xs blue">'.ucfirst($row['txn_sale_state']).'</span>',
                                             '<a href="javascript:;" class="btn default btn-xs blue" data-toggle="modal" onClick="getDetail('.$row['txn_id'].')"><i class="fa fa-eye"></i> view</a>'
                                             );
            $i++;}
        }else{
            $records["aaData"][] = array('<i>No records</i>', null, null,null,null,null,null,null,null,null,null,null);
        }
      
        $records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;
        echo json_encode($records);
    }

    /**
     * [actionDriverInfoView Action to show the driver details on view event]
     * @param  [type] $id [Driver ID]
     * @return [type]     [description]
     * @developer : Amit K Sharma
     */
    public function actionTransactionInfoView() {
        $id = $_POST['id'];
        $transaction = Transaction::model()->findByPk($id);
        if(!empty($transaction)):
          echo json_encode(['errors' => 0, 'data' => $transaction->attributes]);
        else:
          echo json_encode(['errors' => 0, 'msg' => 'No data found!']);
        endif;
    }     

} // end class
