<?php

class AuthenticationController extends Controller {

    public $layout = FALSE;

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        if (isset(Yii::app()->session['admin_data']) && !empty(Yii::app()->session['admin_data'])):
           $this->redirect(Yii::app()->createAbsoluteUrl('home'));
        else:
            $this->redirect(Yii::app()->createAbsoluteUrl('login'));
        endif;
    }

    
    /**
     * [actionLogin render the login page]
     * @return [type] [description]
     */
    public function actionLogin() {
        $this->render('/admin/login');
    }

    /**
     * [actionLoginsubmit initiate this when login form will submit]
     * @return [type] [description]
     */
    public function actionLoginsubmit() {
            $renderData = [];
            $model = new LoginForm;
            if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form'):
                echo CActiveForm::validate($model);
                Yii::app()->end();
            endif;

            if (!empty($_POST['username']) && !empty($_POST['password'])):
                $model->attributes = $_POST;

                // validate inputs $model->validate(), validate login $model->login() and set session data in $model->login()
                if ($model->validate() && $model->login()):
                    $this->redirect(Yii::app()->createAbsoluteUrl('authentication/dashboard'));
                else:
                    Yii::app()->user->setFlash('msg', 'Invalid Username or Password.');
                endif;
            else:
                    Yii::app()->user->setFlash('msg', 'Please insert username and password.');
            endif;
            $this->redirect(Yii::app()->createAbsoluteUrl('authentication/login'));
    }

    /**
     * [actionLogout Logs out the current user and redirect to homepage.]
     * @return [type] [description]
     */
    public function actionLogout() {
        unset(Yii::app()->session['admin_data']);
        $this->redirect(Yii::app()->createAbsoluteUrl('authentication/login'));
    }


    /**
     * [actionForgotpassword show the forgot password view]
     * @return [type] [description]
     */
    public function actionForgotpassword(){
        $this->render('/admin/forgotPassword');
    }
    
    /**
     * [actionForgotpasswordsubmit Check the input email is registered and send email to recover password]
     * @return [type] [description]
     */
    public function actionForgotpasswordsubmit() {
        // echo '<pre>';print_r($_POST);die;
        if(isset($_POST['email'])):
            $email = $_POST['email'];
            $utils = new Utils;
            $admin = Admin::model()->findByAttributes(array
                ('admin_email' => strtolower($email)));
            // echo '<pre>';print_r($admin->admin_name);die;
            if (!empty($admin)):
                // echo '<pre>';print_r($admin);die;
                $password  = $utils->passwordDecrypt($admin->admin_password);
                $email     = $admin->admin_email;
                $user_name = ucwords($admin->admin_name);
                $to        = $email;

                // $userdata['name']     = $user_name;
                // $userdata['email']    = $email;
                // $userdata['password'] = $password;
               // echo Yii::app()->params['image_path'];die;
                
                // $this->render("/template/forgot_password");die;
                    
                $subject = '=?UTF-8?B?' . base64_encode("Forgot Password") . '?=';
                //$data_array=array();
                $msg=$this->renderPartial("/template/forgot_password",array('email'=>$email,'password'=>$password),true,false);
                
                if ($utils->Send($to, $user_name, $subject, $msg)):
                    Yii::app()->user->setFlash('type', 'success');
                    Yii::app()->user->setFlash('message', 'A Mail has been sent. Please check your Inbox for Mail. Thanks!');
                else:
                    Yii::app()->user->setFlash('type', 'danger');
                    Yii::app()->user->setFlash('message', 'Unknown error. Please contact Site Administrator.');
                endif;
            else:
                Yii::app()->user->setFlash('type', 'danger');
                Yii::app()->user->setFlash('message', 'The E-mail ID is not found in our database. Please check your E-mail ID.');
            endif;
        endif;

        $this->redirect(Yii::app()->createAbsoluteUrl('authentication/forgotpassword'));
    }

    /**
     * Performs the AJAX validation.
     * @param Appinfo $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'appinfo-form'):
            echo CActiveForm::validate($model);
            Yii::app()->end();
        endif;
    }

    /**
     * [actionDashboard Render the dashboard view]
     * @return [type] [description]
     */
    public function actionDashboard(){
       $this->redirect(Yii::app()->createAbsoluteUrl('dashboard/index'));
    }

}
