<?php

class RequestsController extends Controller {

    public $layout = '//layouts/column1';
    public $data_array = [];

    protected function beforeAction($event) {
        if (!isset(Yii::app()->session['admin_data'])):
            $this->redirect(Yii::app()->createAbsoluteUrl('authentication/login'));
        endif;
        return TRUE;
    }


    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        if (isset(Yii::app()->session['admin_data']) && !empty(Yii::app()->session['admin_data'])):
            $this->render('//admin/dashboard');
        else:
            $this->redirect(Yii::app()->createAbsoluteUrl('authentication/login'));
        endif;
    }

    /**
     * [actionProfile Used to render the profile view]
     * @return [type] [description]
     * @developer : Amit K Sharma
     */
    public function actionRequest() {
        $data_array['css_file']     = 'request';
        $data_array['title']        = 'Request Management';
        $data_array['sub_title']    = '';
        $data_array['js_file']      = 'request';
        $this->render('/admin/request/request', $data_array);
    }


    /**
     * [actionShowlist Show the list of drivers]
     * @return [type] [description]
     * @developer : Amit K Sharma
     */
    public function actionShowlist(){
        $model = Request::model()->findAll();
        $iTotalRecords  = count($model);
        $iDisplayLength = intval($_REQUEST['iDisplayLength']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart  = intval($_REQUEST['iDisplayStart']);
        $sEcho = intval($_REQUEST['sEcho']);
        
        $records           = array();
        $records["aaData"] = array();

        $end               = $iDisplayStart + $iDisplayLength;
        $end               = $end > $iTotalRecords ? $iTotalRecords : $end;

        $joins             = array();

        $condition = '';
        $user = $sAdd = $dAdd = $dist = $time = $status = '';

        // creating the instance of CDbCriteria
        // $criteria = new CDbCriteria;

        $where = '';
        if(!empty($_POST['user'])){
            $user = $_POST['user'];
            // $criteria->compare('user_fname',$user,true);
            $where .= 'WHERE CONCAT(user_fname," ",user_lname) LIKE '. "'%$user%'" ;
        }
      
        if(!empty($_POST['sAdd'])){
            $sAdd = $_POST['sAdd'];
            // $criteria->compare('req_src_adrs',$sAdd,true);
            $where .= 'WHERE req_src_adrs LIKE '."'%$sAdd%'" ;
        }

        if(!empty($_POST['dAdd'])){
            $dAdd = $_POST['dAdd'];
            // $criteria->compare('req_des_adrs',$dAdd,true);
            $where .= 'WHERE req_des_adrs LIKE '."'%$dAdd%'" ;
        }

        if(!empty($_POST['distance'])){
            $dist = $_POST['distance'];
            // $criteria->compare('req_distance',$dist,true);
            $where .= 'WHERE req_distance = '.$dist ;
        }

        if(!empty($_POST['time'])){
            $time = $_POST['time'];
            // $criteria->compare('req_distance',$dist,true);
            $where .= 'WHERE req_time = '.$time ;
        }

        if(!empty($_POST['status'])){
            $status = $_POST['status'];
            // $criteria->compare('req_status',$status,true);
            $where .= 'WHERE req_status ='."'$status'" ;
        }   


        $column = $_POST["iSortCol_0"];
        $sort_by = 'DESC';

        if(!empty($_POST["sAction"])):
          if($_POST["sAction"] == 'filter')
            $column = 0;
        endif;

        switch($column){
                case 1:
                    $order_by = 'ORDER BY user_fname';
                    $sort_by = $_POST["sSortDir_0"];
                    break;
                case 2:
                    $order_by = 'ORDER BY req_src_adrs';
                    $sort_by = $_POST["sSortDir_0"];
                    break;
                case 3:
                    $order_by = 'ORDER BY req_des_adrs';
                    $sort_by = $_POST["sSortDir_0"];
                    break;
                case 4:
                    $order_by = 'ORDER BY req_distance';
                    $sort_by = $_POST["sSortDir_0"];
                    break;
                case 5:
                    $order_by = 'ORDER BY req_time';
                    $sort_by = $_POST["sSortDir_0"];
                    break;
                case 6:
                    $order_by = 'ORDER BY req_status';
                    $sort_by = $_POST["sSortDir_0"];
                    break;
                default:
                    $order_by = 'ORDER BY req_id';
                    // $sort_by = 'DESC';
        }

        $sql = 'SELECT ur_request.*, ur_user.user_fname, ur_user.user_lname FROM ur_request INNER JOIN ur_user ON ur_user.user_id = ur_request.req_userID '.$where.' '.$order_by.' '.$sort_by.' LIMIT '.$iDisplayLength.' OFFSET '.$iDisplayStart;
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $data = $command->queryAll();

        // $data             = Request::model()->findAll($criteria);
        // echo '<pre>';print_r($data);die;
        if(is_array($data)){
            $i=1;
            foreach($data as $row){
                $records["aaData"][] = array(
                                             $i,
                                             ucwords($row['user_fname'].' '.$row['user_lname']),
                                             wordwrap($row['req_src_adrs'],20,"<br>\n"),
                                             wordwrap($row['req_des_adrs'],20,"<br>\n"),
                                             $row['req_distance'],
                                             $row['req_time'],
                                             $row['req_status'] == 'C' ? '<span class="label label-primary allow-field">Create</span>' : ($row['req_status'] == 'A' ? '<span class="label label-success allow-field">Accept</span>' : '<span class="label label-danger allow-field">Reject</span>') ,
                                             '<a href="javascript:;" class="btn default btn-xs blue" data-toggle="modal" onClick="getDetail('.$row['req_id'].')"><i class="fa fa-eye"></i> view</a>
                                              <a href="javascript:void(0);" onClick="delete_fun('.$row['req_id'].');" class="btn default btn-xs black "><i class="fa fa-trash-o"></i> Delete</a>'
                                             );
            $i++;}
        }else{
            $records["aaData"][] = array('<i>No records</i>', null, null,null,null,null,null,null,null,null,null,null);
        }
      
        $records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;
        echo json_encode($records);
    }


    public function actionRequestInfoEdit( $id ) {
        $data_array['css_file']     = 'request';
        $data_array['title']        = 'Request Management';
        $data_array['sub_title']    = '';
        $data_array['js_file']      = 'request';
        $sql = 'SELECT ur_request.*, ur_user.user_fname, ur_user.user_lname FROM ur_request INNER JOIN ur_user ON ur_user.user_id = ur_request.req_userID WHERE req_id = '.$id;
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $data_array['model'] = $command->queryAll();
        // $data_array['model']     = Request::model()->findByPk($id);
        $this->render('/admin/request/request_edit', $data_array);
    }

     /**
      * [actionDriverDelete Action to delete the Driver information from the database]
      * @return [type] [description]
      * @developer : Amit K Sharma
      */
     function actionRequestDelete(){
        if(Request::model()->findByPk($_POST['id'])->delete()):
            echo json_encode(array('errors' => 0));
        else:
            echo json_encode(array('errors' => 1));
        endif;
     }

    /**
     * [actionDriverInfoView Action to show the driver details on view event]
     * @param  [type] $id [Driver ID]
     * @return [type]     [description]
     * @developer : Amit K Sharma
     */
    public function actionRequestInfoView() {
        $id = $_POST['id'];
        $sql = 'SELECT ur_request.*, ur_user.user_fname, ur_user.user_lname FROM ur_request INNER JOIN ur_user ON ur_user.user_id = ur_request.req_userID WHERE req_id = '.$id;
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $requests = $command->queryAll();
        if(!empty($requests)):
          echo json_encode(['errors' => 0, 'data' => $requests[0]]);
        else:
          echo json_encode(['errors' => 0, 'msg' => 'No data found!']);
        endif;
    }     

} // end class
