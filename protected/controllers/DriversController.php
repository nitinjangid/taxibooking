<?php

class DriversController extends Controller {

    public $layout = '//layouts/column1';
    public $data_array = [];

    protected function beforeAction($event) {
        if (!isset(Yii::app()->session['admin_data'])):
            $this->redirect(Yii::app()->createAbsoluteUrl('authentication/login'));
        endif;
        return TRUE;
    }


    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        if (isset(Yii::app()->session['admin_data']) && !empty(Yii::app()->session['admin_data'])):
            $this->render('//admin/dashboard');
        else:
            $this->redirect(Yii::app()->createAbsoluteUrl('authentication/login'));
        endif;
    }

    /**
     * [actionProfile Used to render the profile view]
     * @return [type] [description]
     * @developer : Amit K Sharma
     */
    public function actionDrivers() {
        $data_array['css_file']     = 'drivers';
        $data_array['title']        = 'Drivers Management';
        $data_array['sub_title']    = '';
        $data_array['js_file']      = 'drivers';
        $this->render('/admin/drivers/drivers', $data_array);
    }


    /**
     * [actionShowlist Show the list of drivers]
     * @return [type] [description]
     * @developer : Amit K Sharma
     */
    public function actionShowlist(){

        $model = Driver::model()->findAll();
        $iTotalRecords  = count($model);
        $iDisplayLength = intval($_REQUEST['iDisplayLength']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart  = intval($_REQUEST['iDisplayStart']);
        $sEcho = intval($_REQUEST['sEcho']);
        
        $records           = array();
        $records["aaData"] = array();

        $end               = $iDisplayStart + $iDisplayLength;
        $end               = $end > $iTotalRecords ? $iTotalRecords : $end;

        $joins             = array();

        $condition = '';
        $name = $email = $mobile = '';

        // creating the instance of CDbCriteria
        $criteria = new CDbCriteria;

        if(!empty($_POST['driver_name'])){
            $name = $_POST['driver_name'];
            $criteria->compare('driver_fname',$name,true);
        }
      
        if(!empty($_POST['driver_email'])){
            $email = $_POST['driver_email'];
            $criteria->compare('driver_email',$email,true);
        }

        if(!empty($_POST['driver_mobile'])){
            $mobile = $_POST['driver_mobile'];
            $criteria->condition = "driver_mobile = $mobile";
        }

        // setting the limit and offset using CDbCriteria class instance
        $criteria->limit = $iDisplayLength;
        $criteria->offset = $iDisplayStart;
        //$data              = Drivers::model()->findAllBySql('SELECT * FROM pa_drivers LIMIT '.$iDisplayLength.' OFFSET '.$iDisplayStart);
        // $data              = Drivers::model()->findAll(array('limit'=>$iDisplayLength, 'offset' => $iDisplayStart));
        $data              = Driver::model()->findAll($criteria);
        
        if(is_array($data)){
            $i=1;
            foreach($data as $row){
                // $image = $row['driver_avatar'] != "" ? Utils::DriverImagePath().'driver-'.$row['driver_id'].'/avatar/'.$row['driver_avatar'] : Utils::UserImagePath().'noimg/1389952697.png';
                $records["aaData"][] = array(
                                             $i,
                                             // '<img src="'.$image.'" alt="" style="height:50px;width:50px;">',
                                             ucwords($row['driver_fname'].' '.$row['driver_lname']),
                                             strtolower($row['driver_email']),
                                             $row['driver_mobile'],
                                             '<a href="'.Yii::app()->createAbsoluteUrl('driverView').'/'.$row['driver_id'].'" class="btn default btn-xs blue "><i class="fa fa-eye"></i> view</a>
                                             <a href="'.Yii::app()->createAbsoluteUrl('driverEdit').'/'.$row['driver_id'].'" class="btn default btn-xs green "><i class="fa fa-edit"></i> Edit</a>
                                              <a href="javascript:void(0);" onClick="delete_fun('.$row['driver_id'].');" class="btn default btn-xs black "><i class="fa fa-trash-o"></i> Delete</a>'
                                             );
            $i++;}
        }else{
            $records["aaData"][] = array('<i>No records</i>', null, null,null,null,null);
        }
      
        $records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;
        echo json_encode($records);
    }

    /**
     * [actionDriverNew Show form to add new driver]
     * @return [type] [description]
     * @developer : Amit K Sharma
     */
    public function actionDriverNew() {
        $data_array['css_file']     = 'drivers';
        $data_array['title']        = 'Drivers Management';
        $data_array['sub_title']    = '';
        $data_array['js_file']      = 'drivers';
        $data_array['model']        = new Driver;
        $data_array['countries']    = Country::model()->findAll();
        $this->render('/admin/drivers/driver_add', $data_array);
    }

    /**
     * [actionRegisterDriver Action to register a driver]
     * @return [type] [description]
     * @developer : Amit K Sharma
     */
    public function actionRegisterDriver() {
        $post_data =  $_POST;

        $lastInsertId = '';
        if(!empty($post_data)):
            $utils = new Utils;
            $driver = new Driver;
            $driver->attributes = $post_data;
            if($driver->validate()):
                $image = CUploadedFile::getInstanceByName('driver_avatar');
                $name = rand(1111, 9999) . date('Ymdhi');
                
                $driver->driver_avatar='';
                 // checking image and rename
                if (!empty($image)):
                    // getting the image extention
                    $extension = strtolower($image->getExtensionName());
                    $filename = "{$name}.{$extension}";

                    // assigning new image name to the model attribute
                    $driver->driver_avatar = $filename;
                endif;
                
                // assigning all the attribute
                $driver->driver_fname    = ucwords($post_data['driver_fname']);
                $driver->driver_lname    = ucwords($post_data['driver_lname']);
                $driver->driver_gender   = $post_data['driver_gender'];
                $driver->driver_email    = strtolower($post_data['driver_email']);
                // $driver->driver_email    = strtolower('amit.ku@cisinlabs.com');
                $driver->driver_mobile   = $post_data['driver_mobile'];
                $driver->driver_password = $utils->passwordEncrypt($utils->random_password(8));
                // echo '<pre>';print_r($driver);die;
                // saving the driver info to the database
                if($driver->save()):
                        // echo '<pre>';print_r($admin);die;
                        $password  = $utils->passwordDecrypt($driver->driver_password);
                        $name      = ucwords($driver->driver_fname.' '.$driver->driver_lname);
                        $email     = $driver->driver_email;
                        $to        = $email;

                        $userdata['name']     = $name;
                        $userdata['email']    = $email;
                        $userdata['password'] = $password;
                        // echo Yii::app()->params['image_path'];die;

                        // $this->render("/template/driver_reg", $userdata);die;
                            
                        $subject = '=?UTF-8?B?' . base64_encode("Driver Registration") . '?=';
                        //$data_array=array();
                        $msg=$this->renderPartial("/template/driver_reg",$userdata,true,false);

                        $utils->Send($to, $name, $subject, $msg);

                        // get the last inserted id
                        $lastInsertId = Yii::app()->db->getLastInsertID();
                    
                        Yii::app()->db->createCommand("INSERT INTO ur_driver_detail (dd_driverID) VALUES ($lastInsertId)")->execute();

                        // checking and creating directory for drivers
                        // checking for directory is available for driver
                        if (!file_exists(Utils::DriverImageBasePath().'driver-'.$lastInsertId.'/avatar/')):
                            //creating a directory for driver
                            mkdir(Utils::DriverImageBasePath().'driver-'.$lastInsertId.'/avatar/', 0777, true);
                        endif;

                        // saving/moving image to the folder
                        $image->saveAs(Utils::DriverImageBasePath().'driver-'.$lastInsertId.'/avatar/'. $driver->driver_avatar);


                    Yii::app()->user->setFlash('type', 'success');
                    Yii::app()->user->setFlash('message', 'Personal Information Added Successfully.');
                else:
                    Yii::app()->user->setFlash('type', 'danger');
                    Yii::app()->user->setFlash('message', 'Some Errors are found!');
                endif;
            else:
                    Yii::app()->user->setFlash('type', 'danger');
                    Yii::app()->user->setFlash('message', 'No Data has been received!');
            endif;
        endif;

        $this->redirect(Yii::app()->createAbsoluteUrl('driverEdit/'.$lastInsertId.'#tab_1'));
    }




    /**
     * [actionUploadLicence Action to upload licence]
     * @return [type] [description]
     * @developer : Amit K Sharma
     */
    public function actionUploadLicence() {
        $flag = 0;
        $driver = new Driver;
        $lastInsertId = $driver->findAll(array('order'=>'id DESC', 'limit'=>'1'));
        $driver = $driver->model()->findByPk($lastInsertId[0]->id);

        // getting the image instacnce
        $image1 = CUploadedFile::getInstanceByName('driver_licence_front');
        $image2 = CUploadedFile::getInstanceByName('driver_licence_back');

        $driver->driver_licence_front = '';
        $driver->driver_licence_back = '';

        for($ik = 1; $ik <= 2; $ik++):
            $name = rand(11111, 99999) . date('Ymdhi');
            if($ik == 1):
                $image = $image1;
            elseif($ik == 2):
                $image = $image2;
            endif;

            if (!empty($image)):
                // get theextention of uploaded image
                $extension = strtolower($image->getExtensionName());
                $filename = "{$name}.{$extension}";

                // assigning new image name to the model attribute
                if($ik == 1):
                    $driver->driver_licence_front = $filename;
                elseif($ik == 2):
                    $driver->driver_licence_back = $filename;
                endif;

                // echo Utils::DriverImageBasePath().'driver-'.$lastInsertId[0]->id.'/licence/';
                if (!file_exists(Utils::DriverImageBasePath().'driver-'.$lastInsertId[0]->id.'/licence/')):
                    //creating a directory for driver
                    mkdir(Utils::DriverImageBasePath().'driver-'.$lastInsertId[0]->id.'/licence/', 0777, true);
                endif;
                // saving/moving image to the folder
                if($ik == 1):
                    $image->saveAs(Utils::DriverImageBasePath().'driver-'.$lastInsertId[0]->id.'/licence/'. $driver->driver_licence_front);
                elseif($ik == 2):
                    $image->saveAs(Utils::DriverImageBasePath().'driver-'.$lastInsertId[0]->id.'/licence/'. $driver->driver_licence_back);
                endif;
                $flag = 1;
            endif;
        endfor;

        $driver->dd_licence_no = $_POST['driver_licence_no'];

        if($driver->save()):
            if($flag == 1):
                Yii::app()->user->setFlash('type3', 'success');
                Yii::app()->user->setFlash('message3', 'Licence are saved successfully.');
            else:
                Yii::app()->user->setFlash('type3', 'danger');
                Yii::app()->user->setFlash('message3', 'No images are received.');
            endif;
        else:
            Yii::app()->user->setFlash('type3', 'danger');
            Yii::app()->user->setFlash('message3', 'Some Errors on saving data!');
        endif;
        $this->redirect(Yii::app()->createAbsoluteUrl('driverEdit/'.$lastInsertId.'#tab_3'));
     }

     /**
      * [actionUploadVehicleDocument Action to upload vehilce documents]
      * @return [type] [description]
      * @developer : Amit K Sharma
      */
     public function actionUploadVehicleDocument(){
        $flag = 0;
        $driver = new Driver;
        $lastInsertId = $driver->findAll(array('order'=>'id DESC', 'limit'=>'1'));
        $driver = $driver->model()->findByPk($lastInsertId[0]->id);
        $driver->attributes = $_POST;

        $image1 = CUploadedFile::getInstanceByName('driver_vehicle_registration');
        $image2 = CUploadedFile::getInstanceByName('driver_vehicle_insurance');

        $driver->driver_vehicle_registration = '';
        $driver->driver_vehicle_insurance = '';

        for($ik = 1; $ik <= 2; $ik++):
            $name = rand(11111, 99999) . date('Ymdhi');
            if($ik == 1):
                $image = $image1;
            elseif($ik == 2):
                $image = $image2;
            endif;
            // echo '<pre>';print_r($image);
            if (!empty($image)):
                $extension = strtolower($image->getExtensionName());
                $filename = "{$name}.{$extension}";
                //print_r($filename);die;

                // assigning new image name to the model attribute
                if($ik == 1):
                    $driver->driver_vehicle_registration = $filename;
                elseif($ik == 2):
                    $driver->driver_vehicle_insurance = $filename;
                endif;

                $driver->driver_vehicle_model = $_POST['driver_vehicle_model'];
                $driver->driver_vehicle_maker = $_POST['driver_vehicle_maker'];
                $driver->driver_vehicle_year  = $_POST['driver_vehicle_year'];
                $driver->dd_vehicle_seat      = $_POST['dd_vehicle_seat'];

              // echo Utils::DriverImageBasePath().'driver-'.$lastInsertId[0]->id.'/licence/';
                if (!file_exists(Utils::DriverImageBasePath().'driver-'.$lastInsertId[0]->id.'/vehicle/')):
                    //creating a directory for driver
                    mkdir(Utils::DriverImageBasePath().'driver-'.$lastInsertId[0]->id.'/vehicle/', 0777, true);
                endif;
                // saving/moving image to the folder
                if($ik == 1):
                    $image->saveAs(Utils::DriverImageBasePath().'driver-'.$lastInsertId[0]->id.'/vehicle/'. $driver->driver_vehicle_registration);
                elseif($ik == 2):
                    $image->saveAs(Utils::DriverImageBasePath().'driver-'.$lastInsertId[0]->id.'/vehicle/'. $driver->driver_vehicle_insurance);
                endif;
                $flag = 1;
            endif;
        endfor;
        if($driver->save()):
            if($flag == 1):
                Yii::app()->user->setFlash('type4', 'success');
                Yii::app()->user->setFlash('message4', 'Vehicle information saved successfully.');
            else:
                Yii::app()->user->setFlash('type4', 'danger');
                Yii::app()->user->setFlash('message4', 'No data received.');
            endif;
        else:
            Yii::app()->user->setFlash('type4', 'danger');
            Yii::app()->user->setFlash('message4', 'Some Errors on saving data!');
        endif;
        $this->redirect(Yii::app()->createAbsoluteUrl('driverEdit/'.$lastInsertId.'#tab_4'));
     }

     /**
      * [actionDriverInfoEdit Action to edit driver information]
      * @param  [type] $id [driver id]
      * @return [type]     [description]
      * @developer : Amit K Sharma
      */
    public function actionDriverInfoEdit( $id ) {
        $data_array['css_file']  = 'drivers';
        $data_array['title']     = 'Drivers Management';
        $data_array['sub_title'] = '';
        $data_array['js_file']   = 'drivers';
        $data_array['model1']     = Driver::model()->findByPk($id);
        $data_array['model2']     = Driver_detail::model()->findByAttributes(array('dd_driverID' => $id));
        // echo '<pre>';print_r($data_array['model2']);die('not');
        $data_array['countries'] = Country::model()->findAll();
        $this->render('/admin/drivers/driver_edit', $data_array);
    }

    /**
     * [actionUpdateDriver Action to update the driver details/information in to the database]
     * @return [type] [description]
     * @developer : Amit K Sharma
     */
    public function actionUpdateDriver() {
        $post_data =  $_POST;
        $lastInsertId = $post_data['id'];
        if(!empty($post_data)):
            $driver = Driver::model()->findByPk($post_data['id']);
            $driver->attributes = $post_data;
            if($driver->validate()):
                $image = CUploadedFile::getInstanceByName('driver_avatar');
                $name = rand(1111, 9999) . date('Ymdhi');
                
                 // checking image and rename
                if (!empty($image)):

                    // to unlink the present profile pic from the directory
                    unlink(Utils::DriverImageBasePath().'driver-'.$lastInsertId.'/avatar/'.$driver->driver_avatar);

                    $driver->driver_avatar='';
                    $extension = strtolower($image->getExtensionName());
                    $filename = "{$name}.{$extension}";

                    // assigning new image name to the model attribute
                    $driver->driver_avatar = $filename;
                endif;

                // assigning all the attribute
                $driver->driver_fname   = ucwords($post_data['driver_fname']);
                $driver->driver_lname   = ucwords($post_data['driver_lname']);
                $driver->driver_gender  = $post_data['driver_gender'];
                $driver->driver_email   = strtolower($post_data['driver_email']);
                $driver->driver_mobile  = $post_data['driver_mobile'];

                if($driver->save()):
                        // checking for directory is available for driver
                        if (!empty($image)):
                            if (!file_exists(Utils::DriverImageBasePath().'driver-'.$lastInsertId.'/avatar/')):
                                //creating a directory for driver
                                mkdir(Utils::DriverImageBasePath().'driver-'.$lastInsertId.'/avatar/', 0777, true);
                            endif;

                            // saving/moving image to the folder
                            $image->saveAs(Utils::DriverImageBasePath().'driver-'.$lastInsertId.'/avatar/'. $driver->driver_avatar);
                        endif;

                    Yii::app()->user->setFlash('type', 'success');
                    Yii::app()->user->setFlash('message', 'Personal Information Updated Successfully.');
                else:
                    Yii::app()->user->setFlash('type', 'danger');
                    Yii::app()->user->setFlash('message', 'Some Errors are found!');
                endif;
            else:
                    Yii::app()->user->setFlash('type', 'danger');
                    Yii::app()->user->setFlash('message', 'No Data has been received!');
            endif;
        endif;

        $this->redirect(Yii::app()->createAbsoluteUrl('drivers/driverInfoEdit/'.$post_data['id'].'#tab_1'));
    }


    public function actionInsertAddress() {
        $post_data =  $_POST;
        $driver = Driver_detail::model()->findByAttributes(['dd_driverID' => $_POST['id']]);

        $lastInsertId = $_POST['id'];
        // creating the instance of CDbCriteria
        $criteria = new CDbCriteria;
        if(!empty($post_data)):
            // $criteria->limit = 1;
            // $criteria->order = 'driver_id DESC';
            // $data = Driver::model()->findAll($criteria);
            if(!empty($driver)):
                $driver->attributes = $post_data;
            else:
                $driver = new Driver_detail;
                $driver->attributes = $post_data;
            endif;
            // echo '<pre>';print_r($driver->attributes);die;
            if($driver->validate()):
                // assigning all the attribute
                $driver->dd_street   = $post_data['dd_street'];
                $driver->dd_city     = $post_data['dd_city'];
                $driver->dd_state    = $post_data['dd_state'];
                $driver->dd_country  = $post_data['dd_country'];
                $driver->dd_pin      = $post_data['dd_pin'];
                $driver->dd_driverID = $_POST['id'];

                // saving the driver Address info to the database
                if($driver->save()):
                    Yii::app()->user->setFlash('type2', 'success');
                    Yii::app()->user->setFlash('message2', 'Address Saved Successfully.');
                else:
                    Yii::app()->user->setFlash('type2', 'danger');
                    Yii::app()->user->setFlash('message2', 'Some Errors are found!');
                endif;
            else:
                    Yii::app()->user->setFlash('type2', 'danger');
                    Yii::app()->user->setFlash('message2', 'No Data has been received!');
            endif;
        endif;

        $this->redirect(Yii::app()->createAbsoluteUrl('driverEdit/'.$lastInsertId.'#tab_2'));
    }


    /**
     * [actionUpdateLicence Action to update licence documents]
     * @return [type] [description]
     * @developer : Amit K Sharma
     */
    public function actionUpdateLicence() {
        $post_data =  $_POST;
        $flag = 1;
        $driver = Driver_detail::model()->findByAttributes(['dd_driverID' => $_POST['id']]);
        $lastInsertId = $_POST['id'];

        // get the instance of uploading images
        $image1 = CUploadedFile::getInstanceByName('dd_licence_front');
        $image2 = CUploadedFile::getInstanceByName('dd_licence_back');
        // echo '<pre>';print_r($image1);die;

        if(!empty($driver)):
            $driver->attributes = $post_data;
        else:
            $driver = new Driver_detail;
            $driver->attributes = $post_data;
        endif;

        for($ik = 1; $ik <= 2; $ik++):
            $name = rand(11111, 99999) . date('Ymdhi');
            if($ik == 1):
                $image = $image1;
            elseif($ik == 2):
                $image = $image2;
            endif;

            if (!empty($image)):
                $extension = strtolower($image->getExtensionName());
                $filename = "{$name}.{$extension}";

                // assigning new image name to the model attribute
                if($ik == 1):
                    // to unlink the present licence of driver on update
                    if(!empty($driver->dd_licence_front))
                        unlink(Utils::DriverImageBasePath().'driver-'.$lastInsertId.'/licence/'.$driver->dd_licence_front);
                    $driver->dd_licence_front = $filename;
                elseif($ik == 2):
                    // to unlink the present licence of driver on update
                    if(!empty($driver->dd_licence_back))
                        unlink(Utils::DriverImageBasePath().'driver-'.$lastInsertId.'/licence/'.$driver->dd_licence_back);
                    $driver->dd_licence_back = $filename;
                endif;

                $driver->dd_licence_no = $_POST['driver_licence_no'];

                // echo Utils::DriverImageBasePath().'driver-'.$lastInsertId[0]->id.'/licence/';
                if (!file_exists(Utils::DriverImageBasePath().'driver-'.$lastInsertId.'/licence/')):
                    //creating a directory for driver
                    mkdir(Utils::DriverImageBasePath().'driver-'.$lastInsertId.'/licence/', 0777, true);
                endif;

                // saving/moving image to the folder
                if($ik == 1):
                    $image->saveAs(Utils::DriverImageBasePath().'driver-'.$lastInsertId.'/licence/'. $driver->dd_licence_front);
                elseif($ik == 2):
                    $image->saveAs(Utils::DriverImageBasePath().'driver-'.$lastInsertId.'/licence/'. $driver->dd_licence_back);
                endif;
                $flag = 1;
            endif;
        endfor;

        //saving the driver's data to the database
        if($driver->save()):
            if($flag == 1):
                Yii::app()->user->setFlash('type3', 'success');
                Yii::app()->user->setFlash('message3', 'Licence updated successfully.');
            else:
                Yii::app()->user->setFlash('type3', 'danger');
                Yii::app()->user->setFlash('message3', 'No images are received.');
            endif;
        else:
            Yii::app()->user->setFlash('type3', 'danger');
            Yii::app()->user->setFlash('message3', 'Some Errors on saving data!');
        endif;

        $this->redirect(Yii::app()->createAbsoluteUrl('drivers/driverInfoEdit/'.$lastInsertId.'#tab_3'));
     }


     public function actionUpdateAccountDetails(){
        $post_data =  $_POST;
        $flag = 1;
        $driver = Driver_detail::model()->findByAttributes(['dd_driverID' => $_POST['id']]);
        $lastInsertId = $_POST['id'];
        $driver->attributes = $_POST;
        
        if(!empty($driver)):
            $driver->attributes = $post_data;
        else:
            $driver = new Driver_detail;
            $driver->attributes = $post_data;
        endif;
        
        $driver->dd_bankName = $_POST['dd_bankName'];
        $driver->dd_accountNumber  = $_POST['dd_accountNumber'];
            
        if($driver->save()):
            if($flag == 1):
                Yii::app()->user->setFlash('type4', 'success');
                Yii::app()->user->setFlash('message4', 'Account details updated successfully.');
            else:
                Yii::app()->user->setFlash('type4', 'danger');
                Yii::app()->user->setFlash('message4', 'No data received.');
            endif;
        else:
            Yii::app()->user->setFlash('type4', 'danger');
            Yii::app()->user->setFlash('message4', 'Some Errors on saving data!');
        endif;
        $this->redirect(Yii::app()->createAbsoluteUrl('drivers/driverInfoEdit/'.$lastInsertId.'#tab_4'));
     }


     /**
      * [actionUpdateVehicleDocument Action to update vehicle documents Registration and Insurance]
      * @return [type] [description]
      * @developer : Amit K Sharma
      */
     public function actionUpdateVehicleDocument(){
        $post_data =  $_POST;
        $flag = 1;
        $driver = Driver_detail::model()->findByAttributes(['dd_driverID' => $_POST['id']]);
        $lastInsertId = $_POST['id'];
        $driver->attributes = $_POST;
        
        if(!empty($driver)):
            $driver->attributes = $post_data;
        else:
            $driver = new Driver_detail;
            $driver->attributes = $post_data;
        endif;

        // getting the uploaded image instance
        $image1 = CUploadedFile::getInstanceByName('dd_vehicle_registration');
        $image2 = CUploadedFile::getInstanceByName('dd_vehicle_insurance');

        for($ik = 1; $ik <= 2; $ik++):
            $name = rand(11111, 99999) . date('Ymdhi');
            if($ik == 1):
                $image = $image1;
            elseif($ik == 2):
                $image = $image2;
            endif;

            $driver->dd_vehicle_model = $_POST['dd_vehicle_model'];
            $driver->dd_vehicle_make  = $_POST['dd_vehicle_make'];
            $driver->dd_vehicle_year  = $_POST['dd_vehicle_year'];
            $driver->dd_vehicle_seat  = $_POST['dd_vehicle_seat'];

            // echo '<pre>';print_r($image);
            if (!empty($image)):
                $extension = strtolower($image->getExtensionName());
                $filename = "{$name}.{$extension}";
                //print_r($filename);die;

                // assigning new image name to the model attribute
                if($ik == 1):
                    // to unlink the present vehicle registration on update to new
                    if(!empty($driver->dd_vehicle_registration))
                        unlink(Utils::DriverImageBasePath().'driver-'.$lastInsertId.'/vehicle/'.$driver->dd_vehicle_registration);
                    $driver->dd_vehicle_registration = $filename;
                elseif($ik == 2):
                    // to unlink the present vehicle insurance on update to new
                    if(!empty($driver->dd_vehicle_insurance))
                        unlink(Utils::DriverImageBasePath().'driver-'.$lastInsertId.'/vehicle/'.$driver->dd_vehicle_insurance);
                    $driver->dd_vehicle_insurance = $filename;
                endif;

   

              // echo Utils::DriverImageBasePath().'driver-'.$lastInsertId[0]->id.'/licence/';
                if (!file_exists(Utils::DriverImageBasePath().'driver-'.$lastInsertId.'/vehicle/')):
                    //creating a directory for driver
                    mkdir(Utils::DriverImageBasePath().'driver-'.$lastInsertId.'/vehicle/', 0777, true);
                endif;
                // saving/moving image to the folder
                if($ik == 1):
                    $image->saveAs(Utils::DriverImageBasePath().'driver-'.$lastInsertId.'/vehicle/'. $driver->dd_vehicle_registration);
                elseif($ik == 2):
                    $image->saveAs(Utils::DriverImageBasePath().'driver-'.$lastInsertId.'/vehicle/'. $driver->dd_vehicle_insurance);
                endif;
                $flag = 1;
            endif;
        endfor;
        if($driver->save()):
            if($flag == 1):
                Yii::app()->user->setFlash('type5', 'success');
                Yii::app()->user->setFlash('message5', 'Vehicle information updated successfully.');
            else:
                Yii::app()->user->setFlash('type4', 'danger');
                Yii::app()->user->setFlash('message4', 'No data received.');
            endif;
        else:
            Yii::app()->user->setFlash('type5', 'danger');
            Yii::app()->user->setFlash('message5', 'Some Errors on saving data!');
        endif;
        $this->redirect(Yii::app()->createAbsoluteUrl('drivers/driverInfoEdit/'.$lastInsertId.'#tab_5'));
     }

     /**
      * [actionDriverDelete Action to delete the Driver information from the database]
      * @return [type] [description]
      * @developer : Amit K Sharma
      */
     function actionDriverDelete(){
        if(Driver::model()->findByPk($_POST['id'])->delete()):
            echo json_encode(array('errors' => 0));
        else:
            echo json_encode(array('errors' => 1));
        endif;
     }

    /**
     * [actionDriverInfoView Action to show the driver details on view event]
     * @param  [type] $id [Driver ID]
     * @return [type]     [description]
     * @developer : Amit K Sharma
     */
    public function actionDriverInfoView( $id ) {
        $data_array['css_file']  = 'drivers';
        $data_array['title']     = 'Drivers Management';
        $data_array['sub_title'] = '';
        $data_array['js_file']   = 'drivers';
        $data_array['model']     = Driver::model()->findByPk($id);
        $data_array['model2']    = Driver_detail::model()->findByAttributes(array('dd_driverID' => $id));
        $this->render('/admin/drivers/driver_view', $data_array);
    }     

} // end class
