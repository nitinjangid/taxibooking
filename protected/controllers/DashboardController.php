<?php

class DashboardController extends Controller {

    public $layout = '//layouts/column1';
  

    protected function beforeAction($event) {
        if (!isset(Yii::app()->session['admin_data'])):
            $this->redirect(Yii::app()->createAbsoluteUrl('authentication/login'));
        endif;
        return TRUE;
    }
    
    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }


    public function actionError()
    {
        $error = Yii::app()->errorHandler->error;
        if( $error )
        {
            $this -> render( '/admin/error', array( 'message' => $error ) );
        }
    }


    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {    	
        if (isset(Yii::app()->session['admin_data']) && !empty(Yii::app()->session['admin_data'])):
            $data_array['css_file']     = 'dashboard';
            $data_array['title']        = 'Dashboard';
            $data_array['sub_title']    = '';
            $data_array['js_file']      = 'dashboard';
            $this->render('//admin/dashboard/dashboard', $data_array);
        else:
            $this->redirect(Yii::app()->createAbsoluteUrl('authentication/login'));
        endif;
    }

    /**
     * [actionProfile Used to render the profile view]
     * @return [type] [description]
     */
    public function actionProfile() {
        if (isset(Yii::app()->session['admin_data']['admin_id']) && !empty(Yii::app()->session['admin_data']['admin_id'])):
            $admin = Admin::getProfile();
            $id = Yii::app()->session['admin_data']['admin_id'];
            $model = Admin::model()->findByPk($id);
            $this->render('//admin/profile/profile', array('adminData' => $admin, 'model' => $model));
        endif;
    }

    /**
     * [actionUpdateprofile Used to get the submitted input from the from prefomr validations and operations and save to the database]
     * @return [type] [description]
     */
    public function actionUpdateprofile() {
        if (isset(Yii::app()->session['admin_data'])):
            $id = Yii::app()->session['admin_data']['admin_id'];
            $model = Admin::model()->findByPk($id);
            $this->performAjaxValidation($model);
            $model->attributes = $_POST;

            // checking input from from
            if (isset($_POST)):
                $model->attributes = $_POST;

                // validate input fields of the form
                if($model->validate()):

                    // on update profile info
                    if (isset($_POST['btnSaveProfile'])):
                        $model->admin_name = $_POST['admin_name'];
                        if ($model->save()):
                            Yii::app()->user->setFlash('type', 'success');
                            Yii::app()->user->setFlash('message', 'Personal Information updated successfully.');
                        else:
                            Yii::app()->user->setFlash('type', 'danger');
                            Yii::app()->user->setFlash('message', 'Operation Failed due to lack of connectivity.');
                        endif;
                    endif;

                    // on update profile image
                    if (isset($_POST['btnSaveProfilePicture'])):
                        $admin_image = CUploadedFile::getInstance($model, 'admin_image');
                        $random_name = rand(1111, 9999) . date('Ymdhi');

                        // checking image and rename
                        if (!empty($admin_image)):
                            $extension = strtolower($admin_image->getExtensionName());
                            $filename = "{$random_name}.{$extension}";

                            // assigning new image name tp the model attribute
                            $model->admin_image = $filename;

                            // saving/moving image to the folder
                            $admin_image->saveAs(Utils::UserImageBasePath() . $filename);
                        endif;

                        // saving image data to database
                        if ($model->save()):
                            // Yii::app()->session['admin_data']['admin_image'] = $model->admin_image;
                            Yii::app()->user->setFlash('type', 'success');
                            Yii::app()->user->setFlash('message', 'Profile photo updated successfully.');
                        else:
                            Yii::app()->user->setFlash('type', 'danger');
                            Yii::app()->user->setFlash('message', 'Operation Failed due to lack of connectivity.');
                        endif;
                    endif;

                    // on update profile password
                    if (isset($_POST['btnSavePassword'])):

                        // validationg the input 
                        if(!empty($_POST['old_password']) && !empty($_POST['new_password']) && !empty($_POST['new_password_again'])):

                            // compairing the new password and confirm password
                            if( $_POST['new_password'] == $_POST['new_password_again']):

                                // decrypting the password
                                $current_password_db = Utils::passwordDecrypt($model->admin_password);
                                $current_password_post = $_POST['old_password'];

                                // matching the current password from database and current password from input
                                if($current_password_db == $current_password_post):
                                    $model->admin_password = Utils::passwordEncrypt($_POST['new_password']);
                                    if ($model->save()):
                                        Yii::app()->user->setFlash('type', 'success');
                                        Yii::app()->user->setFlash('message', 'Password changed successfully.');
                                    else:
                                        Yii::app()->user->setFlash('type', 'danger');
                                        Yii::app()->user->setFlash('message', 'Operation Failed due to lack of connectivity.');
                                    endif;
                                else:
                                    Yii::app()->user->setFlash('type', 'danger');
                                    Yii::app()->user->setFlash('message', 'Current password is incorrect.');
                                endif;
                            else:
                                Yii::app()->user->setFlash('type', 'danger');
                                Yii::app()->user->setFlash('message', 'Passwords and Confirm password not matched.');
                            endif;
                        else:
                            Yii::app()->user->setFlash('type', 'danger');
                            Yii::app()->user->setFlash('message', 'Passwords fields can not be empty.');
                        endif;
                    endif;
                else:
                    $admin = Admin::getProfile();
                    $this->render('//admin/profile/profile', array('adminData' => $admin, 'model' => $model));
                endif;
            endif;
      
            $this->redirect(Yii::app()->createAbsoluteUrl('profile'));
        else:
            $this->redirect(Yii::app()->createAbsoluteUrl('authentication/login'));      
        endif;
    }

    public function actionChangePassword() {
        if (isset(Yii::app()->session['admin_data'])):
            $model = new User;
            $this->render('changepassword', array('model' => $model));
        else:
            $this->redirect(Yii::app()->createAbsoluteUrl('authentication/login'));
        endif;
    }


    /**
     * Performs the AJAX validation.
     * @param Appinfo $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'appinfo-form'):
            echo CActiveForm::validate($model);
            Yii::app()->end();
        endif;
    }

}
