<?php

class WebserviceController extends Controller {

    public $layout = false;

    /*
     * Error Function
     */

    public function actionAccounts() {
        $this->render('accounts');
    }

    public function actionError() {
        $error = array('error' => "true");
        $this->response(401, $error);
    }

    /*
     * User Registration 
     * */

    public function actionRegister() {
        $post_data = $_POST;
        if (!empty($post_data)) {
            $user = User::model()->findByAttributes(array('user_email' => strtolower($post_data['email'])));
            if (empty($user)) {
                $user = new User;
                $image = CUploadedFile::getInstanceByName('avatar');
                $name = rand(1111, 9999) . date('Ymdhi');
                $user->user_avatar = '';
                // checking image and rename
                if (!empty($image)):
                    $extension = strtolower($image->getExtensionName());
                    $filename = "{$name}.{$extension}";
                    // assigning new image name tp the model attribute
                    $user->user_avatar = $filename;
                    // saving/moving image to the folder
                    $image->saveAs(Yii::app()->params['profileSetPath'] . $filename);
                endif;
                $user->user_fname = ucwords($post_data['first_name']);
                $user->user_lname = ucwords($post_data['last_name']);
                $user->user_email = strtolower($post_data['email']);
                $user->user_mobile = $post_data['mobile'];
                $user->user_udid = $post_data['udid'];
                $user->user_platform = $post_data['platform'];
                $user->user_pwd = base64_encode($post_data['password']);
                if ($user->save()) {
                    $card = new Card;
                    $card = Card::model()->findByAttributes(array('card_userID' => $user->user_id));
                    $card_status = 'NOTVERIFIED';
                    if (!empty($card)) {
                        $card_status = 'VERIFIED';
                    }
                    $sql = 'SELECT  booking_id,IF(rating_rate != "",rating_rate,"") rating    FROM ur_request INNER JOIN ur_booking ON ur_booking.booking_requestID = ur_request.req_id  LEFT JOIN ur_rating ON ur_rating.rating_bookingID=ur_booking.booking_id where req_userID = ' . $user->user_id . ' ORDER BY booking_id DESC';
                    $connection = Yii::app()->db;
                    $command = $connection->createCommand($sql);
                    $booking = $command->queryRow();
                    $rating = 'NOTRATED';
                    $booking_id = "";
                    if (!empty($booking)) {
                        $booking_id = $booking['booking_id'];
                        if (!empty($booking['rating'])) {
                            $rating = 'RATED';
                        }
                    }
                    $result = array(
                        'user_id' => $user->user_id,
                        'user_fname' => $user->user_fname,
                        'user_lname' => $user->user_lname,
                        'user_email' => $user->user_email,
                        'secret' => $user->user_secret,
                        'rating' => $rating,
                        'booking_id' => $booking_id,
                        'card' => $card_status,
                        'user_avatar' => ($user->user_avatar != "") ? Yii::app()->params['profileGetPath'] . $user->user_avatar : "",
                        'user_mobile' => $user->user_mobile
                    );
                    $this->response(200, $arrayName = array('status' => TRUE, 'data' => $result, 'response' => 'User has been registered successfully.'));
                } else {
                    $error = $user->getErrors();
                    $key = array_keys($error);
                    $this->response(200, $arrayName = array('status' => FALSE, 'response' => $error[$key[0]][0]));
                }
            } else {
                $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'Email already exist.'));
            }
        } else {
            $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'No any data received.'));
        }
    }

    /*
     * User Login Function
     */

    public function actionLogin() {
        $post_data = $_POST;
        if (!empty($post_data)) {
            //$user = new User;
            $user = User::model()->findByAttributes(array('user_email' => strtolower($post_data['email']), 'user_status' => 1));
            if (!empty($user)) {
                if (base64_decode($user->user_pwd) == $post_data['password']) {
                    $user->user_platform = $post_data['platform'];
                    $user->user_udid = $post_data['udid'];
                    $user->save();
                    $card = new Card;
                    $card = Card::model()->findByAttributes(array('card_userID' => $user->user_id));
                    $card_status = 'NOTVERIFIED';
                    if (!empty($card)) {
                        $card_status = 'VERIFIED';
                    }
                    $sql = 'SELECT  booking_id,IF(rating_rate != "",rating_rate,"") rating    FROM ur_request INNER JOIN ur_booking ON ur_booking.booking_requestID = ur_request.req_id  LEFT JOIN ur_rating ON ur_rating.rating_bookingID=ur_booking.booking_id where req_userID = ' . $user->user_id . ' ORDER BY booking_id DESC';
                    $connection = Yii::app()->db;
                    $command = $connection->createCommand($sql);
                    $booking = $command->queryRow();
                    $rating = 'NOTRATED';
                    $booking_id = "";
                    //print_r($booking);die;
                    if (!empty($booking)) {
                        $booking_id = $booking['booking_id'];
                        if (!empty($booking['rating'])) {
                            $rating = 'RATED';
                        }
                    }
                    $result = array(
                        'user_id' => $user->user_id,
                        'user_fname' => $user->user_fname,
                        'user_lname' => $user->user_lname,
                        'user_email' => $user->user_email,
                        'secret' => $user->user_secret,
                        'rating' => $rating,
                        'booking_id' => $booking_id,
                        'card' => $card_status,
                        'user_avatar' => ($user->user_avatar != "") ? Yii::app()->params['profileGetPath'] . $user->user_avatar : "",
                        'user_mobile' => $user->user_mobile
                    );
                    $this->response(200, $arrayName = array('status' => TRUE, 'data' => $result, 'response' => 'User logged in.'));
                } else {
                    $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'Incorrect password.'));
                }
            } else {
                $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'No any user found.'));
            }
        } else {
            $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'No any data received.'));
        }
    }

    /*
     * Driver Login Function
     */

    public function actionDriverLogin() {
        $post_data = $_POST;
        if (!empty($post_data)) {
            //$user = new User;
            $driver = Driver::model()->findByAttributes(array('driver_email' => strtolower($post_data['email']), 'driver_status' => 1));
            if (!empty($driver)) {
                if (base64_decode($driver->driver_password) == $post_data['password']) {
                    $driver->driver_platform = $post_data['platform'];
                    $driver->driver_udid = $post_data['udid'];
                    $driver->driver_mode = 1;
                    $driver->save();
                    $result = array(
                        'driver_id' => $driver->driver_id,
                        'driver_fname' => $driver->driver_fname,
                        'driver_lname' => $driver->driver_lname,
                        'driver_email' => $driver->driver_email,
                        'driver_mode' => strval($driver->driver_mode),
                        //'secret' => $user->user_secret,
                        'avatar' => ($driver['driver_avatar'] != "") ? str_replace('-id', '-' . $driver['driver_id'], Yii::app()->params['driverProfileGetPath'] . $driver['driver_avatar']) : "",
                        'driver_mobile' => $driver->driver_mobile
                    );
                    $this->response(200, $arrayName = array('status' => TRUE, 'data' => $result, 'response' => 'Driver logged in.'));
                } else {
                    $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'Incorrect password.'));
                }
            } else {
                $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'No any user found.'));
            }
        } else {
            $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'No any data received.'));
        }
    }

    /*
     * User Forgot Password Function
     */

    public function actionForgotpassword() {
        $post_data = $_POST;
        if (empty($post_data['email'])) {
            $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'Email empty.'));
        } else {
            $email = $post_data['email'];
            $utils = new Utils;
            $user = User::model()->findByAttributes(array
                ('user_email' => strtolower($email)));
            if (!empty($user)) {
                $password = base64_decode($user->user_pwd);
                $email = $user->user_email;
                $user_name = ucwords($user->user_fname . ' ' . $user->user_lname);
                $to = $email;
                //echo Yii::app()->baseUrl;die;
                $userdata['name'] = $user_name;
                $userdata['email'] = $email;
                $userdata['password'] = $password;
               // echo Yii::app()->params['image_path'];die;
                
                //$this->render("/template/forgot_password");
                    
                $subject = '=?UTF-8?B?' . base64_encode("Forgot Password") . '?=';
                //$data_array=array();
                $msg=$this->renderPartial("/template/forgot_password",array('email'=>$email,'password'=>$password),true,false);
                
                if ($utils->Send($to, $user_name, $subject, $msg)) {
                    $this->response(200, $arrayName = array('status' => TRUE, 'response' => 'Password sent.'));
                } else {
                    $this->response(200, $arrayName = array('status' => TRUE, 'response' => 'Mail not sent.'));
                }
            } else {
                $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'Invalid email.'));
            }
        }
    }

    /*
     * User Forgot Password Function
     */

    public function actionDriverForgotpassword() {
        $post_data = $_POST;
        if (empty($post_data['email'])) {
            $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'Email empty.'));
        } else {
            $email = $post_data['email'];
            $utils = new Utils;
            $driver = Driver::model()->findByAttributes(array
                ('driver_email' => strtolower($email)));
            if (!empty($driver)) {
                $password = base64_decode($driver->driver_password);
                $email = $driver->driver_email;
                $driver_name = ucwords($driver->driver_fname . ' ' . $driver->driver_lname);
                $to = $email;
                //$userdata['href'] = Yii::app()->params['site_url'];
                $userdata['name'] = $driver_name;
                $userdata['email'] = $email;
                $userdata['password'] = $password;
                //$userdata['sitename'] = Yii::app()->params['site_url'];
                $subject = '=?UTF-8?B?' . base64_encode("Forgot Password") . '?=';
                //$message = file_get_contents('./bootstrap/mailtemplate/ForgotPassword.php');
                //$msg = $utils->replace($userdata, $message);
                $msg = ' Hi  ' . $driver_name . '<br>Your password is <b> ' . $password . ' </b>';
                if ($utils->Send($to, $driver_name, $subject, $msg)) {
                    $this->response(200, $arrayName = array('status' => TRUE, 'response' => 'Password sent.'));
                } else {
                    $this->response(200, $arrayName = array('status' => TRUE, 'response' => 'Mail not sent.'));
                }
            } else {
                $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'Invalid email.'));
            }
        }
    }

    public function actionShowProfile() {
        $post_data = $_POST;
        if (empty($post_data['user_id'])) {
            $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'invalid data.'));
        } else {
            $user_id = $post_data['user_id'];
            $user = User::model()->findByPk($user_id);
            if (empty($user)) {
                $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'user_id not exist.'));
            } else {
                $user = array(
                    'user_id' => $user->user_id,
                    'first_name' => $user->user_fname,
                    'last_name' => $user->user_lname,
                    'email' => $user->user_email,
                    'mobile' => $user->user_mobile,
                    'avatar' => ($user->user_avatar != "") ? Yii::app()->params['profileGetPath'] . $user->user_avatar : ""
                );
                $this->response(200, $arrayName = array('status' => TRUE, 'data' => $user, 'response' => 'success'));
            }
        }
    }

    public function actionDriverShowProfile() {
        $post_data = $_POST;
        if (empty($post_data['driver_id'])) {
            $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'invalid data.'));
        } else {
            $driver_id = $post_data['driver_id'];
            $sql = 'SELECT  ur_driver.*,IF(dd_vehicle_no !="",dd_vehicle_no,"") driver_vehicle_no    FROM ur_driver  LEFT JOIN ur_driver_detail ON ur_driver_detail.dd_driverID=ur_driver.driver_id where driver_id = ' . $driver_id;
            $connection = Yii::app()->db;
            $command = $connection->createCommand($sql);
            $driver = $command->queryRow();
            // print_r($driver);die;
            // $driver = Driver::model()->findByPk($driver_id);
            if (empty($driver)) {
                $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'driver_id not exist.'));
            } else {
                $driver = array(
                    'driver_id' => $driver['driver_id'],
                    'first_name' => $driver['driver_fname'],
                    'last_name' => $driver['driver_lname'],
                    'email' => $driver['driver_email'],
                    'mobile' => $driver['driver_mobile'],
                    'vehicle_no' => $driver['driver_vehicle_no'],
                    'avatar' => ($driver['driver_avatar'] != "") ? str_replace('-id', '-' . $driver['driver_id'], Yii::app()->params['driverProfileGetPath'] . $driver['driver_avatar']) : ""
                );
                $this->response(200, $arrayName = array('status' => TRUE, 'data' => $driver, 'response' => 'success'));
            }
        }
    }

    public function actionEditProfile() {
        $post_data = $_POST;
        if (empty($_POST['user_id'])) {
            $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'invalid data.'));
        } else {
            $user_id = $post_data['user_id'];
            $user = User::model()->findByPk($user_id);
            if (empty($user)) {
                $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'no any user found.'));
            } else {
                //print_r($_FILES);
                $image = CUploadedFile::getInstanceByName('avatar');
                //print_r($admin_image);die;
                $name = rand(1111, 9999) . date('Ymdhi');
                $user->user_avatar = '';
                // checking image and rename
                if (!empty($image)):
                    $extension = strtolower($image->getExtensionName());
                    $filename = "{$name}.{$extension}";
                    //print_r($filename);die;
                    // assigning new image name tp the model attribute
                    $user->user_avatar = $filename;
                    //echo Yii::app()->basePath.'/../upload/user/';die;
                    // echo Yii::app()->params['profileSetPath'];die;
                    // saving/moving image to the folder
                    $image->saveAs(Yii::app()->params['profileSetPath'] . $filename);
                endif;
                if ($user->user_secret == 'VERIFIED') {
                    if ($user->user_mobile != $post_data['mobile']) {
                        $user->user_secret = 'NOTVERIFIED';
                    }
                    //$user->user_mobile = $post_data['mobile'];
                }
                $user->user_fname = $post_data['first_name'];
                $user->user_lname = $post_data['last_name'];
                $user->user_email = $post_data['email'];
                $user->user_platform = $post_data['platform'];
                if ($user->update()) {
                    $this->response(200, $arrayName = array('status' => TRUE, 'data' => array('secret' => $user->user_secret), 'response' => 'Profile updated.'));
                } else {
                    $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'Profile update fail.'));
                }
            }
        }
    }

    /*
     * User Reset Password Function
     */

    public function actionResetPassword() {
        $post_data = $_POST;
        if (empty($post_data)) {
            $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'Missing data'));
        } else {
            $user_id = $post_data['user_id'];
            $old_pwd = $post_data['old_password'];
            $new_pwd = $post_data['new_password'];
            $user = User::model()->findByAttributes(array(
                'user_id' => $user_id));
            if (empty($user)) {
                $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'User not found'));
            } else {
                if ($old_pwd != base64_decode($user->user_pwd)) {
                    $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'Incorrect password'));
                } else {
                    $user->user_pwd = base64_encode($new_pwd);
                    if ($user->update()) {
                        $this->response(200, $arrayName = array('status' => TRUE, 'response' => 'Password Updated'));
                    } else {
                        $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'Password not updated'));
                    }
                }
            }
        }
    }

    public function actionFbCheck() {
        $post_data = $_POST;
        if (empty($post_data['fb_id'])) {
            $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'fb_id missing.'));
        } else {
            $user = User::model()->findByAttributes(array('user_fbid' => ($post_data['fb_id'])));
            if (empty($user)) {
                $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'fb_id not register.'));
            } else {
                $user->user_platform = $post_data['platform'];
                $user->user_udid = $post_data['udid'];
                if ($user->update()) {
                    $this->response(200, $arrayName = array('status' => TRUE, 'data' => array('user_id' => $user->user_id), 'response' => 'fb_id register.'));
                } else {
                    $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'data missing.'));
                }
            }
        }
    }

    /*
     * User Registration 
     * */

    public function actionFbConnect() {
        $post_data = $_POST;
        $posts = array();
        if (empty($post_data)) {
            $posts['status'] = FALSE;
            $posts['response'] = 'Post data missing.';
            //$this->response(200, $arrayName = array('status' => FALSE, 'response' => 'Post data missing.'));
        } else {
            $user = User::model()->findByAttributes(array('user_fbid' => ($post_data['fb_id']), 'user_email' => $post_data['email']));
            $image = CUploadedFile::getInstanceByName('avatar');
            $name = rand(1111, 9999) . date('Ymdhi');
            $filename = '';
            // checking image and rename
            if (!empty($image)):
                $extension = strtolower($image->getExtensionName());
                $filename = "{$name}.{$extension}";
                $image->saveAs(Yii::app()->params['profileSetPath'] . $filename);
            endif;
            if (empty($user)) {
                $user = new User;
                $user->user_fname = ucwords($post_data['first_name']);
                $user->user_fbid = ucwords($post_data['fb_id']);
                $user->user_lname = ucwords($post_data['last_name']);
                $user->user_email = strtolower($post_data['email']);
                $user->user_mobile = $post_data['mobile'];
                $user->user_avatar = $filename;
                $user->user_udid = $post_data['udid'];
                $user->user_platform = $post_data['platform'];
                if ($user->save()) {
                    $result = array(
                        'user_id' => $user->user_id,
                        'user_fname' => $user->user_fname,
                        'user_lname' => $user->user_lname,
                        'user_email' => $user->user_email,
                        'secret' => $user->user_secret,
                        'user_avatar' => ($user->user_avatar != "") ? Yii::app()->params['profileGetPath'] . $user->user_avatar : "",
                        'user_mobile' => $user->user_mobile
                    );
                    $posts['status'] = TRUE;
                    $posts['data'] = $result;
                    $posts['response'] = 'User registerd from facebook.';
                    //$this->response(200, $arrayName = array('status' => TRUE, 'data' => $result, 'response' => 'User registerd from facebook.'));
                } else {
                    $posts['status'] = FALSE;
                    $posts['response'] = 'User not registerd from facebook.';
                    //$this->response(200, $arrayName = array('status' => FALSE, 'response' => 'User not registerd from facebook.'));
                }
            } else {
                $user->user_fname = ucwords($post_data['first_name']);
                $user->user_fbid = ucwords($post_data['fb_id']);
                $user->user_lname = ucwords($post_data['last_name']);
                $user->user_email = strtolower($post_data['email']);
                //$user->user_mobile = $post_data['mobile'];
                $user->user_avatar = $filename;
                $user->user_udid = $post_data['udid'];
                $user->user_platform = $post_data['platform'];
                $result = array(
                    'user_id' => $user->user_id,
                    'user_fname' => $user->user_fname,
                    'user_lname' => $user->user_lname,
                    'user_email' => $user->user_email,
                    'secret' => $user->user_secret,
                    'user_avatar' => ($user->user_avatar != "") ? Yii::app()->params['profileGetPath'] . $user->user_avatar : "",
                    'user_mobile' => $user->user_mobile
                );
                if ($user->update()) {
                    $posts['status'] = TRUE;
                    $posts['data'] = $result;
                    $posts['response'] = 'User login from facebook.';
                    //$this->response(200, $arrayName = array('status' => TRUE, 'data' => $result, 'response' => 'User login from facebook.'));
                } else {
                    $posts['status'] = FALSE;
                    $posts['response'] = 'User not login from facebook.';
                    //$this->response(200, $arrayName = array('status' => FALSE, 'response' => 'User not login from facebook.'));
                }
            }
            $card = new Card;
            $card = Card::model()->findByAttributes(array('card_userID' => $user->user_id));
            $card_status = 'NOTVERIFIED';
            if (!empty($card)) {
                $card_status = 'VERIFIED';
            }
            $sql = 'SELECT  booking_id,IF(rating_rate != "",rating_rate,"") rating    FROM ur_request INNER JOIN ur_booking ON ur_booking.booking_requestID = ur_request.req_id  LEFT JOIN ur_rating ON ur_rating.rating_bookingID=ur_booking.booking_id where req_userID = ' . $user->user_id . ' ORDER BY booking_id DESC';
            $connection = Yii::app()->db;
            $command = $connection->createCommand($sql);
            $booking = $command->queryRow();
            $rating = 'NOTRATED';
            $booking_id = "";
            //print_r($booking);die;
            if (!empty($booking)) {
                $booking_id = $booking['booking_id'];
                if (!empty($booking['rating'])) {
                    $rating = 'RATED';
                }
            }
            $posts['data']['rating'] = $rating;
            $posts['data']['booking_id'] = $booking_id;
            $posts['data']['card'] = $card_status;
        }
        $this->response(200, $arrayName = $posts);
    }

    /*
     * Generate a Secret
     * */

    public function actionGenerateSecret() {
        $post_data = $_POST;
        if (!empty($post_data)) {
            //$user = new User;
            $user = User::model()->findByAttributes(array('user_email' => strtolower($post_data['email']), 'user_status' => 1));
            if (empty($user)) {
                $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'No any user found.'));
            } else {
                $utils = new Utils;
                $secret = rand(11111, 99999);
                $email = $user->user_email;
                $user_name = ucwords($user->user_fname . ' ' . $user->user_lname);
                $to = $email;
                $userdata['name'] = $user_name;
                $userdata['email'] = $email;
                $subject = '=?UTF-8?B?' . base64_encode("OTP Verification") . '?=';
                $msg = ' Hi  ' . $user_name . '<br>Your otp is <b> ' . $secret . ' </b>';
                if ($utils->Send($to, $user_name, $subject, $msg)) {
                    $user->user_secret = $secret;
                    if ($user->update()) {
                        $this->response(200, $arrayName = array('status' => TRUE, 'response' => 'OTP sent.'));
                    } else {
                        $this->response(200, $arrayName = array('status' => TRUE, 'response' => 'Mail not sent.'));
                    }
                } else {
                    $this->response(200, $arrayName = array('status' => TRUE, 'response' => 'Mail not sent.'));
                }
            }
        } else {
            $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'No any data received.'));
        }
    }

    /*
     * Validate a Secret function
     */

    public function actionValidateSecret() {
        $post_data = $_POST;
        if (empty($post_data)) {
            $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'No any data received.'));
        } else {
            $secret = $post_data['secret'];
            $mobile = $post_data['mobile_no'];
            $user = User::model()->findByAttributes(array('user_secret' => $post_data['secret'], 'user_status' => 1));
            if (empty($user)) {
                $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'No any user found.'));
            } else {
                if ($user->user_secret != $secret) {
                    $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'OTP is invalid.'));
                } else {
                    $user->user_secret = 'VERIFIED';
                    $user->user_mobile = $mobile;
                    if ($user->update()) {
                        $this->response(200, $arrayName = array('status' => TRUE, 'response' => 'OTP verified.'));
                    } else {
                        $this->response(200, $arrayName = array('status' => TRUE, 'response' => 'OTP not sent.'));
                    }
                }
            }
        }
        if (!empty($post_data)) {
            //$user = new User;
            $user = User::model()->findByAttributes(array('user_email' => strtolower($post_data['email']), 'user_status' => 1));
            if (empty($user)) {
                $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'No any user found.'));
            } else {
                $utils = new Utils;
                $secret = rand(11111, 99999);
                $email = $user->user_email;
                $user_name = ucwords($user->user_fname . ' ' . $user->user_lname);
                $to = $email;
                $userdata['name'] = $user_name;
                $userdata['email'] = $email;
                $subject = '=?UTF-8?B?' . base64_encode("OTP Verification") . '?=';
                $msg = ' Hi  ' . $user_name . '<br>Your otp is <b> ' . $secret . ' </b>';
                if ($utils->Send($to, $user_name, $subject, $msg)) {
                    $user->user_secret = $secret;
                    if ($user->update()) {
                        $this->response(200, $arrayName = array('status' => TRUE, 'response' => 'OTP sent.'));
                    } else {
                        $this->response(200, $arrayName = array('status' => TRUE, 'response' => 'Mail not sent.'));
                    }
                } else {
                    $this->response(200, $arrayName = array('status' => TRUE, 'response' => 'Mail not sent.'));
                }
            }
        } else {
            $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'No any data received.'));
        }
    }

    /*
     * Fare calculation
     */

    public function actionFareCalculation() {
        $post_data = $_POST;
        if (empty($post_data)) {
            $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'No any data received.'));
        } else {
            $time = $post_data['time'];
            $distance = $post_data['distance'];
            $fare = Fare::model()->findByAttributes(array('fare_id' => 1));
            $per_time = $fare->fare_time;
            $per_distance = $fare->fare_distance;
            $base_fare = $fare->fare_base;
            $calculate_fare = ($per_time * $time) + ($per_distance * $distance);
            if ($calculate_fare < $base_fare) {
                $fare = $base_fare;
            } else {
                $fare = $calculate_fare;
            }
            $this->response(200, $arrayName = array('status' => TRUE, 'data' => array('base_fare'=>$base_fare,'distance'=>$distance,'per_distance'=>$per_distance,'distance_fare'=>($per_distance * $distance),'time'=>$time,'per_time'=>$per_time,'time_fare'=>($per_time * $time),'fare' => strval($fare)), 'response' => 'success'));
        }
    }

    public function actionGetNearestDriver() {
        $post_data = $_POST;
        if (empty($post_data)) {
            $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'No any data received.'));
        } else {
            $latitude = $post_data['latitude'];
            $longitude = $post_data['longitude'];
            $sql = "SELECT 111.1111 *
    DEGREES(ACOS(COS(RADIANS(" . $latitude . "))
         * COS(RADIANS(ur_driver.driver_lat))
         * COS(RADIANS(" . $longitude . " - ur_driver.driver_long))
         + SIN(RADIANS(" . $latitude . "))
         * SIN(RADIANS(ur_driver.driver_lat)))) AS distance,CONCAT(driver_fname,' ',driver_lname) name,driver_vehicle_no vehicle_no,driver_vehicle_model vehicle_model,IF(driver_status = 2,2,3) rating  FROM ur_driver HAVING distance<20 LIMIT 0,1";
            $connection = Yii::app()->db;
            $command = $connection->createCommand($sql);
            $driver = $command->queryAll();
            if (empty($driver)) {
                $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'No any driver found'));
            } else {
                $this->response(200, $arrayName = array('status' => FALSE, 'data' => $driver, 'response' => 'success'));
            }
        }
    }

    public function actionAddCard() {
        $post_data = $_POST;
        if (empty($post_data)) {
            $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'No any data received.'));
        } else {
            $data = json_decode($post_data['data']);
            $card = Card::model()->findByAttributes(array('card_userID' => $post_data['user_id'], 'card_token' => $data->id, 'card_number' => $data->number));
            if (empty($card)) {
                $card = new Card;
                $card->card_token = $data->id;
                $card->card_userID = $post_data['user_id'];
                $card->car_create_time = $data->create_time;
                $card->car_expire_month = $data->expire_month;
                $card->car_expire_year = $data->expire_year;
                $card->card_first_name = $data->first_name;
                $card->card_number = $data->number;
                $card->card_state = $data->state;
                $card->card_type = $data->type;
                $card->card_update_time = $data->update_time;
                $card->card_valid_until = $data->valid_until;
                //print_r($card);die;
                //$user->user_pwd = base64_encode('123456');
                if ($card->save()) {
                    $this->response(200, $arrayName = array('status' => TRUE, 'response' => 'Card added.'));
                } else {
                    //print_r($card->getErrors());
                    $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'Card not added.'));
                }
            } else {
                $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'Card already exist.'));
            }
        }
    }

    public function actionGetCard() {
        $post_data = $_POST;
        //$user_id= $post_data['user_id'];
        if (empty($post_data)) {
            $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'No any data received.'));
        } else {
            $cards = Card::model()->findAllByAttributes(array('card_userID' => $post_data['user_id']), array('select' => 'card_id,card_number,card_type'));
            //$card = Card::model()->findAllByAttributes(array('card_userID'=>1));            
            if (empty($cards)) {
                $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'No any card exist.'));
            } else {
                $result = array();
                $i = 0;
                foreach ($cards as $card) {
                    $result[$i]['card_id'] = $card->card_id;
                    $result[$i]['card_number'] = $card->card_number;
                    $result[$i]['card_type'] = $card->card_type;
                    $i++;
                }
                $this->response(200, $arrayName = array('status' => TRUE, 'data' => $result, 'response' => 'success'));
            }
        }
    }

    public function actionSetDriverMode() {
        $post_data = $_POST;
        //$user_id= $post_data['user_id'];
        if (empty($post_data)) {
            $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'No any data received.'));
        } else {
            $driver = Driver::model()->findByAttributes(array('driver_id' => $post_data['driver_id']));
            //$card = Card::model()->findAllByAttributes(array('card_userID'=>1));            
            if (empty($driver)) {
                $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'No any driver exist.'));
            } else {
                $driver->driver_platform = $post_data['platform'];
                $driver->driver_mode = $post_data['mode'];
                if ($driver->update()) {
                    $this->response(200, $arrayName = array('status' => TRUE, 'data' => array('mode' => $post_data['mode']), 'response' => 'Mode successfully changed.'));
                } else {
                    $this->response(200, $arrayName = array('status' => TRUE, 'response' => 'Mode not change.'));
                }
                //$user->user_udid = $post_data['udid'];
                //$user->save();                
            }
        }
    }

    public function actionDeleteCard() {
        $post_data = $_POST;
        if (empty($post_data)) {
            $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'No any data received.'));
        } else {
            $count = Card::model()->countByAttributes(array('card_userID' => $post_data['user_id']));
            if ($count > 1) {
                $deleteThis = Card::model()->deleteAllByAttributes(array('card_userID' => $post_data['user_id'], 'card_id' => $post_data['card_id']));
                if (empty($deleteThis)) {
                    $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'card not deleted.'));
                } else {
                    $this->response(200, $arrayName = array('status' => TRUE, 'response' => 'card successfully deleted.'));
                }
            } else {
                $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'you can not delete this card.'));
            }
        }
    }

    public function actionGetBookingDetail() {
        $post_data = $_POST;
        if (empty($post_data['booking_id'])) {
            $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'invalid data.'));
        } else {
            $booking_id = $post_data['booking_id'];
            $sql = 'SELECT  booking_id,booking_image,req_src_adrs source,req_des_adrs destination,req_distance distance,req_time time,booking_created created,user_id,CONCAT(user_fname," ",user_lname) user_name,user_mobile,user_email,user_avatar,driver_id,CONCAT(driver_fname," ",driver_lname) driver_name,driver_email,driver_mobile,IF(dd_vehicle_no !="",dd_vehicle_no,"") vehicle_no,IF(dd_vehicle_model !="",dd_vehicle_model,"") vehicle_model,IF(rating_rate != "",rating_rate,0) rating,driver_avatar FROM ur_booking  INNER JOIN ur_request ON ur_request.req_id=ur_booking.booking_requestID INNER JOIN ur_user ON ur_user.user_id=ur_request.req_userID INNER JOIN ur_driver ON ur_driver.driver_id=ur_booking.booking_driverID LEFT JOIN ur_driver_detail ON ur_driver_detail.dd_driverID = ur_driver.driver_id LEFT JOIN ur_rating ON ur_rating.rating_bookingID = ur_booking.booking_id where booking_id = ' . $booking_id;
            //echo $sql;die;
            $connection = Yii::app()->db;
            $command = $connection->createCommand($sql);
            $booking = $command->queryRow();
            // print_r($booking);die;
            // $driver = Driver::model()->findByPk($driver_id);
            if (empty($booking)) {
                $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'booking not exist.'));
            } else {
                $time = $booking['time'];
                $distance = $booking['distance'];
                $fare = Fare::model()->findByAttributes(array('fare_id' => 1));
                $per_time = $fare->fare_time;
                $per_distance = $fare->fare_distance;
                $base_fare = $fare->fare_base;
                $calculate_fare = ($per_time * $time) + ($per_distance * $distance);
                if ($calculate_fare < $base_fare) {
                    $fare = $base_fare;
                } else {
                    $fare = $calculate_fare;
                }
                $booking_image = "NA";
                $get_path = str_replace('-id', '-' . $booking['booking_id'], Yii::app()->params['bookingGetPath']);
                $set_path = str_replace('-id', '-' . $booking['booking_id'], Yii::app()->params['bookingSetPath']);
                if ($booking['booking_image'] != 'NA') {
                    $booking_image = ((file_exists($set_path . $booking['booking_image'])) ? $get_path . $booking['booking_image'] : 'NA');
                }
                $user_avatar = "NA";
                if ($booking['user_avatar'] != 'NA') {
                    $user_avatar = file_exists(Yii::app()->params['profileSetPath'] . $booking['user_avatar']) ? Yii::app()->params['profileGetPath'] . $booking['user_avatar'] : 'NA';
                }
                $driver_avatar = "NA";
                $get_path = str_replace('-id', '-' . $booking['driver_id'], Yii::app()->params['driverProfileGetPath']);
                $set_path = str_replace('-id', '-' . $booking['driver_id'], Yii::app()->params['driverProfileSetPath']);
                if ($booking['driver_avatar'] != 'NA') {
                    $driver_avatar = (file_exists($set_path . $booking['driver_avatar'])) ? $get_path . $booking['driver_avatar'] : '';
                }
                //print_r($booking);die;
                $driver = array(
                    'booking_id' => $booking['booking_id'],
                    'booking_image' => $booking_image,
                    'source' => $booking['source'],
                    'destination' => $booking['destination'],
                    'distance' => $booking['distance'],
                    'time' => $booking['time'],
                    'date' => $booking['created'],
                    'fare' => strval(number_format($fare, 2, '.', '')),
                    'rating' => $booking['rating'],
                    'user_id' => $booking['user_id'],
                    'user_name' => $booking['user_name'],
                    'user_mobile' => $booking['user_mobile'],
                    'user_email' => $booking['user_email'],
                    'user_avatar' => $user_avatar,
                    'driver_id' => $booking['driver_id'],
                    'driver_name' => $booking['driver_name'],
                    'driver_mobile' => $booking['driver_mobile'],
                    'driver_email' => $booking['driver_email'],
                    'driver_avatar' => $driver_avatar,
                    'vehicle_no' => $booking['vehicle_no'],
                    'vehicle_model' => $booking['vehicle_model']
                );
                $this->response(200, $arrayName = array('status' => TRUE, 'data' => $driver, 'response' => 'success'));
            }
        }
    }

    public function actionGetBookingHistory() {
        $post_data = $_POST;
        if (empty($post_data['id']) || empty($post_data['type'])) {
            $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'invalid data.'));
        } else {
            $limit = (!empty($post_data['limit']) ? $post_data['limit'] : 10 );
            $current_page = (!empty($post_data['current_page']) ? $post_data['current_page'] : 1 );
            $where = ($post_data['type'] == 'user') ? ' req_userID = ' . $post_data['id'] : ' booking_driverID = ' . $post_data['id'];
            $sql = "SELECT COUNT(booking_id) total FROM ur_booking JOIN ur_request ON ur_request.req_id = ur_booking.booking_requestID WHERE " . $where;
            //$sql = "SELECT booking_id,req_src_adrs source_address,req_src_lat source_latitude,req_src_long source_longitude,req_des_adrs destination_address,req_des_lat destination_latitude,req_des_long destination_longitude FROM ur_booking JOIN ur_request ON ur_request.req_id = ur_booking.booking_requestID WHERE ".$where;
            $connection = Yii::app()->db;
            $command = $connection->createCommand($sql);
            $booking = $command->queryRow();
            //print_r($booking);die;
            if (empty($booking)) {
                $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'No any booking found'));
            } else {
                $total_page = ($booking['total'] % $limit == 0) ? $booking['total'] / $limit : ((int) ($booking['total'] / $limit)) + 1; //calculate total page
                $offset = (($current_page - 1) * $limit);
                $sql = "SELECT booking_id,req_src_adrs source_address,req_src_lat source_latitude,req_src_long source_longitude,req_des_adrs destination_address,req_des_lat destination_latitude,req_des_long destination_longitude,booking_created date,booking_status FROM ur_booking JOIN ur_request ON ur_request.req_id = ur_booking.booking_requestID WHERE " . $where . " ORDER BY  `ur_booking`.`booking_id` DESC  LIMIT " . $offset . " , " . $limit;
                $connection = Yii::app()->db;
                $command = $connection->createCommand($sql);
                $booking = $command->queryAll();
                $this->response(200, $arrayName = array('status' => TRUE, 'data' => $booking, 'total_page' => strval($total_page), 'current_page' => strval($current_page), 'limit' => strval($limit), 'response' => 'success'));
            }
        }
    }

    public function actionAddBookingImage() {
        $post_data = $_POST;
        if (empty($post_data['booking_id'])) {
            $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'invalid data.'));
        } else {
            $booking = Booking::model()->findByAttributes(array('booking_id' => $post_data['booking_id']));
            if (empty($booking)) {
                $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'booking not found'));
            } else {
                //$booking = new Booking;            
                $image = CUploadedFile::getInstanceByName('avatar');
                $name = rand(1111, 9999) . date('Ymdhi');
                $booking->booking_image = '';
                $path = str_replace('-id', '-' . $booking->booking_id, Yii::app()->params['bookingSetPath']);
                // die($path);
                // checking image and rename
                if (!empty($image)) {
                    if (!file_exists($path)) {
                        mkdir($path);
                    }
                    $extension = strtolower($image->getExtensionName());
                    $filename = "{$name}.{$extension}";
                    $booking->booking_image = $filename;
                    $image->saveAs($path . $filename);
                }
                if ($booking->update()) {
                    $this->response(200, $arrayName = array('status' => TRUE, 'response' => 'Booking image added.'));
                } else {
                    $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'Booking image not added.'));
                }
            }
        }
    }

    public function actionAddRating() {
        $post_data = $_POST;
        if (empty($post_data['booking_id'])) {
            $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'invalid data.'));
        } else {
            $rating = Rating::model()->findByAttributes(array('rating_bookingID' => $post_data['booking_id']));
            if (!empty($rating)) {
                $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'rating already exist'));
            } else {
                $rating = new Rating;
                $rating->rating_bookingID = $post_data['booking_id'];
                $rating->rating_rate = $post_data['rate'];
                $rating->rating_comment = $post_data['comment'];
                if ($rating->save()) {
                    $this->response(200, $arrayName = array('status' => TRUE, 'response' => 'Rating added.'));
                } else {
                    //print_r($card->getErrors());
                    $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'Rating not added.'));
                }
            }
        }
    }

    
    
    public function actionGetLatestBooking(){
        $post_data = $_POST;
        if (empty($post_data['type']) || empty($post_data['id'])) {
            $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'invalid data.'));
        }
        else{
            $where=(!empty($post_data['type']) && $post_data['type']=='user') ? 'req_userID = '.$post_data['id'] : 'booking_driverID = '.$post_data['id'] ;
            
            
           // $booking_id = $post_data['booking_id'];
            $sql = 'SELECT  booking_id,booking_image,req_src_adrs source,req_des_adrs destination,req_src_lat source_latitude,req_src_long source_longitude,req_des_lat destination_latitude,req_des_long destination_longitude,booking_paid,req_distance distance,req_time time,booking_created created,user_id,CONCAT(user_fname," ",user_lname) user_name,user_mobile,user_email,user_avatar,driver_id,CONCAT(driver_fname," ",driver_lname) driver_name,driver_email,driver_mobile,driver_lat,driver_long,IF(dd_vehicle_no !="",dd_vehicle_no,"") vehicle_no,IF(dd_vehicle_model !="",dd_vehicle_model,"") vehicle_model,ROUND(IF(AVG(rating_rate) != "",AVG(rating_rate),0),1)  rating,driver_avatar,booking_status,booking_ride FROM ur_booking  INNER JOIN ur_request ON ur_request.req_id=ur_booking.booking_requestID INNER JOIN ur_user ON ur_user.user_id=ur_request.req_userID INNER JOIN ur_driver ON ur_driver.driver_id=ur_booking.booking_driverID LEFT JOIN ur_driver_detail ON ur_driver_detail.dd_driverID = ur_driver.driver_id LEFT JOIN ur_rating ON ur_rating.rating_bookingID = ur_booking.booking_id where ' . $where.' ORDER BY booking_id DESC';
            //echo $sql;die;
            $connection = Yii::app()->db;
            $command = $connection->createCommand($sql);
            $booking = $command->queryRow();
             //print_r($booking);die;
            // $driver = Driver::model()->findByPk($driver_id);
            if (empty($booking)) {
                $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'booking not exist.'));
            } else {
                $time = $booking['time'];
                $distance = $booking['distance'];
                $fare = Fare::model()->findByAttributes(array('fare_id' => 1));
                $per_time = $fare->fare_time;
                $per_distance = $fare->fare_distance;
                $base_fare = $fare->fare_base;
                $calculate_fare = ($per_time * $time) + ($per_distance * $distance);
                if ($calculate_fare < $base_fare) {
                    $fare = $base_fare;
                } else {
                    $fare = $calculate_fare;
                }
                $booking_image = "NA";
                $get_path = str_replace('-id', '-' . $booking['booking_id'], Yii::app()->params['bookingGetPath']);
                $set_path = str_replace('-id', '-' . $booking['booking_id'], Yii::app()->params['bookingSetPath']);
                if ($booking['booking_image'] != 'NA') {
                    $booking_image = ((file_exists($set_path . $booking['booking_image'])) ? $get_path . $booking['booking_image'] : 'NA');
                }
                $user_avatar = "NA";
                if ($booking['user_avatar'] != 'NA') {                    
                    $user_avatar = file_exists(Yii::app()->params['profileSetPath'] . $booking['user_avatar']) ? Yii::app()->params['profileGetPath'] . $booking['user_avatar'] : 'NA';
                }
                $driver_avatar = "NA";
                $get_path = str_replace('-id', '-' . $booking['driver_id'], Yii::app()->params['driverProfileGetPath']);
                $set_path = str_replace('-id', '-' . $booking['driver_id'], Yii::app()->params['driverProfileSetPath']);
                if ($booking['driver_avatar'] != 'NA') {
                    $driver_avatar = (file_exists($set_path . $booking['driver_avatar'])) ? $get_path . $booking['driver_avatar'] : '';
                }
                //print_r($booking);die;
                $driver = array(
                    'booking_id' => $booking['booking_id'],
                    'booking_image' => $booking_image,
                    'source' => $booking['source'],
                    'source_latitude' => $booking['source_latitude'],
                    'source_longitude' => $booking['source_longitude'],
                    'destination' => $booking['destination'],
                    'destination_latitude' => $booking['destination_latitude'],
                    'destination_longitude' => $booking['destination_longitude'],
                    'distance' => $booking['distance'],                    
                    'time' => $booking['time'],
                    'date' => $booking['created'],
                    'ride'=>$booking['booking_ride'],
                    'status' => $booking['booking_status'],
                    'paid' => $booking['booking_paid'],
                    'fare' => strval(number_format($fare, 2, '.', '')),
                    'rating' => $booking['rating'],
                    'user_id' => $booking['user_id'],
                    'status' => $booking['booking_status'],
                    'user_name' => $booking['user_name'],
                    'user_mobile' => $booking['user_mobile'],
                    'user_email' => $booking['user_email'],
                    'user_avatar' => $user_avatar,
                    'driver_id' => $booking['driver_id'],
                    'driver_name' => $booking['driver_name'],
                    'driver_mobile' => $booking['driver_mobile'],
                    'driver_email' => $booking['driver_email'],
                    'driver_lat' => $booking['driver_lat'],
                    'driver_long' => $booking['driver_long'],
                    'driver_avatar' => $driver_avatar,
                    'vehicle_no' => $booking['vehicle_no'],
                    'vehicle_model' => $booking['vehicle_model']
                );
                $this->response(200, $arrayName = array('status' => TRUE, 'data' => $driver, 'response' => 'success'));
            }
        }
    }
    
    
    public function actionGetAccountHistory() {
        $post_data = $_POST;
        if (empty($post_data['driver_id'])) {
            $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'invalid data.'));
        } else {
            $driver_id = $post_data['driver_id'];
            $sql = 'SELECT  transfer_id,transfer_amount amount,transfer_created date FROM ur_transfer JOIN ur_driver ON ur_driver.driver_id = ur_transfer.transfer_driverID where transfer_driverID = ' . $driver_id;
            $connection = Yii::app()->db;
            $command = $connection->createCommand($sql);
            $transfer = $command->queryAll();
            if (!empty($transfer)) {
                $this->response(200, $arrayName = array('status' => TRUE, 'data' => $transfer, 'response' => 'success'));
            } else {
                $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'no any transfer found.'));
            }
        }
    }

    
    
    public function actionUserLogout(){
        $post_data = $_POST;      
        if(empty($post_data['user_id'])){
            $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'invalid data.'));
        }
        else{
             $user = User::model()->findByAttributes(array('user_id' => $post_data['user_id']));
             if(empty($user)){
                $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'no any user found'));
             }
             else{
                $user->user_udid='NA';
                $user->user_platform='NA';
                if($user->update()){
                    $this->response(200, $arrayName = array('status' => TRUE,'data'=>array('user_id'=>$user->user_id), 'response' => 'User successfully logout.'));
                }
             }
        }
    }
    
    
    
    public function actionDriverLogout(){
        $post_data = $_POST;
        
        if(empty($post_data['driver_id'])){
            $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'invalid data.'));
        }
        else{
             $driver = Driver::model()->findByAttributes(array('driver_id' => $post_data['driver_id']));
             if(empty($driver)){
                $this->response(200, $arrayName = array('status' => FALSE, 'response' => 'no any driver found'));
             }
             else{
                $driver->driver_udid='NA';
                $driver->driver_platform='NA';
                $driver->driver_mode='0';
                
                if($driver->update()){
                    $this->response(200, $arrayName = array('status' => TRUE,'data'=>array('driver_id'=>$driver->driver_id), 'response' => 'Driver successfully logout.'));
                }
             }
        }
    }
    
    
    /*
     * Add Device Token Function
     */

    public function actionAddDeviceToken() {
        //user_id, device_token, device_type         
        if (in_array('', $_POST) || (count($_POST) < 3)) {
            $this->response(200, $arrayName = array('status' => FALSE, 'status_code' => '85'));
        } else {
            $result = Devices::model()->ws_register($_POST);
            switch ($result) {
                case 1:
                    $result = array(
                        'status' => TRUE,
                        'status_code' => '86',
                    );
                    $this->response(200, $arrayName = $result);
                    break;
                case 2:
                    $result = array(
                        'status' => FALSE,
                        'status_code' => '87'
                    );
                    $this->response(200, $arrayName = $result);
                    break;
                default :
                    $result = array(
                        'status' => FALSE,
                        'status_code' => '88'
                    );
                    $this->response(200, $arrayName = $result);
                    break;
            }
        }
    }

    /*
     * Delete Device Token Function
     */

    public function actionDeleteDeviceToken() {
        //device_token
        if (in_array('', $_POST) || (count($_POST) < 1)) {
            $this->response(200, $arrayName = array('status' => FALSE, 'status_code' => '89'));
        } else {
            $result = Devices::model()->ws_unregister($_POST);
            switch ($result) {
                case 1:
                    $result = array(
                        'status' => TRUE,
                        'status_code' => '90',
                    );
                    $this->response(200, $arrayName = $result);
                    break;
                case 2:
                    $result = array(
                        'status' => FALSE,
                        'status_code' => '91'
                    );
                    $this->response(200, $arrayName = $result);
                    break;
                default :
                    $result = array(
                        'status' => FALSE,
                        'status_code' => '92'
                    );
                    $this->response(200, $arrayName = $result);
                    break;
            }
        }
    }

    public function actionSendNotification() {
        if (isset($_REQUEST['id']) && !empty($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
        }
        $driver_name = $_REQUEST['driver_name'];
        $license_number = $_REQUEST['license_number'];
        $contact = $_REQUEST['contact'];
        $type = $_REQUEST['type'];
        $token_1 = array();
        $token_2 = array();
        $from = '';
        $to = '';
        if ($type == "1" || $type == "2") {
            $model = Booking::model()->findByPk($id);
            $user_id = $model->booking_userID;
            $car_model = Booking::model()->getBookingCarModelByID($model->booking_model);
            $datetime = $model->booking_eta;
            if ($type == "2") {
                $from = $model->booking_location . ', ' . District::model()->getDistrictName($model->booking_districtID);
                $to = 'Airport';
            } else {
                $from = 'Airport';
                $to = $model->booking_location . ', ' . District::model()->getDistrictName($model->booking_districtID);
            }
            $devices = Devices::model()->findAllByAttributes(array('device_userID' => $user_id));
            foreach ($devices as $d) {
                if ($d->device_type == 1) {
                    $token_1[] = $d->device_token;
                } else {
                    $token_2[] = $d->device_token;
                }
            }
        } else if ($type == "3") {
            $model = Hourlyonhire::model()->findByPk($id);
            $user_id = $model->hourlyonhire_userID;
            $car_model = Booking::model()->getBookingCarModelByID($model->hourlyonhire_model);
            $datetime = $model->hourlyonhire_pickupdatetime;
            $from = $model->hourlyonhire_pickuppoint;
            $to = '';
            $devices = Devices::model()->findAllByAttributes(array('device_userID' => $user_id));
            foreach ($devices as $d) {
                if ($d->device_type == 1) {
                    $token_1[] = $d->device_token;
                } else {
                    $token_2[] = $d->device_token;
                }
            }
        } else if ($type == "4") {
            $model = Adhocptop::model()->findByPk($id);
            $user_id = $model->adhocptop_userID;
            $car_model = Booking::model()->getBookingCarModelByID($model->adhocptop_model);
            $datetime = $model->adhocptop_pickupdatetime;
            $from = $model->adhocptop_fromaddress . ', ' . District::model()->getDistrictName($model->adhocptop_fromdistrictID) . ', ' . Country::model()->getCountryName($model->adhocptop_fromcountryID);
            $to = $model->adhocptop_toaddress . ', ' . District::model()->getDistrictName($model->adhocptop_todistrictID) . ', ' . Country::model()->getCountryName($model->adhocptop_tocountryID);
            $devices = Devices::model()->findAllByAttributes(array('device_userID' => $user_id));
            foreach ($devices as $d) {
                if ($d->device_type == 1) {
                    $token_1[] = $d->device_token;
                } else {
                    $token_2[] = $d->device_token;
                }
            }
        }
        $message = array(
            'from' => $from,
            'to' => $to,
            'date_time' => $datetime,
            'driver_name' => $driver_name,
            'contact' => $contact,
            'license_number' => $license_number,
            'model' => $car_model
        );
        $message = json_encode($message);
//        print_r($message);
//        die;
        //Android            
        //foreach ($token_1 as $t) {
        if (count($token_1) > 0) {
            $this->actionSendNotificationAndroid($token_1, $message);
        }
        //}
        //iPhone
        if (count($token_2) > 0) {
            foreach ($token_2 as $t) {
                $this->actionSendNotificationIphone($t, $message);
            }
        }
    }

    public function actionSendNotificationAndroid($token, $message) {
//        echo 'Android';
//        print_r($token);
//        echo $message;
//        die;
        $GOOGLE_API_KEY = 'AIzaSyD_elCI0g8kt2vmfIxdkn-9ajnECIedL8E';
        // Set POST variables        
        $url = 'https://android.googleapis.com/gcm/send';
        $msg = array(
            'message' => $message,
            'title' => 'Parklane Limousine',
            'subtitle' => '',
            'tickerText' => '',
            'vibrate' => 1,
            'sound' => 1
        );
        //print_r($registatoin_ids);die;
        $fields = array(
            'registration_ids' => $token,
            'data' => $msg
        );
        $headers = array(
            'Authorization: key=' . $GOOGLE_API_KEY,
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        // Close connection
        curl_close($ch);
        echo $result;
        exit;
    }

    public function actionSendNotificationIphone($deviceToken, $message) {
//        echo 'iPhone';
//        print_r($deviceToken);
//        echo $message;
        //die;
        $path = $_SERVER['DOCUMENT_ROOT'] . '/bootstrap/DevelopmentCertificates.pem';
        // Put your private key's passphrase here:
        $passphrase = '123';
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', $path);
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
        // Open a connection to the APNS server
        $fp = stream_socket_client(
                'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);
        //echo 'Connected to APNS' . PHP_EOL;
        // Create the payload body
        $body['aps'] = array(
            'alert' => $message,
            'sound' => 'default'
        );
        // Encode the payload as JSON
        $payload = json_encode($body);
        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', trim($deviceToken)) . pack('n', strlen($payload)) . $payload;
        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));
        if (!$result)
            echo 0; //echo 'Message not delivered' . PHP_EOL;
        else
            echo 1; //echo 'Message successfully delivered' . PHP_EOL;
        fclose($fp);
    }

    public function actionSendTestIphone() {
        $deviceToken = 'c2d928fc1a91e6897d33c40a129c3032a6a35ee87dd45a68019f6e0050455609';
        $message = 'Test message from Parklane Limousine!';
        $this->actionSendNotification($deviceToken, $message);
        //$deviceToken ="c2d928fc1a91e6897d33c40a129c3032a6a35ee87dd45a68019f6e0050455609";
        $path = $_SERVER['DOCUMENT_ROOT'] . '/bootstrap/DevelopmentCertificates.pem';
        //$path = "bootstrap/DevelopmentCertificates.pem";  
        $message = "by amam 123 ";
        // Put your private key's passphrase here:
        $passphrase = '123';
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', $path);
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
        // Open a connection to the APNS server
        $fp = stream_socket_client(
                'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);
        //echo 'Connected to APNS' . PHP_EOL;
        // Create the payload body
        $body['aps'] = array(
            'alert' => $message,
            'sound' => 'default'
        );
        // Encode the payload as JSON
        $payload = json_encode($body);
        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', trim($deviceToken)) . pack('n', strlen($payload)) . $payload;
        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));
        if (!$result)
            echo 0; //echo 'Message not delivered' . PHP_EOL;
        else
            echo 1; //echo 'Message successfully delivered' . PHP_EOL;
        fclose($fp);
    }

    private function response($status = 200, $body = '', $content_type = 'application/json') {
        $statusHeader = 'HTTP/1.1 ' . $status . ' ' . $this->getStatusCodeMessage($status);
        header($statusHeader);
        header('Content-type: ' . $content_type);
        echo CJSON:: encode($body);
        Yii::app()->end();
    }

    protected function getStatusCodeMessage($status) {
        $codes = array(
            100 => 'No E-email ID or Password found',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported',
        );
        return (isset($codes[$status])) ? $codes[$status] : '';
    }

}
