<?php

class UsersController extends Controller {

    public $layout = '//layouts/column1';
    public $data_array = [];

    protected function beforeAction($event) {
        if (!isset(Yii::app()->session['admin_data'])):
            $this->redirect(Yii::app()->createAbsoluteUrl('authentication/login'));
        endif;
        return TRUE;
    }


    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        if (isset(Yii::app()->session['admin_data']) && !empty(Yii::app()->session['admin_data'])):
            $this->render('//admin/dashboard');
        else:
            $this->redirect(Yii::app()->createAbsoluteUrl('authentication/login'));
        endif;
    }

    /**
     * [actionProfile Used to render the profile view]
     * @return [type] [description]
     * @developer : Amit K Sharma
     */
    public function actionUsers() {
        $data_array['css_file']     = 'user';
        $data_array['title']        = 'User Management';
        $data_array['sub_title']    = '';
        $data_array['js_file']      = 'user';
        $this->render('/admin/users/users', $data_array);
    }


    /**
     * [actionShowlist Show the list of drivers]
     * @return [type] [description]
     * @developer : Amit K Sharma
     */
    public function actionShowlist(){

        $model = Users::model()->findAll();
        $iTotalRecords  = count($model);
        $iDisplayLength = intval($_REQUEST['iDisplayLength']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart  = intval($_REQUEST['iDisplayStart']);
        $sEcho = intval($_REQUEST['sEcho']);
        
        $records           = array();
        $records["aaData"] = array();

        $end               = $iDisplayStart + $iDisplayLength;
        $end               = $end > $iTotalRecords ? $iTotalRecords : $end;

        $joins             = array();

        $condition = '';
        $name = $email = $mobile = $status = '';

        // creating the instance of CDbCriteria
        $criteria = new CDbCriteria;

        if(!empty($_POST['user_name'])){
            $name = $_POST['user_name'];
            $criteria->compare('user_fname',$name,true);
        }
      
        if(!empty($_POST['user_email'])){
            $email = $_POST['user_email'];
            $criteria->compare('user_email',$email,true);
        }

        if(!empty($_POST['user_mobile'])){
            $mobile = $_POST['user_mobile'];
            $criteria->condition = "user_mobile = $mobile";
        }
        
        $_POST['user_status'] = '';
        if(!empty($_POST['user_status']) || $_POST['user_status'] == 0){
            $status = $_POST['user_status'];
            $criteria->compare('user_status',$status,true);
        }


        $column = $_POST["iSortCol_0"];
        $sort_by = 'DESC';

        switch($column){
                case 1:
                    $order_by = 'user_fname';
                    $sort_by = $_POST["sSortDir_0"];
                    break;
                case 2:
                    $order_by = 'user_email';
                    $sort_by = $_POST["sSortDir_0"];
                    break;
                case 3:
                    $order_by = 'user_mobile';
                    $sort_by = $_POST["sSortDir_0"];
                    break;
                case 4:
                    $order_by = 'user_status';
                    $sort_by = $_POST["sSortDir_0"];
                    break;
                default:
                    $order_by = 'user_id';
                    $sort_by = 'DESC';
        }

        // setting the limit and offset using CDbCriteria class instance
        $criteria->limit = $iDisplayLength;
        $criteria->offset = $iDisplayStart;
        $criteria->order = $order_by.' '.$sort_by;
        //$data              = Drivers::model()->findAllBySql('SELECT * FROM pa_drivers LIMIT '.$iDisplayLength.' OFFSET '.$iDisplayStart);
        // $data              = Drivers::model()->findAll(array('limit'=>$iDisplayLength, 'offset' => $iDisplayStart));
        $data              = Users::model()->findAll($criteria);
        
        if(is_array($data)){
            $i=1;
            foreach($data as $row){
                // $image = $row['user_avatar'] != "" ? Utils::UserImagePath().$row['user_avatar'] : Utils::UserImagePath().'noimg/no-image.png';
                $records["aaData"][] = array(
                                             $i,
                                             // '<img src="'.$image.'" alt="" style="height:50px;width:50px;">',
                                             ucwords($row['user_fname'].' '.$row['user_lname']),
                                             strtolower($row['user_email']),
                                             $row['user_mobile'],
                                             $row['user_status'] == 1 ?  '<span class="label label-success allow-field" onClick="notAllow('.$row['user_id'].')">Active</span>': '<span class="label label-danger allow-field" onClick="allow('.$row['user_id'].')">Block</span>', 
                                             '<a href="javascript:;" class="btn default btn-xs blue" data-toggle="modal" onClick="userDetail('.$row['user_id'].')"><i class="fa fa-eye"></i> view</a>
                                             ',
                                             );
            $i++;}
        }else{
            $records["aaData"][] = array('<i>No records</i>', null, null,null,null,null);
        }
      
        $records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;
        echo json_encode($records);
    }

    /**
     * [actionUserallow Function to allow users]
     * @return [type] [description]
     * @developer : Amit K Sharma
     */
    public function actionUserallow(){
        $users = new Users;
        $users_model = $users->model()->findByPk($_POST['id']);
        $users_model->attributes = $_POST;
        $users_model->user_status = 1;
        if($users_model->save()):
               echo json_encode(array('errors' => 0));
        else:
            echo json_encode(array('errors' => 1));
        endif;
    }

    /**
     * [actionUserNotAllow Function to disallow users]
     * @return [type] [description]
     * @developer : Amit K Sharma
     */
    public function actionUserNotAllow(){
        $users = new Users;
        $users_model = $users->model()->findByPk($_POST['id']);
        $users_model->attributes = $_POST;
        $users_model->user_status = 0;
        if($users_model->save()):
               echo json_encode(array('errors' => 0));
        else:
            echo json_encode(array('errors' => 1));
        endif;
    }    

    /**
     * [actionDriverInfoView Action to show the driver details on view event]
     * @param  [type] $id [Driver ID]
     * @return [type]     [description]
     * @developer : Amit K Sharma
     */
    public function actionUserInfoView() {
        $id = $_POST['id'];
        $data_array   = Users::model()->findByPk($id);
        // echo '<pre>';print_r($data_array);die;
        if(!empty($data_array)):
            echo json_encode(['errors' => 0, 'data' => $data_array->attributes]);
        else:
            echo json_encode(['errors' => 1,'msg'=>'No data found!']);
        endif;
    }     

} // end class
