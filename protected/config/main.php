<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
        'defaultController' => 'authentication',
	'name'=>'URIDE',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'cgw3Amce',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		
	),

	// application components
	'components'=>array(

		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),

		// uncomment the following to enable URLs in path-format
		
                /*
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
                */
                'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'caseSensitive' => false,
            'rules' => array(
                'login'=>'authentication/login',
                'profile'=>'dashboard/profile',
            	'updateprofile'=>'dashboard/updateprofile',
            	'home'=>'dashboard/index',
            	'drivers'=>'drivers/drivers',
            	'users'=>'users/users',
            	'settings'=>'settings/settings',
            	'requests'=>'requests/request',
            	'bookings'=>'bookings/booking',
            	'transaction'=>'transaction/transaction',
            	'payment'=>'bookings/payment',
            	'promo'=>'promo/promo',
            	'promoAdd'=>'promo/promoAdd',
            	'driverView/<id:\d+>'=>'drivers/driverInfoView',
            	'driverEdit/<id:\d+>'=>'drivers/driverInfoEdit',
            	'showPaymentDetail/<id:\d+>'=>'bookings/showPaymentDetail',
            	'paymentDetails/<id:\d+>'=>'bookings/paymentInfoView',
            	'userView/<id:\d+>'=>'users/userInfoView',
                '<action:[\w\-]+>' => 'dashboard/<action>',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                'gii' => 'gii',
                'gii/<controller:\w+>' => 'gii/<controller>',
                'gii/<controller:\w+>/<action:\w+>' => 'gii/<controller>/<action>',
            ),
		),
		

		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>YII_DEBUG ? null : 'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
                'image_path'=>'http://'.$_SERVER['HTTP_HOST'].'/ola/taxibooking/images/',
		'adminEmail'=>'webmaster@example.com',
                'profileSetPath'=>$_SERVER['DOCUMENT_ROOT'].'ola/yii/upload/user/',
                'profileGetPath'=>'http://'.$_SERVER['HTTP_HOST'].'/ola/yii/upload/user/',
                
                'bookingSetPath'=>$_SERVER['DOCUMENT_ROOT'].'ola/yii/upload/booking/booking-id/',
                'bookingGetPath'=>'http://'.$_SERVER['HTTP_HOST'].'/ola/yii/upload/booking/booking-id/',
                
                'driverProfileSetPath'=>$_SERVER['DOCUMENT_ROOT'].'ola/yii/upload/driver/driver-id/profile/',
                'driverProfileGetPath'=>'http://'.$_SERVER['HTTP_HOST'].'/ola/yii/upload/driver/driver-id/profile/'
                
	),
        
       
        
);
